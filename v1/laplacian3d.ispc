#include "laplacian3d.h"

export void forward(uniform int n,uniform int nx,uniform int nxy,uniform float dx2,uniform float dy2,uniform float dz2,uniform float dt2,uniform float v[],uniform float eps[],uniform float del[],uniform float prevSigmaX[],uniform float curSigmaX[],uniform float prevSigmaZ[],uniform float curSigmaZ[]){
    foreach(i=0...n){
        float c33=v[i]*v[i];
        float c11=c33*(1.f+2.f*eps[i]);
        float c13=c33*sqrt(1.f+2.f*del[i]);
        float d2SigmaXdx2=(C0*curSigmaX[i]+C1*(curSigmaX[i+1]+curSigmaX[i-1])
                                          +C2*(curSigmaX[i+2]+curSigmaX[i-2])
                                          +C3*(curSigmaX[i+3]+curSigmaX[i-3])
                                          +C4*(curSigmaX[i+4]+curSigmaX[i-4]))/dx2;
        float d2SigmaXdy2=(C0*curSigmaX[i]+C1*(curSigmaX[i+nx]+curSigmaX[i-nx])
                                          +C2*(curSigmaX[i+2*nx]+curSigmaX[i-2*nx])
                                          +C3*(curSigmaX[i+3*nx]+curSigmaX[i-3*nx])
                                          +C4*(curSigmaX[i+4*nx]+curSigmaX[i-4*nx]))/dy2;
        float d2SigmaZdz2=(C0*curSigmaZ[i]+C1*(curSigmaZ[i+nxy]+curSigmaZ[i-nxy])
                                          +C2*(curSigmaZ[i+2*nxy]+curSigmaZ[i-2*nxy])
                                          +C3*(curSigmaZ[i+3*nxy]+curSigmaZ[i-3*nxy])
                                          +C4*(curSigmaZ[i+4*nxy]+curSigmaZ[i-4*nxy]))/dz2;
        prevSigmaX[i]=2.f*curSigmaX[i]+dt2*(c11*(d2SigmaXdx2+d2SigmaXdy2)+c13*d2SigmaZdz2)-prevSigmaX[i];
        prevSigmaZ[i]=2.f*curSigmaZ[i]+dt2*(c13*(d2SigmaXdx2+d2SigmaXdy2)+c33*d2SigmaZdz2)-prevSigmaZ[i];
    }
}

export void adjoint(uniform int n,uniform int nx,uniform int nxy,uniform float dx2,uniform float dy2,uniform float dz2,uniform float dt2,uniform float v[],uniform float eps[],uniform float del[],uniform float prevSigmaX[],uniform float curSigmaX[],uniform float prevSigmaZ[],uniform float curSigmaZ[],uniform float cc[]){
    foreach(i=0...n){
        float c33=v[i]*v[i];
        float c11=c33*(1.f+2.f*eps[i]);
        float c13=c33*sqrt(1.f+2.f*del[i]);
        
        float d2SigmaXdx2=C0*(c11*curSigmaX[i]+c13*curSigmaZ[i]);
        float d2SigmaXdy2=d2SigmaXdx2;
        float d2SigmaZdz2=C0*(c13*curSigmaX[i]+c33*curSigmaZ[i]);
        
        for(int j=1;j<5;j++){
            size_t ij=i+j;
            c33=v[ij]*v[ij];
            c11=c33*(1.f+2.f*eps[ij]);
            c13=c33*sqrt(1.f+2.f*del[ij]);
            float temp=c11*curSigmaX[ij]+c13*curSigmaZ[ij];
            
            ij=i-j;
            c33=v[ij]*v[ij];
            c11=c33*(1.f+2.f*eps[ij]);
            c13=c33*sqrt(1.f+2.f*del[ij]);
            temp+=c11*curSigmaX[ij]+c13*curSigmaZ[ij];
            
            d2SigmaXdx2+=cc[j]*temp;
            
            ij=i+j*nx;
            c33=v[ij]*v[ij];
            c11=c33*(1.f+2.f*eps[ij]);
            c13=c33*sqrt(1.f+2.f*del[ij]);
            temp=c11*curSigmaX[ij]+c13*curSigmaZ[ij];
            
            ij=i-j*nx;
            c33=v[ij]*v[ij];
            c11=c33*(1.f+2.f*eps[ij]);
            c13=c33*sqrt(1.f+2.f*del[ij]);
            temp+=c11*curSigmaX[ij]+c13*curSigmaZ[ij];
            
            d2SigmaXdy2+=cc[j]*temp;
            
            ij=i+j*nxy;
            c33=v[ij]*v[ij];
            c13=c33*sqrt(1.f+2.f*del[ij]);
            temp=c13*curSigmaX[ij]+c33*curSigmaZ[ij];
            
            ij=i-j*nxy;
            c33=v[ij]*v[ij];
            c13=c33*sqrt(1.f+2.f*del[ij]);
            temp+=c13*curSigmaX[ij]+c33*curSigmaZ[ij];
            
            d2SigmaZdz2+=cc[j]*temp;
        }
        prevSigmaX[i]=2.f*curSigmaX[i]+dt2*(d2SigmaXdx2/dx2+d2SigmaXdy2/dy2)-prevSigmaX[i];
        prevSigmaZ[i]=2.f*curSigmaZ[i]+dt2*d2SigmaZdz2/dz2-prevSigmaZ[i];
    }
}

export void gradient(uniform float gv[],uniform float geps[],uniform float gdel[],uniform int n,uniform int nx,uniform int nxy,uniform float dx2,uniform float dy2,uniform float dz2,uniform float dt2,uniform float v[],uniform float eps[],uniform float del[],uniform float prevSigmaX[],uniform float curSigmaX[],uniform float prevSigmaZ[],uniform float curSigmaZ[],uniform float curSigmaXa[],uniform float curSigmaZa[]){
    foreach(i=0...n){
        float c33=v[i]*v[i];
        float c11=c33*(1.f+2.f*eps[i]);
        float sqrt12del=sqrt(1.f+2.f*del[i]);
        float c13=c33*sqrt12del;

        float d2SigmaXdx2=(C0*curSigmaX[i]+C1*(curSigmaX[i+1]+curSigmaX[i-1])
                                          +C2*(curSigmaX[i+2]+curSigmaX[i-2])
                                          +C3*(curSigmaX[i+3]+curSigmaX[i-3])
                                          +C4*(curSigmaX[i+4]+curSigmaX[i-4]))/dx2;
        float d2SigmaXdy2=(C0*curSigmaX[i]+C1*(curSigmaX[i+nx]+curSigmaX[i-nx])
                                          +C2*(curSigmaX[i+2*nx]+curSigmaX[i-2*nx])
                                          +C3*(curSigmaX[i+3*nx]+curSigmaX[i-3*nx])
                                          +C4*(curSigmaX[i+4*nx]+curSigmaX[i-4*nx]))/dy2;
        float d2SigmaZdz2=(C0*curSigmaZ[i]+C1*(curSigmaZ[i+nxy]+curSigmaZ[i-nxy])
                                          +C2*(curSigmaZ[i+2*nxy]+curSigmaZ[i-2*nxy])
                                          +C3*(curSigmaZ[i+3*nxy]+curSigmaZ[i-3*nxy])
                                          +C4*(curSigmaZ[i+4*nxy]+curSigmaZ[i-4*nxy]))/dz2;
        prevSigmaX[i]=2.f*curSigmaX[i]+dt2*(c11*(d2SigmaXdx2+d2SigmaXdy2)+c13*d2SigmaZdz2)-prevSigmaX[i];
        prevSigmaZ[i]=2.f*curSigmaZ[i]+dt2*(c13*(d2SigmaXdx2+d2SigmaXdy2)+c33*d2SigmaZdz2)-prevSigmaZ[i];
        
        float gc11=curSigmaXa[i]*(d2SigmaXdx2+d2SigmaXdy2);
        float gc33=curSigmaZa[i]*d2SigmaZdz2;
        float gc13=curSigmaXa[i]*d2SigmaZdz2+curSigmaZa[i]*(d2SigmaXdx2+d2SigmaXdy2);
        
        gv[i]+=2.f*v[i]*(gc11*(1.f+2.f*eps[i])+gc13*sqrt12del+gc33);
        geps[i]+=gc11*2.f*c33;
        gdel[i]+=gc13*c33/sqrt12del;
    }
}





