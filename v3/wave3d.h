#ifndef WAVE3D_H
#define WAVE3D_H

#define PI 3.14159265359
#define ONETHIRD 0.3333333333
#define TWOTHIRD 0.6666666666

#define C0 -2.84722222222
#define C1 1.6
#define C2 -0.2
#define C3 0.02539682539
#define C4 -0.00178571428

#define DAMPER 0.95

#define BLOCK_DIM 16
#define NUPDATE 25
#define HALF_STENCIL 4
#define NLAG 32

void memcpyCpuToCpu2(float *dest1,float *sou1,float *dest2,float *sou2,size_t nbytes);

void memcpyCpuToCpu3(float *dest1,float *sou1,float *dest2,float *sou2,float *dest3,float *sou3,size_t nbytes);

void memcpyCpuToGpu2(float *dest1,float *sou1,float *dest2,float *sou2,size_t nbytes,cudaStream_t *stream);

void memcpyCpuToGpu3(float *dest1,float *sou1,float *dest2,float *sou2,float *dest3,float *sou3,size_t nbytes,cudaStream_t *stream);

void memcpyGpuToCpu2(float *dest1,float *sou1,float *dest2,float *sou2,size_t nbytes,cudaStream_t *stream);

void memcpyGpuToCpu3(float *dest1,float *sou1,float *dest2,float *sou2,float *dest3,float *sou3,size_t nbytes,cudaStream_t *stream);

void memcpyGpuToGpu2(float *dest1,float *sou1,float *dest2,float *sou2,size_t nbytes,cudaStream_t *stream);

void memcpyGpuToGpu3(float *dest1,float *sou1,float *dest2,float *sou2,float *dest3,float *sou3,size_t nbytes,cudaStream_t *stream);

__global__ void forwardKernelBottomBlock(float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2);

__global__ void forwardKernelTopBlock(float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2);

__global__ void forwardKernel(float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2);

__global__ void injectSource(float *SigmaX,float *SigmaZ,float source,int souIndex);

__global__ void extractAdjWfldAtSouLoc(float *gwavelet,float *SigmaXa,float *SigmaZa,int souIndexBlock,int it);

__global__ void recordData(float *data,float *SigmaX,float *SigmaZ,int nr,const int *recIndex);

void init_abc(float *damping,int nx,int ny,int npad);

void init_abc(float *damping,int nx,int ny,int nz,int npad);

__global__ void abc(int iblock,int nx,int ny,int nz,int npad,float *nextSigmaX,float *curSigmaX1,float *nextSigmaZ,float *curSigmaZ1,const float *damping);

__global__ void abcXYZ(int ib,int nx,int ny,int nz,int npad,float *nextSigmaX,float *curSigmaX1,float *nextSigmaZ,float *curSigmaZ1,const float *damping);

__global__ void abcXY(int ib,int nx,int ny,int nz,int npad,float *nextSigmaX,float *curSigmaX1,float *nextSigmaZ,float *curSigmaZ1,const float *damping);

bool forwardAbc(int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,const int *nbuffSigma,const float *d_damping,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,const float *wavelet,int souIndexBlock,int souBlock,int nr,const int *recIndex,int recBlock,int samplingTimeStep,float *data,int nx,int ny,int nz,int nt,int npad,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu=0,int NGPU=1);

void forwardRandom(int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,const int *nbuffSigma,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,int nx,int ny,int nz,int nt,int npad,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu=0,int NGPU=1);

void backwardRandom(int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,const int *nbuffSigma,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,int nx,int ny,int nz,int npad,int nt,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu=0,int NGPU=1);

__global__ void adjointKernelBottomBlock(float *prevSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *nextSigmaX,float *prevSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *nextSigmaZ,float *v0,float *v1,float *v2,float *eps0,float *eps1,float *eps2,float *del0,float *del1,float *del2,int nx,int ny,float dx2,float dy2,float dz2,float dt2);

__global__ void adjointKernelTopBlock(float *prevSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *nextSigmaX,float *prevSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *nextSigmaZ,float *v0,float *v1,float *v2,float *eps0,float *eps1,float *eps2,float *del0,float *del1,float *del2,int nx,int ny,float dx2,float dy2,float dz2,float dt2);

__global__ void adjointKernel(float *prevSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *nextSigmaX,float *prevSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *nextSigmaZ,float *v0,float *v1,float *v2,float *eps0,float *eps1,float *eps2,float *del0,float *del1,float *del2,int nx,int ny,float dx2,float dy2,float dz2,float dt2);

__global__ void injectResidual(float *residual,float *SigmaX,float *SigmaZ,int nr,const int *recIndex,float dt2);

void adjoint(int b,int e,int ib,float ***d_SigmaX,float ***d_SigmaZ,const int *nbuffSigma,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *data,int nr,const int *recIndex,int recBlock,int nx,int ny,int nz,int npad,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream);

void interpolateResidual(float *fineResidual,float *coarseResidual,int timeIndex,int nnt,int nr,int samplingTimeStep);

__global__ void gradientKernelBottomBlock(float *gv,float *geps,float *gdel,float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *curSigmaX1a,float *curSigmaZ1a,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2);

__global__ void gradientKernelTopBlock(float *gv,float *geps,float *gdel,float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *curSigmaX1a,float *curSigmaZ1a,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2);

__global__ void gradientKernel(float *gv,float *geps,float *gdel,float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *curSigmaX1a,float *curSigmaZ1a,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2);

void gradient(float **gv,float **geps,float **gdel,int ndg,int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,float ***d_SigmaXa,float ***d_SigmaZa,const int *nbuffSigma,const float *d_damping,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,float *data,int nr,const int *recIndex,int recBlock,int nx,int ny,int nz,int npad,int nt,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu=0,int NGPU=1);

void gradientWavelet(float *gwavelet,int b,int e,int k,float ***d_SigmaXa,float ***d_SigmaZa,const int *nbuffSigma,const float *d_damping,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,int souIndexBlock,int souBlock,float *data,int nr,const int *recIndex,int recBlock,int nx,int ny,int nz,int npad,int nt,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu,int NGPU);

void sumGradientTime(float *gv,float *geps,float *gdel,float *h_gv,float *h_geps,float *h_gdel,size_t nElemBlock);

__global__ void imagingKernel(float *image,float *nextSigmaX,float *nextSigmaZ,float *nextSigmaXa,float *nextSigmaZa,int nx,int ny);

void imaging(float **image,int ndg,int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,float ***d_SigmaXa,float ***d_SigmaZa,const int *nbuffSigma,const float *d_damping,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,float *data,int nr,const int *recIndex,int recBlock,int nx,int ny,int nz,int npad,int nt,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu=0,int NGPU=1);

void sumImageTime(float *image,float *h_image,size_t nElemBlock);

__global__ void extendedImagingKernel(float *image,float *nextSigmaX,float *nextSigmaZ,float *nextSigmaXa,float *nextSigmaZa,int nx,int ny);

void extendedImaging(float **image,int ndg,int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,float ***d_SigmaXa,float ***d_SigmaZa,const int *nbuffSigma,const float *d_damping,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,float *data,int nr,const int *recIndex,int recBlock,int nx,int ny,int nz,int npad,int nt,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu=0,int NGPU=1);

void modelData3d_f(float *souloc,int ns,float *recloc,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float ox,float oy,float oz,float ot,float dx,float dy,float dz,float dt,float samplingRate);

void modelData3dGroup_f(float *souloc,int ns,float *recloc,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float ox,float oy,float oz,float ot,float dx,float dy,float dz,float dt,float samplingRate,int *GPUs,int NGPU,string prefix);

void modelData3d_f(float *data,float *souloc,int ns,float *recloc,const float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float ox,float oy,float oz,float ot,float dx,float dy,float dz,float dt,float samplingRate);

void rtm3d_f(float *image,float *souloc,int ns,float *recloc,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float ox,float oy,float oz,float ot,float dx,float dy,float dz,float dt,float samplingRate);
 
double objFuncGradientVEpsDel_f(float *gv,float *geps,float *gdel,float *souloc,int ns,float *recloc,float *wavelet,float *v,float *eps,float *del,float *padboundary,float *randomboundary,int nx,int ny,int nz,int nt,int npad,float ox,float oy,float oz,float ot,float dx,float dy,float dz,float dt,float samplingRate);

double objFuncGradientWavelet_f(float *gwavelet,float *souloc,int ns,float *recloc,float *wavelet,float *v,float *eps,float *del,float *padboundary,int nx,int ny,int nz,int nt,int npad,float ox,float oy,float oz,float ot,float dx,float dy,float dz,float dt,float samplingRate);

void gradientWavelet_f(float *gwavelet,const float *observedData,float *souloc,int ns,float *recloc,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float ox,float oy,float oz,float ot,float dx,float dy,float dz,float dt,float samplingRate);

double objFuncGradientVEpsDel(float *gv,float *geps,float *gdel,float *souloc,int ns,float *recloc,float *wavelet,float *v,float *eps,float *del,float *padboundary,float *randomboundary,int nx,int ny,int nz,int nt,int npad,float ox,float oy,float oz,float ot,float dx,float dy,float dz,float dt,float samplingRate,float v0,float eps0,float wbottom,float *m);

void odcig3d_f(float *image,float *souloc,int ns,float *recloc,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float ox,float oy,float oz,float ot,float dx,float dy,float dz,float dt,float samplingRate);

#endif
