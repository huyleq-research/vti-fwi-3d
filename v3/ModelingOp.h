#ifndef MODELINGOP_H
#define MODELINGOP_H

#include <cmath>
#include "LinearSolver.h"

class ModelingOp:public Operator{
    public:
    ModelingOp(float *souloc,int ns,float *recloc,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float ox,float oy,float oz,float ot,float dx,float dy,float dz,float dt,float samplingRate):Operator(nt,((nt-1)/std::round(samplingRate/dt)+1)*(souloc[5*ns-1]+souloc[5*ns-2])),_souloc(souloc),_ns(ns),_recloc(recloc),_v(v),_eps(eps),_del(del),_nx(nx),_ny(ny),_nz(nz),_nt(nt),_npad(npad),_ox(ox),_oy(oy),_oz(oz),_ot(ot),_dx(dx),_dy(dy),_dz(dz),_dt(dt),_samplingRate(samplingRate){};
    void forward(bool add,const float *model,float *data);
    void adjoint(bool add,float *model,const float *data);
    float *_souloc,*_recloc,*_v,*_eps,*_del;
    int _ns,_nx,_ny,_nz,_nt,_npad;
    float _ox,_oy,_oz,_ot,_dx,_dy,_dz,_dt,_samplingRate;
};

#endif
