#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <iostream>
#include <vector>
#include <thread>
#include <chrono>

#include "myio.h"
#include "mylib.h"
#include "wave3d.h"

using namespace std;

void memcpyCpuToCpu2(float *dest1,float *sou1,float *dest2,float *sou2,size_t nbytes){
 memcpy(dest1,sou1,nbytes);
 memcpy(dest2,sou2,nbytes);
}

void memcpyCpuToCpu3(float *dest1,float *sou1,float *dest2,float *sou2,float *dest3,float *sou3,size_t nbytes){
 memcpy(dest1,sou1,nbytes);
 memcpy(dest2,sou2,nbytes);
 memcpy(dest3,sou3,nbytes);
}

void memcpyCpuToGpu2(float *dest1,float *sou1,float *dest2,float *sou2,size_t nbytes,cudaStream_t *stream){
 cudaMemcpyAsync(dest1,sou1,nbytes,cudaMemcpyHostToDevice,*stream);
 cudaMemcpyAsync(dest2,sou2,nbytes,cudaMemcpyHostToDevice,*stream);
}

void memcpyCpuToGpu3(float *dest1,float *sou1,float *dest2,float *sou2,float *dest3,float *sou3,size_t nbytes,cudaStream_t *stream){
 cudaMemcpyAsync(dest1,sou1,nbytes,cudaMemcpyHostToDevice,*stream);
 cudaMemcpyAsync(dest2,sou2,nbytes,cudaMemcpyHostToDevice,*stream);
 cudaMemcpyAsync(dest3,sou3,nbytes,cudaMemcpyHostToDevice,*stream);
}

void memcpyGpuToCpu2(float *dest1,float *sou1,float *dest2,float *sou2,size_t nbytes,cudaStream_t *stream){
 cudaMemcpyAsync(dest1,sou1,nbytes,cudaMemcpyDeviceToHost,*stream);
 cudaMemcpyAsync(dest2,sou2,nbytes,cudaMemcpyDeviceToHost,*stream);
}

void memcpyGpuToCpu3(float *dest1,float *sou1,float *dest2,float *sou2,float *dest3,float *sou3,size_t nbytes,cudaStream_t *stream){
 cudaMemcpyAsync(dest1,sou1,nbytes,cudaMemcpyDeviceToHost,*stream);
 cudaMemcpyAsync(dest2,sou2,nbytes,cudaMemcpyDeviceToHost,*stream);
 cudaMemcpyAsync(dest3,sou3,nbytes,cudaMemcpyDeviceToHost,*stream);
}

void memcpyGpuToGpu2(float *dest1,float *sou1,float *dest2,float *sou2,size_t nbytes,cudaStream_t *stream){
 cudaMemcpyAsync(dest1,sou1,nbytes,cudaMemcpyDefault,*stream);
 cudaMemcpyAsync(dest2,sou2,nbytes,cudaMemcpyDefault,*stream);
}

void memcpyGpuToGpu3(float *dest1,float *sou1,float *dest2,float *sou2,float *dest3,float *sou3,size_t nbytes,cudaStream_t *stream){
 cudaMemcpyAsync(dest1,sou1,nbytes,cudaMemcpyDefault,*stream);
 cudaMemcpyAsync(dest2,sou2,nbytes,cudaMemcpyDefault,*stream);
 cudaMemcpyAsync(dest3,sou3,nbytes,cudaMemcpyDefault,*stream);
}

__global__ void testKernel(float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2){

 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int i=ix+iy*nx;
  for(int j=0;j<HALF_STENCIL;j++){
   int ij=i+j*nx*ny;
   nextSigmaX[ij]=curSigmaX1[ij]+1;
   nextSigmaZ[ij]=curSigmaZ1[ij]+1;
  }
 }

 return;
}

__global__ void forwardKernelBottomBlock(float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2){
 
 __shared__ float sSigmaX[BLOCK_DIM+2*HALF_STENCIL][BLOCK_DIM+2*HALF_STENCIL]; 
 
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int i=ix+iy*nx;
  
  int six=threadIdx.x+HALF_STENCIL;
  int siy=threadIdx.y+HALF_STENCIL;
  
  float zSigmaZ[2*HALF_STENCIL+1];
  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   zSigmaZ[iz+1]=curSigmaZ0[j];
   zSigmaZ[iz+1+HALF_STENCIL]=curSigmaZ1[j];
  }

  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   
   zSigmaZ[0]=zSigmaZ[1];
   zSigmaZ[1]=zSigmaZ[2];
   zSigmaZ[2]=zSigmaZ[3];
   zSigmaZ[3]=zSigmaZ[4];
   zSigmaZ[4]=zSigmaZ[5];
   zSigmaZ[5]=zSigmaZ[6];
   zSigmaZ[6]=zSigmaZ[7];
   zSigmaZ[7]=zSigmaZ[8];
   zSigmaZ[8]=0.;
   
   __syncthreads();
   
   sSigmaX[six][siy]=curSigmaX1[j];
   
   if(threadIdx.x<HALF_STENCIL){
    int k=min(blockDim.x,nx-2*HALF_STENCIL-blockIdx.x*blockDim.x);
    sSigmaX[threadIdx.x][siy]=curSigmaX1[j-HALF_STENCIL];
	sSigmaX[six+k][siy]=curSigmaX1[j+k];
   }
   
   if(threadIdx.y<HALF_STENCIL){
    int k=min(blockDim.y,ny-2*HALF_STENCIL-blockIdx.y*blockDim.y);
    sSigmaX[six][threadIdx.y]=curSigmaX1[j-HALF_STENCIL*nx];
	sSigmaX[six][siy+k]=curSigmaX1[j+k*nx];
   }

   __syncthreads();

   float c33=v[j]*v[j];
   float c11=c33*(1.+2.*eps[j]);
   float c13=c33*sqrt(1.+2.*del[j]);
  
   float tx=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six-1][siy]+sSigmaX[six+1][siy])
                                 +C2*(sSigmaX[six-2][siy]+sSigmaX[six+2][siy])
                                 +C3*(sSigmaX[six-3][siy]+sSigmaX[six+3][siy])
                                 +C4*(sSigmaX[six-4][siy]+sSigmaX[six+4][siy]))/dx2;
   float ty=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six][siy-1]+sSigmaX[six][siy+1])
                                 +C2*(sSigmaX[six][siy-2]+sSigmaX[six][siy+2])
                                 +C3*(sSigmaX[six][siy-3]+sSigmaX[six][siy+3])
                                 +C4*(sSigmaX[six][siy-4]+sSigmaX[six][siy+4]))/dy2;
   float tz=(C0*zSigmaZ[HALF_STENCIL]+C1*(zSigmaZ[HALF_STENCIL-1]+zSigmaZ[HALF_STENCIL+1])
                                     +C2*(zSigmaZ[HALF_STENCIL-2]+zSigmaZ[HALF_STENCIL+2])
                                     +C3*(zSigmaZ[HALF_STENCIL-3]+zSigmaZ[HALF_STENCIL+3])
                                     +C4*(zSigmaZ[HALF_STENCIL-4]+zSigmaZ[HALF_STENCIL+4]))/dz2;
   
   nextSigmaX[j]=dt2*(c11*(tx+ty)+c13*tz)+2.*sSigmaX[six][siy]-prevSigmaX[j];
   nextSigmaZ[j]=dt2*(c13*(tx+ty)+c33*tz)+2.*zSigmaZ[HALF_STENCIL]-prevSigmaZ[j];
  }
 } 

 return;
}

__global__ void forwardKernelTopBlock(float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2){
 
 __shared__ float sSigmaX[BLOCK_DIM+2*HALF_STENCIL][BLOCK_DIM+2*HALF_STENCIL]; 
 
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int i=ix+iy*nx;
  
  int six=threadIdx.x+HALF_STENCIL;
  int siy=threadIdx.y+HALF_STENCIL;
  
  float zSigmaZ[2*HALF_STENCIL+1];
  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   zSigmaZ[iz+1]=0.;
   zSigmaZ[iz+1+HALF_STENCIL]=curSigmaZ1[j];
  }

  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   
   zSigmaZ[0]=zSigmaZ[1];
   zSigmaZ[1]=zSigmaZ[2];
   zSigmaZ[2]=zSigmaZ[3];
   zSigmaZ[3]=zSigmaZ[4];
   zSigmaZ[4]=zSigmaZ[5];
   zSigmaZ[5]=zSigmaZ[6];
   zSigmaZ[6]=zSigmaZ[7];
   zSigmaZ[7]=zSigmaZ[8];
   zSigmaZ[8]=curSigmaZ2[j];
   
   __syncthreads();
   
   sSigmaX[six][siy]=curSigmaX1[j];
   
   if(threadIdx.x<HALF_STENCIL){
    int k=min(blockDim.x,nx-2*HALF_STENCIL-blockIdx.x*blockDim.x);
    sSigmaX[threadIdx.x][siy]=curSigmaX1[j-HALF_STENCIL];
	sSigmaX[six+k][siy]=curSigmaX1[j+k];
   }
   
   if(threadIdx.y<HALF_STENCIL){
    int k=min(blockDim.y,ny-2*HALF_STENCIL-blockIdx.y*blockDim.y);
    sSigmaX[six][threadIdx.y]=curSigmaX1[j-HALF_STENCIL*nx];
	sSigmaX[six][siy+k]=curSigmaX1[j+k*nx];
   }

   __syncthreads();

   float c33=v[j]*v[j];
   float c11=c33*(1.+2.*eps[j]);
   float c13=c33*sqrt(1.+2.*del[j]);
  
   float tx=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six-1][siy]+sSigmaX[six+1][siy])
                                 +C2*(sSigmaX[six-2][siy]+sSigmaX[six+2][siy])
                                 +C3*(sSigmaX[six-3][siy]+sSigmaX[six+3][siy])
                                 +C4*(sSigmaX[six-4][siy]+sSigmaX[six+4][siy]))/dx2;
   float ty=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six][siy-1]+sSigmaX[six][siy+1])
                                 +C2*(sSigmaX[six][siy-2]+sSigmaX[six][siy+2])
                                 +C3*(sSigmaX[six][siy-3]+sSigmaX[six][siy+3])
                                 +C4*(sSigmaX[six][siy-4]+sSigmaX[six][siy+4]))/dy2;
   float tz=(C0*zSigmaZ[HALF_STENCIL]+C1*(zSigmaZ[HALF_STENCIL-1]+zSigmaZ[HALF_STENCIL+1])
                                     +C2*(zSigmaZ[HALF_STENCIL-2]+zSigmaZ[HALF_STENCIL+2])
                                     +C3*(zSigmaZ[HALF_STENCIL-3]+zSigmaZ[HALF_STENCIL+3])
                                     +C4*(zSigmaZ[HALF_STENCIL-4]+zSigmaZ[HALF_STENCIL+4]))/dz2;
   
   nextSigmaX[j]=dt2*(c11*(tx+ty)+c13*tz)+2.*sSigmaX[six][siy]-prevSigmaX[j];
   nextSigmaZ[j]=dt2*(c13*(tx+ty)+c33*tz)+2.*zSigmaZ[HALF_STENCIL]-prevSigmaZ[j];
  }
 } 

 return;
}

__global__ void forwardKernel(float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2){
 
 __shared__ float sSigmaX[BLOCK_DIM+2*HALF_STENCIL][BLOCK_DIM+2*HALF_STENCIL]; 
 
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int i=ix+iy*nx;
  
  int six=threadIdx.x+HALF_STENCIL;
  int siy=threadIdx.y+HALF_STENCIL;
  
  float zSigmaZ[2*HALF_STENCIL+1];
  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   zSigmaZ[iz+1]=curSigmaZ0[j];
   zSigmaZ[iz+1+HALF_STENCIL]=curSigmaZ1[j];
  }

  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   
   zSigmaZ[0]=zSigmaZ[1];
   zSigmaZ[1]=zSigmaZ[2];
   zSigmaZ[2]=zSigmaZ[3];
   zSigmaZ[3]=zSigmaZ[4];
   zSigmaZ[4]=zSigmaZ[5];
   zSigmaZ[5]=zSigmaZ[6];
   zSigmaZ[6]=zSigmaZ[7];
   zSigmaZ[7]=zSigmaZ[8];
   zSigmaZ[8]=curSigmaZ2[j];
   
   __syncthreads();
   
   sSigmaX[six][siy]=curSigmaX1[j];
   
   if(threadIdx.x<HALF_STENCIL){
    int k=min(blockDim.x,nx-2*HALF_STENCIL-blockIdx.x*blockDim.x);
    sSigmaX[threadIdx.x][siy]=curSigmaX1[j-HALF_STENCIL];
	sSigmaX[six+k][siy]=curSigmaX1[j+k];
   }
   
   if(threadIdx.y<HALF_STENCIL){
    int k=min(blockDim.y,ny-2*HALF_STENCIL-blockIdx.y*blockDim.y);
    sSigmaX[six][threadIdx.y]=curSigmaX1[j-HALF_STENCIL*nx];
	sSigmaX[six][siy+k]=curSigmaX1[j+k*nx];
   }

   __syncthreads();

   float c33=v[j]*v[j];
   float c11=c33*(1.+2.*eps[j]);
   float c13=c33*sqrt(1.+2.*del[j]);
  
   float tx=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six-1][siy]+sSigmaX[six+1][siy])
                                 +C2*(sSigmaX[six-2][siy]+sSigmaX[six+2][siy])
                                 +C3*(sSigmaX[six-3][siy]+sSigmaX[six+3][siy])
                                 +C4*(sSigmaX[six-4][siy]+sSigmaX[six+4][siy]))/dx2;
   float ty=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six][siy-1]+sSigmaX[six][siy+1])
                                 +C2*(sSigmaX[six][siy-2]+sSigmaX[six][siy+2])
                                 +C3*(sSigmaX[six][siy-3]+sSigmaX[six][siy+3])
                                 +C4*(sSigmaX[six][siy-4]+sSigmaX[six][siy+4]))/dy2;
   float tz=(C0*zSigmaZ[HALF_STENCIL]+C1*(zSigmaZ[HALF_STENCIL-1]+zSigmaZ[HALF_STENCIL+1])
                                     +C2*(zSigmaZ[HALF_STENCIL-2]+zSigmaZ[HALF_STENCIL+2])
                                     +C3*(zSigmaZ[HALF_STENCIL-3]+zSigmaZ[HALF_STENCIL+3])
                                     +C4*(zSigmaZ[HALF_STENCIL-4]+zSigmaZ[HALF_STENCIL+4]))/dz2;
   
   nextSigmaX[j]=dt2*(c11*(tx+ty)+c13*tz)+2.*sSigmaX[six][siy]-prevSigmaX[j];
   nextSigmaZ[j]=dt2*(c13*(tx+ty)+c33*tz)+2.*zSigmaZ[HALF_STENCIL]-prevSigmaZ[j];
  }
 } 

 return;
}

__global__ void injectSource(float *SigmaX,float *SigmaZ,float source,int souIndexBlock){
 SigmaX[souIndexBlock]+=source;
 SigmaZ[souIndexBlock]+=source;
 return;
}

__global__ void recordData(float *data,float *SigmaX,float *SigmaZ,int nr,const int *recIndex){
 int ir=threadIdx.x+blockIdx.x*blockDim.x;
 if(ir<nr){
  data[ir]=2./3.*SigmaX[recIndex[ir]]+1./3.*SigmaZ[recIndex[ir]];
 }
 return;
}

void init_abc(float *damping,int nx,int ny,int npad){
	#pragma omp parallel for num_threads(16)
	for(int iy=0;iy<ny;iy++){
		float dist;
		if(iy<npad) dist=npad-iy;
		else if(iy>ny-npad) dist=iy-ny+npad;
		else dist=0.;
		float dampingY=DAMPER+(1.-DAMPER)*cos(PI*dist/npad);
		for(int ix=0;ix<nx;ix++){
			if(ix<npad) dist=npad-ix;
			else if(ix>nx-npad) dist=ix-nx+npad;
			else dist=0.;
			float dampingX=DAMPER+(1.-DAMPER)*cos(PI*dist/npad);
			damping[ix+iy*nx]=dampingX*dampingY;
		}
	}
	
	return;
}

__global__ void abc(int ib,int nx,int ny,int nz,int npad,float *nextSigmaX,float *curSigmaX1,float *nextSigmaZ,float *curSigmaZ1,const float *damping){
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int ixy=ix+iy*nx;

  float dist,dampingZ;
  for(int i=0;i<HALF_STENCIL;++i){
   int iz=ib*HALF_STENCIL+i;
   
   if(iz<npad) dist=npad-iz;
   else if(iz>nz-npad) dist=iz-nz+npad;
   else dist=0.;
   dampingZ=DAMPER+(1.-DAMPER)*cos(PI*dist/npad);
   
   dampingZ*=damping[ix+iy*nx];

   int j=ixy+i*nx*ny;
   nextSigmaX[j]*=dampingZ;
   curSigmaX1[j]*=dampingZ;
   nextSigmaZ[j]*=dampingZ;
   curSigmaZ1[j]*=dampingZ;
  }
 }
	
 return;
}

__global__ void abc(int ib,int nx,int ny,int nz,int npad,float *nextSigmaX,float *curSigmaX1,float *nextSigmaZ,float *curSigmaZ1){
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int ixy=ix+iy*nx;

   float dist;
   if(ix<npad) dist=npad-ix;
   else if(ix>nx-npad) dist=ix-nx+npad;
   else dist=0.;
   float dampingX=DAMPER+(1.-DAMPER)*cos(PI*dist/npad);
   
   if(iy<npad) dist=npad-iy;
   else if(iy>ny-npad) dist=iy-ny+npad;
   else dist=0.;
   float dampingY=DAMPER+(1.-DAMPER)*cos(PI*dist/npad);
   
  for(int i=0;i<HALF_STENCIL;++i){
   int iz=ib*HALF_STENCIL+i;
   
   if(iz<npad) dist=npad-iz;
   else if(iz>nz-npad) dist=iz-nz+npad;
   else dist=0.;
   float dampingZ=DAMPER+(1.-DAMPER)*cos(PI*dist/npad);
   
   float damping=dampingX*dampingY*dampingZ;

   int j=ixy+i*nx*ny;
   nextSigmaX[j]*=damping;
   curSigmaX1[j]*=damping;
   nextSigmaZ[j]*=damping;
   curSigmaZ1[j]*=damping;
  }
 }
	
 return;
}

bool forwardAbc(int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,const int *nbuffSigma,const float *d_damping,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,int nr,const int *recIndex,int recBlock,int samplingTimeStep,float *data,int nx,int ny,int nz,int npad,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu){
 dim3 block(BLOCK_DIM,BLOCK_DIM);
 dim3 grid((nx-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM,(ny-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM);
 
 bool record=false;
 for(int i=b;i<e;++i){
  int nb=nz/HALF_STENCIL;
  int ib=(k-3-i)%nb;
  int it=(k-3-i)/nb*NUPDATE*NGPU+gpu*NUPDATE+2+i;
  
  if(ib==0){
   forwardKernelTopBlock<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else if(ib==nb-1){
   forwardKernelBottomBlock<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else{
   forwardKernel<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  
  if(ib==souBlock){
   float source=dt2*wavelet[it-1];
   injectSource<<<1,1,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],source,souIndexBlock);
  }
 
  abc<<<grid,block,0,*stream>>>(ib,nx,ny,nz,npad,d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_damping);
  
  if(ib==recBlock && it%samplingTimeStep==0){
   recordData<<<(nr+BLOCK_DIM-1)/BLOCK_DIM,BLOCK_DIM,0,*stream>>>(data,d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],nr,recIndex);
   record=true;
  }
 }
 
 return record;
}

bool forwardAbc(int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,const int *nbuffSigma,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,int nr,const int *recIndex,int recBlock,int samplingTimeStep,float *data,int nx,int ny,int nz,int npad,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu){
 dim3 block(BLOCK_DIM,BLOCK_DIM);
 dim3 grid((nx-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM,(ny-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM);
 
 bool record=false;
 for(int i=b;i<e;++i){
  int nb=nz/HALF_STENCIL;
  int ib=(k-3-i)%nb;
  int it=(k-3-i)/nb*NUPDATE*NGPU+gpu*NUPDATE+2+i;
  
  if(ib==0){
   forwardKernelTopBlock<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else if(ib==nb-1){
   forwardKernelBottomBlock<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else{
   forwardKernel<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  
  if(ib==souBlock){
   float source=dt2*wavelet[it-1];
   injectSource<<<1,1,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],source,souIndexBlock);
  }
 
  abc<<<grid,block,0,*stream>>>(ib,nx,ny,nz,npad,d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]]);
  
  if(ib==recBlock && it%samplingTimeStep==0){
   recordData<<<(nr+BLOCK_DIM-1)/BLOCK_DIM,BLOCK_DIM,0,*stream>>>(data,d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],nr,recIndex);
   record=true;
  }
 }
 
 return record;
}

void modelData1Shot1GPU(float *data,float soulocX,float soulocY,float soulocZ,float *recloc,int nr,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float dx,float dy,float dz,float dt,float samplingRate,int device){
 cudaSetDevice(device);
 fprintf(stderr,"NUPDATE %d\n",NUPDATE);
 
 float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;
 long long nxyz=nx*ny*nz;
 int nxy=nx*ny;
 
 float *damping=new float[nxy];
 init_abc(damping,nx,ny,npad);
 float *d_damping;
 cudaMalloc(&d_damping,nxy*sizeof(float));
 cudaMemcpy(d_damping,damping,nxy*sizeof(float),cudaMemcpyHostToDevice);

 int *recIndexBlock=new int[nr];

 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  recIndexBlock[ir]=ixy+(iz%HALF_STENCIL)*nxy;
 }
 int recBlock=recloc[2]/dz/HALF_STENCIL;
 
 int *d_recIndex;
 cudaMalloc(&d_recIndex,nr*sizeof(int));
 cudaMemcpy(d_recIndex,recIndexBlock,nr*sizeof(int),cudaMemcpyHostToDevice);

 int samplingTimeStep=std::round(samplingRate/dt);

 float *prevSigmaX=new float[nxyz]();
 float *curSigmaX=new float[nxyz]();
 float *prevSigmaZ=new float[nxyz]();
 float *curSigmaZ=new float[nxyz]();

 int souIndexX=soulocX/dx;
 int souIndexY=soulocY/dy;
 int souIndexZ=soulocZ/dz;
 int souIndex=souIndexX+souIndexY*nx+souIndexZ*nxy;
 int souIndexBlock=souIndexX+souIndexY*nx+(souIndexZ%HALF_STENCIL)*nxy;
 int souBlock=souIndexZ/HALF_STENCIL;

 int nsave=(nt-2)/NUPDATE+1;
 float *shotWavefield=new float[nx*nz*nsave];

 //injecting source at time 0 to wavefields at time 1
 curSigmaX[souIndex]=dt2*wavelet[0];
 curSigmaZ[souIndex]=dt2*wavelet[0];
 
 size_t nElemBlock=HALF_STENCIL*nxy;
 size_t nByteBlock=nElemBlock*sizeof(float);
 int nb=nz/HALF_STENCIL;

 float *h_v[2],*h_eps[2],*h_del[2];
 float *h_prevSigmaX[2],*h_curSigmaX[2],*h_SigmaX4[2],*h_SigmaX5[2];
 float *h_prevSigmaZ[2],*h_curSigmaZ[2],*h_SigmaZ4[2],*h_SigmaZ5[2];
 float *h_data[2],*d_data[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_v[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_eps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_del[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_data[i],nr*sizeof(float),cudaHostAllocDefault);
  cudaMalloc(&d_data[i],nr*sizeof(float));
 }

 const int nd_Sigma=NUPDATE+2;
 float **d_SigmaX[nd_Sigma],**d_SigmaZ[nd_Sigma];
 int nbuffSigma[nd_Sigma];
 
 for(int i=0;i<nd_Sigma;++i) nbuffSigma[i]=3;
 nbuffSigma[1]=4;nbuffSigma[nd_Sigma-2]=4;
 
 size_t nByteAlloc=0;

 for(int i=0;i<nd_Sigma;++i){
  d_SigmaX[i]=new float*[nbuffSigma[i]]();
  d_SigmaZ[i]=new float*[nbuffSigma[i]]();
  for(int j=0;j<nbuffSigma[i];++j){
   cudaMalloc(&d_SigmaX[i][j],nByteBlock); 
   cudaMalloc(&d_SigmaZ[i][j],nByteBlock); 
   nByteAlloc+=2*nByteBlock;
   cudaMemset(d_SigmaX[i][j],0,nByteBlock);
   cudaMemset(d_SigmaZ[i][j],0,nByteBlock);
  }
 }

 const int nbuffVEpsDel=NUPDATE+4;
 float *d_v[nbuffVEpsDel],*d_eps[nbuffVEpsDel],*d_del[nbuffVEpsDel];
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaMalloc(&d_v[i],nByteBlock);
  cudaMalloc(&d_eps[i],nByteBlock);
  cudaMalloc(&d_del[i],nByteBlock);
  nByteAlloc+=3*nByteBlock;
  cudaMemset(d_v[i],0,nByteBlock);
  cudaMemset(d_eps[i],0,nByteBlock);
  cudaMemset(d_del[i],0,nByteBlock);
 }

 fprintf(stderr,"alloc %f MBs on GPU\n",nByteAlloc*1e-6);

 cudaStream_t computeStream,transfInStream,transfOutStream;
 cudaStreamCreate(&computeStream);
 cudaStreamCreate(&transfInStream);
 cudaStreamCreate(&transfOutStream);

 vector<thread> threads;
 
 int ntb=(nt-2)/NUPDATE*nb;
 int j=0,krecord=0;

 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 for(int k=0;k<ntb+NUPDATE+5;k++){
   if(k<ntb){
	int ib=k%nb;
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(k>0 && k<ntb+1){
    memcpyCpuToGpu3(d_v[k%nbuffVEpsDel],h_v[(k-1)%2],d_eps[k%nbuffVEpsDel],h_eps[(k-1)%2],d_del[k%nbuffVEpsDel],h_del[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k-1)%2],d_SigmaX[1][k%nbuffSigma[1]],h_curSigmaX[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k-1)%2],d_SigmaZ[1][k%nbuffSigma[1]],h_curSigmaZ[(k-1)%2],nByteBlock,&transfInStream);
   }
   
   if(k>2 && k<ntb+NUPDATE+2){
    bool record=forwardAbc(max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,nbuffSigma,d_damping,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nr,d_recIndex,recBlock,samplingTimeStep,d_data[k%2],nx,ny,nz,npad,dx2,dy2,dz2,dt2,&computeStream);
//    bool record=forwardAbc(max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nr,d_recIndex,recBlock,samplingTimeStep,d_data[k%2],nx,ny,nz,npad,dx2,dy2,dz2,dt2,&computeStream);
    if(record){ 
	 krecord=k;
	 j++;
	}
   }
   
   if(k>NUPDATE+3 && k<ntb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[k%2],d_SigmaX[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[k%2],d_SigmaX[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[k%2],d_SigmaZ[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[k%2],d_SigmaZ[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
   }
    
    if(k-1==krecord){
     cudaMemcpyAsync(h_data[k%2],d_data[(k+1)%2],nr*sizeof(float),cudaMemcpyDeviceToHost,transfOutStream);
    }
   
   if(k>NUPDATE+4){
	int ib=(k-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(k-1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(k-1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(k-1)%2],nByteBlock);
   }
	
    if(k-2==krecord){
     memcpy(data+j*nr,h_data[(k+1)%2],nr*sizeof(float));
    }
   
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(shotWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 long long n=nx*ny*nz;
 long long ncells=n*(nt-2);
 cout<<"ncells "<<ncells<<endl;
 cout<<"speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl;
 
 cudaError_t e=cudaGetLastError();
 if(e!=cudaSuccess) fprintf(stderr,"error %s\n",cudaGetErrorString(e));

 float *p=new float[nxyz]();
 lin_comb(p,2./3.,curSigmaX,1./3.,curSigmaZ,nxyz);

 write("pressure",p,nxyz);
 to_header("pressure","n1",nx,"o1",0,"d1",dx);
 to_header("pressure","n2",ny,"o2",0,"d2",dy);
 to_header("pressure","n3",nz,"o3",0,"d3",dz);
 delete []p;

 write("shotWavefield",shotWavefield,nx*nz*nsave);
 to_header("shotWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("shotWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("shotWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []shotWavefield;

 delete []prevSigmaX;delete []curSigmaX;
 delete []prevSigmaZ;delete []curSigmaZ;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_v[i]);
  cudaFreeHost(h_eps[i]);
  cudaFreeHost(h_del[i]);
  cudaFreeHost(h_prevSigmaX[i]);
  cudaFreeHost(h_curSigmaX[i]);
  cudaFreeHost(h_SigmaX4[i]);
  cudaFreeHost(h_SigmaX5[i]);
  cudaFreeHost(h_prevSigmaZ[i]);
  cudaFreeHost(h_curSigmaZ[i]);
  cudaFreeHost(h_SigmaZ4[i]);
  cudaFreeHost(h_SigmaZ5[i]);
  cudaFreeHost(h_data[i]);
  cudaFree(d_data[i]);
 }
 
 for(int i=0;i<nd_Sigma;++i){
  for(int j=0;j<nbuffSigma[i];++j){
   cudaFree(d_SigmaX[i][j]); 
   cudaFree(d_SigmaZ[i][j]); 
  }
  delete []d_SigmaX[i];
  delete []d_SigmaZ[i];
 }
 
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaFree(d_v[i]);
  cudaFree(d_eps[i]);
  cudaFree(d_del[i]);
 }
 
 delete []recIndexBlock;
 cudaFree(d_recIndex);

 delete []damping;
 cudaFree(d_damping);
 
 cudaStreamDestroy(computeStream);
 cudaStreamDestroy(transfInStream);
 cudaStreamDestroy(transfOutStream);
 
 return;
}

void modelData1ShotNGpu(float *data,float soulocX,float soulocY,float soulocZ,float *recloc,int nr,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float dx,float dy,float dz,float dt,float samplingRate){
 fprintf(stderr,"NUPDATE %d NGPU %d\n",NUPDATE,NGPU);

 float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;
 long long nxyz=nx*ny*nz;
 int nxy=nx*ny;
 
 int *recIndexBlock=new int[nr];

 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  recIndexBlock[ir]=ixy+(iz%HALF_STENCIL)*nxy;
 }
 int recBlock=recloc[2]/dz/HALF_STENCIL;
 
 int samplingTimeStep=std::round(samplingRate/dt);

 float *prevSigmaX=new float[nxyz]();
 float *curSigmaX=new float[nxyz]();
 float *prevSigmaZ=new float[nxyz]();
 float *curSigmaZ=new float[nxyz]();

 int souIndexX=soulocX/dx;
 int souIndexY=soulocY/dy;
 int souIndexZ=soulocZ/dz;
 int souIndex=souIndexX+souIndexY*nx+souIndexZ*nxy;
 int souIndexBlock=souIndexX+souIndexY*nx+(souIndexZ%HALF_STENCIL)*nxy;
 int souBlock=souIndexZ/HALF_STENCIL;

 int nsave=(nt-2)/NUPDATE/NGPU+1;
 float *shotWavefield=new float[nx*nz*nsave];
 cout<<"nsave "<<nsave<<endl;
 cout<<shotWavefield<<endl;

 //injecting source at time 0 to wavefields at time 1
 curSigmaX[souIndex]=dt2*wavelet[0];
 curSigmaZ[souIndex]=dt2*wavelet[0];
 
 size_t nElemBlock=HALF_STENCIL*nxy;
 size_t nByteBlock=nElemBlock*sizeof(float);
 int nb=nz/HALF_STENCIL;

 float *h_v[2],*h_eps[2],*h_del[2];
 float *h_prevSigmaX[2],*h_curSigmaX[2],*h_SigmaX4[2],*h_SigmaX5[2];
 float *h_prevSigmaZ[2],*h_curSigmaZ[2],*h_SigmaZ4[2],*h_SigmaZ5[2];
 float *h_data[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_v[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_eps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_del[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_data[i],nByteBlock,cudaHostAllocDefault);
 }

 const int nd_Sigma=NUPDATE+2;
 int nbuffSigma[nd_Sigma];
 
 int **d_recIndex=new int*[NGPU]();
 float ***d_data=new float**[NGPU]();
 
 for(int i=0;i<nd_Sigma;++i) nbuffSigma[i]=3;
 nbuffSigma[1]=4;nbuffSigma[nd_Sigma-2]=4;

 float ****d_SigmaX=new float ***[NGPU]();
 float ****d_SigmaZ=new float ***[NGPU]();
 
 const int nbuffVEpsDel=NUPDATE+4;
 float ***d_v=new float**[NGPU]();
 float ***d_eps=new float**[NGPU]();
 float ***d_del=new float**[NGPU]();
 
 cudaStream_t *transfInStream=new cudaStream_t[1]();
 cudaStream_t *transfOutStream=new cudaStream_t[NGPU]();
 cudaStream_t *computeStream=new cudaStream_t[NGPU]();
 
 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(gpu);
  
  cudaMalloc(&d_recIndex[gpu],nr*sizeof(int));
  cudaMemcpy(d_recIndex[gpu],recIndexBlock,nr*sizeof(int),cudaMemcpyHostToDevice);
  
  d_data[gpu]=new float*[2]();
  for(int i=0;i<2;i++) cudaMalloc(&d_data[gpu][i],nr*sizeof(float));
  
  d_SigmaX[gpu]=new float**[nd_Sigma]();
  d_SigmaZ[gpu]=new float**[nd_Sigma]();
  for(int i=0;i<nd_Sigma;++i){
   d_SigmaX[gpu][i]=new float*[nbuffSigma[i]]();
   d_SigmaZ[gpu][i]=new float*[nbuffSigma[i]]();
   for(int j=0;j<nbuffSigma[i];++j){
    cudaMalloc(&d_SigmaX[gpu][i][j],nByteBlock); 
    cudaMalloc(&d_SigmaZ[gpu][i][j],nByteBlock); 
    cudaMemset(d_SigmaX[gpu][i][j],0,nByteBlock);
    cudaMemset(d_SigmaZ[gpu][i][j],0,nByteBlock);
   }
  }

  d_v[gpu]=new float*[nbuffVEpsDel]();
  d_eps[gpu]=new float*[nbuffVEpsDel]();
  d_del[gpu]=new float*[nbuffVEpsDel]();
  for(int i=0;i<nbuffVEpsDel;++i){
   cudaMalloc(&d_v[gpu][i],nByteBlock);
   cudaMalloc(&d_eps[gpu][i],nByteBlock);
   cudaMalloc(&d_del[gpu][i],nByteBlock);
   cudaMemset(d_v[gpu][i],0,nByteBlock);
   cudaMemset(d_eps[gpu][i],0,nByteBlock);
   cudaMemset(d_del[gpu][i],0,nByteBlock);
  }
 
  if(gpu==0) cudaStreamCreate(&transfInStream[gpu]);
  cudaStreamCreate(&computeStream[gpu]);
  cudaStreamCreate(&transfOutStream[gpu]);
 }

 vector<thread> threads;

 int ntb=(nt-2)/NUPDATE/NGPU*nb;
 int j=0,krecord=0;

 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 for(int k=0;k<ntb+NGPU*(NUPDATE+3)+2;k++){
   if(k<ntb){
    int ib=k%nb;
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(k>0 && k<ntb+1){
    cudaSetDevice(0);
    memcpyCpuToGpu3(d_v[0][k%nbuffVEpsDel],h_v[(k+1)%2],d_eps[0][k%nbuffVEpsDel],h_eps[(k+1)%2],d_del[0][k%nbuffVEpsDel],h_del[(k+1)%2],nByteBlock,transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k+1)%2],d_SigmaX[0][1][k%nbuffSigma[1]],h_curSigmaX[(k+1)%2],nByteBlock,transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k+1)%2],d_SigmaZ[0][1][k%nbuffSigma[1]],h_curSigmaZ[(k+1)%2],nByteBlock,transfInStream);
   }
  
   for(int gpu=0;gpu<NGPU;gpu++){
    int kgpu=k-gpu*(NUPDATE+3);

    cudaSetDevice(gpu);

    if(kgpu>2 && kgpu<ntb+NUPDATE+2){
     bool record=forwardAbc(max(0,kgpu-ntb-2),min(kgpu-2,NUPDATE),kgpu,d_SigmaX[gpu],d_SigmaZ[gpu],nbuffSigma,d_v[gpu],d_eps[gpu],d_del[gpu],nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nr,d_recIndex[gpu],recBlock,samplingTimeStep,d_data[gpu][kgpu%2],nx,ny,nz,npad,dx2,dy2,dz2,dt2,computeStream+gpu,gpu);
     if(record){
	  krecord=kgpu;
	  j++;
	 }
	}
    
    if(kgpu>NUPDATE+3 && kgpu<ntb+NUPDATE+4){
     if(NGPU>1 && gpu<NGPU-1){
      memcpyGpuToGpu3(d_v[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_v[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],nByteBlock,transfOutStream+gpu);
      memcpyGpuToGpu2(d_SigmaX[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaX[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToGpu2(d_SigmaZ[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaZ[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
     }
     else{
      memcpyGpuToCpu2(h_SigmaX4[kgpu%2],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[kgpu%2],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToCpu2(h_SigmaZ4[kgpu%2],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[kgpu%2],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
     }
    }
    
	if(kgpu-1==krecord){
     cudaMemcpyAsync(h_data[kgpu%2],d_data[gpu][(kgpu+1)%2],nr*sizeof(float),cudaMemcpyDeviceToHost,transfOutStream[gpu]);
    }
 
    if(kgpu-2==krecord){
     memcpy(data+j*nr,h_data[(kgpu+1)%2],nr*sizeof(float));
    }
   }
   
   if(k>NGPU*(NUPDATE+3)+1){
    int kgpu=k-(NGPU-1)*(NUPDATE+3);
	int ib=(kgpu-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(kgpu+1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(kgpu+1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(kgpu+1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(kgpu+1)%2],nByteBlock);
   }
  
   for(int gpu=0;gpu<NGPU;gpu++){
    cudaSetDevice(gpu);
    cudaDeviceSynchronize();
   }
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
   
   int ksave=k-NGPU*(NUPDATE+3)-1;
   if(ksave>=0 && ksave%nb==0){
	int itsave=ksave/nb;
	for(int iz=0;iz<nz;iz++) memcpy(shotWavefield+iz*nx+itsave*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
   }
 }

 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 long long n=nx*ny*nz;
 long long ncells=n*(nt-2);
 cout<<"ncells "<<ncells<<endl;
 cout<<"speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl;
 
 float *p=new float[nxyz]();
 lin_comb(p,2./3.,curSigmaX,1./3.,curSigmaZ,nxyz);

 write("pressure",p,nxyz);
 to_header("pressure","n1",nx,"o1",0,"d1",dx);
 to_header("pressure","n2",ny,"o2",0,"d2",dy);
 to_header("pressure","n3",nz,"o3",0,"d3",dz);
 delete []p;

 write("shotWavefield",shotWavefield,nx*nz*nsave);
 to_header("shotWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("shotWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("shotWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []shotWavefield;

 delete []prevSigmaX;delete []curSigmaX;
 delete []prevSigmaZ;delete []curSigmaZ;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_v[i]);
  cudaFreeHost(h_eps[i]);
  cudaFreeHost(h_del[i]);
  cudaFreeHost(h_prevSigmaX[i]);
  cudaFreeHost(h_curSigmaX[i]);
  cudaFreeHost(h_SigmaX4[i]);
  cudaFreeHost(h_SigmaX5[i]);
  cudaFreeHost(h_prevSigmaZ[i]);
  cudaFreeHost(h_curSigmaZ[i]);
  cudaFreeHost(h_SigmaZ4[i]);
  cudaFreeHost(h_SigmaZ5[i]);
  cudaFreeHost(h_data[i]);
 }
 
 cout<<"here"<<endl;
 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(gpu);

  cudaFree(d_recIndex[gpu]);

  for(int i=0;i<2;i++) cudaFree(d_data[gpu][i]);
  delete []d_data[gpu];
  
  for(int i=0;i<nd_Sigma;++i){
   for(int j=0;j<nbuffSigma[i];++j){
    cudaFree(d_SigmaX[gpu][i][j]); 
    cudaFree(d_SigmaZ[gpu][i][j]); 
   }
   delete []d_SigmaX[gpu][i];
   delete []d_SigmaZ[gpu][i];
  }
  delete []d_SigmaX[gpu];
  delete []d_SigmaZ[gpu];

  for(int i=0;i<nbuffVEpsDel;++i){
   cudaFree(d_v[gpu][i]);
   cudaFree(d_eps[gpu][i]);
   cudaFree(d_del[gpu][i]);
  }
  delete []d_v[gpu];
  delete []d_eps[gpu];
  delete []d_del[gpu];

  if(gpu==0) cudaStreamDestroy(transfInStream[gpu]);
  cudaStreamDestroy(computeStream[gpu]);
  cudaStreamDestroy(transfOutStream[gpu]);
 
  cudaError_t e=cudaGetLastError();
  if(e!=cudaSuccess) fprintf(stderr,"gpu %d error %s\n",gpu,cudaGetErrorString(e));
 }

 cout<<"here"<<endl;
 delete []d_recIndex;
 delete []d_data;
 delete []d_SigmaX;
 delete []d_SigmaZ;
 delete []d_v;
 delete []d_eps;
 delete []d_del;
 delete []transfInStream;
 delete []computeStream;
 delete []transfOutStream;
 
 delete []recIndexBlock;
 
 return;
}

void forwardRandom(int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,const int *nbuffSigma,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,int nx,int ny,int nz,int npad,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu){
 dim3 block(BLOCK_DIM,BLOCK_DIM);
 dim3 grid((nx-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM,(ny-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM);
 
 for(int i=b;i<e;++i){
  int nb=nz/HALF_STENCIL;
  int ib=(k-3-i)%nb;
  int it=(k-3-i)/nb*NUPDATE*NGPU+gpu*NUPDATE+2+i;
  
  if(ib==0){
   forwardKernelTopBlock<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else if(ib==nb-1){
   forwardKernelBottomBlock<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else{
   forwardKernel<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  
  if(ib==souBlock){
   float source=dt2*wavelet[it-1];
   injectSource<<<1,1,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],source,souIndexBlock);
  }
 }
 
 return;
}

void backwardRandom(int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,const int *nbuffSigma,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,int nx,int ny,int nz,int npad,int nt,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu){
 dim3 block(BLOCK_DIM,BLOCK_DIM);
 dim3 grid((nx-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM,(ny-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM);
 
 for(int i=b;i<e;++i){
  int nb=nz/HALF_STENCIL;
  int ib=(k-3-i)%nb;
  int it=(k-3-i)/nb*NUPDATE*NGPU+gpu*NUPDATE+2+i;
  it=nt-1-it;
  
  if(ib==0){
   forwardKernelTopBlock<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else if(ib==nb-1){
   forwardKernelBottomBlock<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else{
   forwardKernel<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  
  if(ib==souBlock){
   float source=dt2*wavelet[it+1];
   injectSource<<<1,1,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],source,souIndexBlock);
  }
 }
 
 return;
}

__global__ void adjointKernelBottomBlock(float *prevSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *nextSigmaX,float *prevSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *nextSigmaZ,float *v0,float *v1,float *v2,float *eps0,float *eps1,float *eps2,float *del0,float *del1,float *del2,int nx,int ny,float dx2,float dy2,float dz2,float dt2){
 
 __shared__ float sSigmaX[BLOCK_DIM+2*HALF_STENCIL][BLOCK_DIM+2*HALF_STENCIL]; 
 
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int i=ix+iy*nx;
  
  int six=threadIdx.x+HALF_STENCIL;
  int siy=threadIdx.y+HALF_STENCIL;
  
  float zSigmaZ[2*HALF_STENCIL+1];
  
  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   float c33=v0[j]*v0[j];
   float c13=c33*sqrt(1.+2.*del0[j]);
   zSigmaZ[iz+1]=c13*curSigmaX0[j]+c33*curSigmaZ0[j];
   c33=v1[j]*v1[j];
   c13=c33*sqrt(1.+2.*del1[j]);
   zSigmaZ[iz+1+HALF_STENCIL]=c13*curSigmaX1[j]+c33*curSigmaZ1[j];
  }

  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   
   zSigmaZ[0]=zSigmaZ[1];
   zSigmaZ[1]=zSigmaZ[2];
   zSigmaZ[2]=zSigmaZ[3];
   zSigmaZ[3]=zSigmaZ[4];
   zSigmaZ[4]=zSigmaZ[5];
   zSigmaZ[5]=zSigmaZ[6];
   zSigmaZ[6]=zSigmaZ[7];
   zSigmaZ[7]=zSigmaZ[8];
   zSigmaZ[8]=0.;
   
   __syncthreads();
   
   float c33=v1[j]*v1[j];
   float c11=c33*(1.+2.*eps1[j]);
   float c13=c33*sqrt(1.+2.*del1[j]);
   
   sSigmaX[six][siy]=c11*curSigmaX1[j]+c13*curSigmaZ1[j];
   
   if(threadIdx.x<HALF_STENCIL){
    int k=min(blockDim.x,nx-2*HALF_STENCIL-blockIdx.x*blockDim.x);
    c33=v1[j-HALF_STENCIL]*v1[j-HALF_STENCIL];
    c11=c33*(1.+2.*eps1[j-HALF_STENCIL]);
    c13=c33*sqrt(1.+2.*del1[j-HALF_STENCIL]);
    sSigmaX[threadIdx.x][siy]=c11*curSigmaX1[j-HALF_STENCIL]+c13*curSigmaZ1[j-HALF_STENCIL];
    c33=v1[j+k]*v1[j+k];
    c11=c33*(1.+2.*eps1[j+k]);
    c13=c33*sqrt(1.+2.*del1[j+k]);
	sSigmaX[six+k][siy]=c11*curSigmaX1[j+k]+c13*curSigmaZ1[j+k];
   }
   
   if(threadIdx.y<HALF_STENCIL){
    int k=min(blockDim.y,ny-2*HALF_STENCIL-blockIdx.y*blockDim.y);
    c33=v1[j-HALF_STENCIL*nx]*v1[j-HALF_STENCIL*nx];
    c11=c33*(1.+2.*eps1[j-HALF_STENCIL*nx]);
    c13=c33*sqrt(1.+2.*del1[j-HALF_STENCIL*nx]);
    sSigmaX[six][threadIdx.y]=c11*curSigmaX1[j-HALF_STENCIL*nx]+c13*curSigmaZ1[j-HALF_STENCIL*nx];
    c33=v1[j+k*nx]*v1[j+k*nx];
    c11=c33*(1.+2.*eps1[j+k*nx]);
    c13=c33*sqrt(1.+2.*del1[j+k*nx]);
	sSigmaX[six][siy+k]=c11*curSigmaX1[j+k*nx]+c13*curSigmaZ1[j+k*nx];
   }

   __syncthreads();
  
   float tx=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six-1][siy]+sSigmaX[six+1][siy])
                                 +C2*(sSigmaX[six-2][siy]+sSigmaX[six+2][siy])
                                 +C3*(sSigmaX[six-3][siy]+sSigmaX[six+3][siy])
                                 +C4*(sSigmaX[six-4][siy]+sSigmaX[six+4][siy]))/dx2;
   float ty=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six][siy-1]+sSigmaX[six][siy+1])
                                 +C2*(sSigmaX[six][siy-2]+sSigmaX[six][siy+2])
                                 +C3*(sSigmaX[six][siy-3]+sSigmaX[six][siy+3])
                                 +C4*(sSigmaX[six][siy-4]+sSigmaX[six][siy+4]))/dy2;
   float tz=(C0*zSigmaZ[HALF_STENCIL]+C1*(zSigmaZ[HALF_STENCIL-1]+zSigmaZ[HALF_STENCIL+1])
                                     +C2*(zSigmaZ[HALF_STENCIL-2]+zSigmaZ[HALF_STENCIL+2])
                                     +C3*(zSigmaZ[HALF_STENCIL-3]+zSigmaZ[HALF_STENCIL+3])
                                     +C4*(zSigmaZ[HALF_STENCIL-4]+zSigmaZ[HALF_STENCIL+4]))/dz2;
   
   prevSigmaX[j]=dt2*(tx+ty)+2.*curSigmaX1[j]-nextSigmaX[j];
   prevSigmaZ[j]=dt2*tz+2.*curSigmaZ1[j]-nextSigmaZ[j];
  }
 } 

 return;
}

__global__ void adjointKernelTopBlock(float *prevSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *nextSigmaX,float *prevSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *nextSigmaZ,float *v0,float *v1,float *v2,float *eps0,float *eps1,float *eps2,float *del0,float *del1,float *del2,int nx,int ny,float dx2,float dy2,float dz2,float dt2){
 
 __shared__ float sSigmaX[BLOCK_DIM+2*HALF_STENCIL][BLOCK_DIM+2*HALF_STENCIL]; 
 
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int i=ix+iy*nx;
  
  int six=threadIdx.x+HALF_STENCIL;
  int siy=threadIdx.y+HALF_STENCIL;
  
  float zSigmaZ[2*HALF_STENCIL+1];
  
  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   zSigmaZ[iz+1]=0.;
   float c33=v1[j]*v1[j];
   float c13=c33*sqrt(1.+2.*del1[j]);
   zSigmaZ[iz+1+HALF_STENCIL]=c13*curSigmaX1[j]+c33*curSigmaZ1[j];
  }

  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   
   float c33=v2[j]*v2[j];
   float c13=c33*sqrt(1.+2.*del2[j]);
   
   zSigmaZ[0]=zSigmaZ[1];
   zSigmaZ[1]=zSigmaZ[2];
   zSigmaZ[2]=zSigmaZ[3];
   zSigmaZ[3]=zSigmaZ[4];
   zSigmaZ[4]=zSigmaZ[5];
   zSigmaZ[5]=zSigmaZ[6];
   zSigmaZ[6]=zSigmaZ[7];
   zSigmaZ[7]=zSigmaZ[8];
   zSigmaZ[8]=c13*curSigmaX2[j]+c33*curSigmaZ2[j];
   
   __syncthreads();
   
   c33=v1[j]*v1[j];
   float c11=c33*(1.+2.*eps1[j]);
   c13=c33*sqrt(1.+2.*del1[j]);
   
   sSigmaX[six][siy]=c11*curSigmaX1[j]+c13*curSigmaZ1[j];
   
   if(threadIdx.x<HALF_STENCIL){
    int k=min(blockDim.x,nx-2*HALF_STENCIL-blockIdx.x*blockDim.x);
    c33=v1[j-HALF_STENCIL]*v1[j-HALF_STENCIL];
    c11=c33*(1.+2.*eps1[j-HALF_STENCIL]);
    c13=c33*sqrt(1.+2.*del1[j-HALF_STENCIL]);
    sSigmaX[threadIdx.x][siy]=c11*curSigmaX1[j-HALF_STENCIL]+c13*curSigmaZ1[j-HALF_STENCIL];
    c33=v1[j+k]*v1[j+k];
    c11=c33*(1.+2.*eps1[j+k]);
    c13=c33*sqrt(1.+2.*del1[j+k]);
	sSigmaX[six+k][siy]=c11*curSigmaX1[j+k]+c13*curSigmaZ1[j+k];
   }
   
   if(threadIdx.y<HALF_STENCIL){
    int k=min(blockDim.y,ny-2*HALF_STENCIL-blockIdx.y*blockDim.y);
    c33=v1[j-HALF_STENCIL*nx]*v1[j-HALF_STENCIL*nx];
    c11=c33*(1.+2.*eps1[j-HALF_STENCIL*nx]);
    c13=c33*sqrt(1.+2.*del1[j-HALF_STENCIL*nx]);
    sSigmaX[six][threadIdx.y]=c11*curSigmaX1[j-HALF_STENCIL*nx]+c13*curSigmaZ1[j-HALF_STENCIL*nx];
    c33=v1[j+k*nx]*v1[j+k*nx];
    c11=c33*(1.+2.*eps1[j+k*nx]);
    c13=c33*sqrt(1.+2.*del1[j+k*nx]);
	sSigmaX[six][siy+k]=c11*curSigmaX1[j+k*nx]+c13*curSigmaZ1[j+k*nx];
   }

   __syncthreads();
  
   float tx=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six-1][siy]+sSigmaX[six+1][siy])
                                 +C2*(sSigmaX[six-2][siy]+sSigmaX[six+2][siy])
                                 +C3*(sSigmaX[six-3][siy]+sSigmaX[six+3][siy])
                                 +C4*(sSigmaX[six-4][siy]+sSigmaX[six+4][siy]))/dx2;
   float ty=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six][siy-1]+sSigmaX[six][siy+1])
                                 +C2*(sSigmaX[six][siy-2]+sSigmaX[six][siy+2])
                                 +C3*(sSigmaX[six][siy-3]+sSigmaX[six][siy+3])
                                 +C4*(sSigmaX[six][siy-4]+sSigmaX[six][siy+4]))/dy2;
   float tz=(C0*zSigmaZ[HALF_STENCIL]+C1*(zSigmaZ[HALF_STENCIL-1]+zSigmaZ[HALF_STENCIL+1])
                                     +C2*(zSigmaZ[HALF_STENCIL-2]+zSigmaZ[HALF_STENCIL+2])
                                     +C3*(zSigmaZ[HALF_STENCIL-3]+zSigmaZ[HALF_STENCIL+3])
                                     +C4*(zSigmaZ[HALF_STENCIL-4]+zSigmaZ[HALF_STENCIL+4]))/dz2;
   
   prevSigmaX[j]=dt2*(tx+ty)+2.*curSigmaX1[j]-nextSigmaX[j];
   prevSigmaZ[j]=dt2*tz+2.*curSigmaZ1[j]-nextSigmaZ[j];
  }
 } 

 return;
}

__global__ void adjointKernel(float *prevSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *nextSigmaX,float *prevSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *nextSigmaZ,float *v0,float *v1,float *v2,float *eps0,float *eps1,float *eps2,float *del0,float *del1,float *del2,int nx,int ny,float dx2,float dy2,float dz2,float dt2){
 
 __shared__ float sSigmaX[BLOCK_DIM+2*HALF_STENCIL][BLOCK_DIM+2*HALF_STENCIL]; 
 
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int i=ix+iy*nx;
  
  int six=threadIdx.x+HALF_STENCIL;
  int siy=threadIdx.y+HALF_STENCIL;
  
  float zSigmaZ[2*HALF_STENCIL+1];
  
  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   float c33=v0[j]*v0[j];
   float c13=c33*sqrt(1.+2.*del0[j]);
   zSigmaZ[iz+1]=c13*curSigmaX0[j]+c33*curSigmaZ0[j];
   c33=v1[j]*v1[j];
   c13=c33*sqrt(1.+2.*del1[j]);
   zSigmaZ[iz+1+HALF_STENCIL]=c13*curSigmaX1[j]+c33*curSigmaZ1[j];
  }

  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   
   float c33=v2[j]*v2[j];
   float c13=c33*sqrt(1.+2.*del2[j]);
   
   zSigmaZ[0]=zSigmaZ[1];
   zSigmaZ[1]=zSigmaZ[2];
   zSigmaZ[2]=zSigmaZ[3];
   zSigmaZ[3]=zSigmaZ[4];
   zSigmaZ[4]=zSigmaZ[5];
   zSigmaZ[5]=zSigmaZ[6];
   zSigmaZ[6]=zSigmaZ[7];
   zSigmaZ[7]=zSigmaZ[8];
   zSigmaZ[8]=c13*curSigmaX2[j]+c33*curSigmaZ2[j];
   
   __syncthreads();
   
   c33=v1[j]*v1[j];
   float c11=c33*(1.+2.*eps1[j]);
   c13=c33*sqrt(1.+2.*del1[j]);
   
   sSigmaX[six][siy]=c11*curSigmaX1[j]+c13*curSigmaZ1[j];
   
   if(threadIdx.x<HALF_STENCIL){
    int k=min(blockDim.x,nx-2*HALF_STENCIL-blockIdx.x*blockDim.x);
    c33=v1[j-HALF_STENCIL]*v1[j-HALF_STENCIL];
    c11=c33*(1.+2.*eps1[j-HALF_STENCIL]);
    c13=c33*sqrt(1.+2.*del1[j-HALF_STENCIL]);
    sSigmaX[threadIdx.x][siy]=c11*curSigmaX1[j-HALF_STENCIL]+c13*curSigmaZ1[j-HALF_STENCIL];
    c33=v1[j+k]*v1[j+k];
    c11=c33*(1.+2.*eps1[j+k]);
    c13=c33*sqrt(1.+2.*del1[j+k]);
	sSigmaX[six+k][siy]=c11*curSigmaX1[j+k]+c13*curSigmaZ1[j+k];
   }
   
   if(threadIdx.y<HALF_STENCIL){
    int k=min(blockDim.y,ny-2*HALF_STENCIL-blockIdx.y*blockDim.y);
    c33=v1[j-HALF_STENCIL*nx]*v1[j-HALF_STENCIL*nx];
    c11=c33*(1.+2.*eps1[j-HALF_STENCIL*nx]);
    c13=c33*sqrt(1.+2.*del1[j-HALF_STENCIL*nx]);
    sSigmaX[six][threadIdx.y]=c11*curSigmaX1[j-HALF_STENCIL*nx]+c13*curSigmaZ1[j-HALF_STENCIL*nx];
    c33=v1[j+k*nx]*v1[j+k*nx];
    c11=c33*(1.+2.*eps1[j+k*nx]);
    c13=c33*sqrt(1.+2.*del1[j+k*nx]);
	sSigmaX[six][siy+k]=c11*curSigmaX1[j+k*nx]+c13*curSigmaZ1[j+k*nx];
   }

   __syncthreads();
  
   float tx=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six-1][siy]+sSigmaX[six+1][siy])
                                 +C2*(sSigmaX[six-2][siy]+sSigmaX[six+2][siy])
                                 +C3*(sSigmaX[six-3][siy]+sSigmaX[six+3][siy])
                                 +C4*(sSigmaX[six-4][siy]+sSigmaX[six+4][siy]))/dx2;
   float ty=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six][siy-1]+sSigmaX[six][siy+1])
                                 +C2*(sSigmaX[six][siy-2]+sSigmaX[six][siy+2])
                                 +C3*(sSigmaX[six][siy-3]+sSigmaX[six][siy+3])
                                 +C4*(sSigmaX[six][siy-4]+sSigmaX[six][siy+4]))/dy2;
   float tz=(C0*zSigmaZ[HALF_STENCIL]+C1*(zSigmaZ[HALF_STENCIL-1]+zSigmaZ[HALF_STENCIL+1])
                                     +C2*(zSigmaZ[HALF_STENCIL-2]+zSigmaZ[HALF_STENCIL+2])
                                     +C3*(zSigmaZ[HALF_STENCIL-3]+zSigmaZ[HALF_STENCIL+3])
                                     +C4*(zSigmaZ[HALF_STENCIL-4]+zSigmaZ[HALF_STENCIL+4]))/dz2;
   
   prevSigmaX[j]=dt2*(tx+ty)+2.*curSigmaX1[j]-nextSigmaX[j];
   prevSigmaZ[j]=dt2*tz+2.*curSigmaZ1[j]-nextSigmaZ[j];
  }
 } 

 return;
}

__global__ void injectResidual(float *residual,float *SigmaX,float *SigmaZ,int nr,const int *recIndex,float dt2){
 int ir=threadIdx.x+blockIdx.x*blockDim.x;
 if(ir<nr){
  SigmaX[recIndex[ir]]+=2./3.*dt2*residual[ir];
  SigmaZ[recIndex[ir]]+=1./3.*dt2*residual[ir];
 }
 return;
}

void adjoint(int b,int e,int ib,float ***d_SigmaX,float ***d_SigmaZ,const int *nbuffSigma,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *data,int nr,const int *recIndex,int recBlock,int nx,int ny,int nz,int npad,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream){
 dim3 block(BLOCK_DIM,BLOCK_DIM);
 dim3 grid((nx-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM,(ny-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM);
 
 for(int i=b;i<e;++i){
  adjointKernel<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(ib-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(ib-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(ib-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(ib-1)%nbuffSigma[i+1]],d_SigmaX[i][(ib-3)%nbuffSigma[i]],d_SigmaZ[i+2][(ib-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(ib-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(ib-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(ib-1)%nbuffSigma[i+1]],d_SigmaZ[i][(ib-3)%nbuffSigma[i]],d_v[(ib-i-3)%nbuffVEpsDel],d_v[(ib-i-2)%nbuffVEpsDel],d_v[(ib-i-1)%nbuffVEpsDel],d_eps[(ib-i-3)%nbuffVEpsDel],d_eps[(ib-i-2)%nbuffVEpsDel],d_eps[(ib-i-1)%nbuffVEpsDel],d_del[(ib-i-3)%nbuffVEpsDel],d_del[(ib-i-2)%nbuffVEpsDel],d_del[(ib-i-1)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
 
  if(ib<recBlock+3+NUPDATE && i==ib-recBlock-3){
   injectResidual<<<(nr+BLOCK_DIM-1)/BLOCK_DIM,BLOCK_DIM,0,*stream>>>(data,d_SigmaX[i+2][(ib-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(ib-1)%nbuffSigma[i+2]],nr,recIndex,dt2);
  }
 
  int iblock=ib-3-i;
  abc<<<grid,block,0,*stream>>>(iblock,nx,ny,nz,npad,d_SigmaX[i+2][(ib-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(ib-2)%nbuffSigma[i+1]],d_SigmaZ[i+2][(ib-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(ib-2)%nbuffSigma[i+1]]);
 }
 
 return;
}

void interpolateResidual(float *fineResidual,float *coarseResidual,int timeIndex,int nr,int samplingTimeStep){
	float f=float(timeIndex)/float(samplingTimeStep);
	int i=f;
	f=f-i;
	#pragma omp parallel for num_threads(16)
	for(int ir=0;ir<nr;ir++){
		fineResidual[ir]=(1.-f)*coarseResidual[ir+i*nr]+f*coarseResidual[ir+(i+1)*nr];
	}
	return;
}

void propagateReceiver1Shot1GPU(float *data,float *recloc,int nr,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float dx,float dy,float dz,float dt,float samplingRate,int device){
 cudaSetDevice(device);
 
 float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;
 int nxy=nx*ny;
 long long nxyz=nx*ny*nz;
 
 int samplingTimeStep=std::round(samplingRate/dt);
 int nnt=(nt-1)/samplingTimeStep+1;

 float *prevSigmaX=new float[nxyz]();
 float *curSigmaX=new float[nxyz]();
 float *prevSigmaZ=new float[nxyz]();
 float *curSigmaZ=new float[nxyz]();
 
 float *recWavefield=new float[nx*nz*(nt-2)/NUPDATE];

 int *recIndex=new int[nr];

 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  int i=ixy+iz*nxy;
  curSigmaX[i]=dt2*data[ir+(nnt-1)*nr];
  curSigmaZ[i]=dt2*data[ir+(nnt-1)*nr];
  recIndex[ir]=ixy+(iz%HALF_STENCIL)*nxy;
 }

 int *d_recIndex;
 cudaMalloc(&d_recIndex,nr*sizeof(int));
 cudaMemcpy(d_recIndex,recIndex,nr*sizeof(int),cudaMemcpyHostToDevice);

 int recBlock=recloc[2]/dz/HALF_STENCIL;

 size_t nElemBlock=HALF_STENCIL*nxy;
 size_t nByteBlock=nElemBlock*sizeof(float);
 int nb=nz/HALF_STENCIL;

 float *h_v[2],*h_eps[2],*h_del[2];
 float *h_prevSigmaX[2],*h_curSigmaX[2],*h_SigmaX4[2],*h_SigmaX5[2];
 float *h_prevSigmaZ[2],*h_curSigmaZ[2],*h_SigmaZ4[2],*h_SigmaZ5[2];
 float *h_data[2],*d_data[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_v[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_eps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_del[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_data[i],nr*sizeof(float),cudaHostAllocDefault);
  cudaMalloc(&d_data[i],nr*sizeof(float));
 }
 
 const int nd_Sigma=NUPDATE+2;
 float **d_SigmaX[nd_Sigma],**d_SigmaZ[nd_Sigma];
 int nbuffSigma[nd_Sigma];
 
 for(int i=0;i<nd_Sigma;++i) nbuffSigma[i]=3;
 nbuffSigma[1]=4;nbuffSigma[nd_Sigma-2]=4;
 
 size_t nByteAlloc=0;

 for(int i=0;i<nd_Sigma;++i){
  d_SigmaX[i]=new float*[nbuffSigma[i]]();
  d_SigmaZ[i]=new float*[nbuffSigma[i]]();
  for(int j=0;j<nbuffSigma[i];++j){
   cudaMalloc(&d_SigmaX[i][j],nByteBlock); 
   cudaMalloc(&d_SigmaZ[i][j],nByteBlock); 
   nByteAlloc+=2*nByteBlock;
   cudaMemset(d_SigmaX[i][j],0,nByteBlock);
   cudaMemset(d_SigmaZ[i][j],0,nByteBlock);
  }
 }

 const int nbuffVEpsDel=NUPDATE+4;
 float *d_v[nbuffVEpsDel],*d_eps[nbuffVEpsDel],*d_del[nbuffVEpsDel];
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaMalloc(&d_v[i],nByteBlock);
  cudaMalloc(&d_eps[i],nByteBlock);
  cudaMalloc(&d_del[i],nByteBlock);
  nByteAlloc+=3*nByteBlock;
  cudaMemset(d_v[i],0,nByteBlock);
  cudaMemset(d_eps[i],0,nByteBlock);
  cudaMemset(d_del[i],0,nByteBlock);
 }

 fprintf(stderr,"alloc %f MBs on GPU\n",nByteAlloc*1e-6);

 cudaStream_t computeStream,transfInStream,transfOutStream;
 cudaStreamCreate(&computeStream);
 cudaStreamCreate(&transfInStream);
 cudaStreamCreate(&transfOutStream);

 vector<thread> threads;
 
 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 for(int it=0;it<(nt-2)/NUPDATE;++it){
  for(int ib=0;ib<nb+NUPDATE+5;++ib){
   if(ib<nb){
    threads.push_back(thread(memcpyCpuToCpu3,h_v[ib%2],v+ib*nElemBlock,h_eps[ib%2],eps+ib*nElemBlock,h_del[ib%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[ib%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[ib%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[ib%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[ib%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(ib<recBlock+NUPDATE+1 && ib>recBlock) threads.push_back(thread(interpolateResidual,h_data[ib%2],data,nt-it*NUPDATE-ib-2+recBlock,nr,samplingTimeStep));
   
   if(ib>0 && ib<nb+1){
    memcpyCpuToGpu3(d_v[ib%nbuffVEpsDel],h_v[(ib+1)%2],d_eps[ib%nbuffVEpsDel],h_eps[(ib+1)%2],d_del[ib%nbuffVEpsDel],h_del[(ib+1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(ib-1)%nbuffSigma[0]],h_prevSigmaX[(ib+1)%2],d_SigmaX[1][ib%nbuffSigma[1]],h_curSigmaX[(ib+1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(ib-1)%nbuffSigma[0]],h_prevSigmaZ[(ib+1)%2],d_SigmaZ[1][ib%nbuffSigma[1]],h_curSigmaZ[(ib+1)%2],nByteBlock,&transfInStream);
   }
   
   if(ib<recBlock+NUPDATE+2 && ib>recBlock+1) cudaMemcpyAsync(d_data[ib%2],h_data[(ib+1)%2],nr*sizeof(float),cudaMemcpyHostToDevice,transfInStream);

   if(ib>2 && ib<nb+NUPDATE+2){
    if(ib<NUPDATE+3) cudaMemsetAsync(d_SigmaZ[ib-2][(ib-3)%nbuffSigma[ib-2]],0,nByteBlock,computeStream);
    if(ib>nb+1) cudaMemsetAsync(d_SigmaZ[ib-nb-1][(ib-1)%nbuffSigma[ib-nb-1]],0,nByteBlock,computeStream);
    adjoint(max(0,ib-nb-2),min(ib-2,NUPDATE),ib,d_SigmaX,d_SigmaZ,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,d_data[(ib+1)%2],nr,d_recIndex,recBlock,nx,ny,nz,npad,dx2,dy2,dz2,dt2,&computeStream);
   }
   
   if(ib>NUPDATE+3 && ib<nb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[ib%2],d_SigmaX[nd_Sigma-2][(ib-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[ib%2],d_SigmaX[nd_Sigma-1][(ib-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[ib%2],d_SigmaZ[nd_Sigma-2][(ib-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[ib%2],d_SigmaZ[nd_Sigma-1][(ib-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
   }
   
   if(ib>NUPDATE+4){
    memcpyCpuToCpu2(prevSigmaX+(ib-NUPDATE-5)*nElemBlock,h_SigmaX4[(ib+1)%2],curSigmaX+(ib-NUPDATE-5)*nElemBlock,h_SigmaX5[(ib+1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+(ib-NUPDATE-5)*nElemBlock,h_SigmaZ4[(ib+1)%2],curSigmaZ+(ib-NUPDATE-5)*nElemBlock,h_SigmaZ5[(ib+1)%2],nByteBlock);
   }
   
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  }
  
  for(int iz=0;iz<nz;iz++) memcpy(recWavefield+iz*nx+it*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 long long n=nx*ny*nz;
 long long ncells=n*nt;
 cout<<"ncells "<<ncells<<endl;
 cout<<"speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl;
 
 cudaError_t e=cudaGetLastError();
 if(e!=cudaSuccess) fprintf(stderr,"error %s\n",cudaGetErrorString(e));

 write("recWavefield",recWavefield,nx*nz*(nt-2)/NUPDATE);
 to_header("recWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("recWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("recWavefield","n3",(nt-2)/NUPDATE,"o3",0.,"d3",NUPDATE);

 delete []recWavefield;

 delete []prevSigmaX;delete []curSigmaX;
 delete []prevSigmaZ;delete []curSigmaZ;
 delete []recIndex;
 
 for(int i=0;i<2;++i){
  cudaFreeHost(h_v[i]);
  cudaFreeHost(h_eps[i]);
  cudaFreeHost(h_del[i]);
  cudaFreeHost(h_prevSigmaX[i]);
  cudaFreeHost(h_curSigmaX[i]);
  cudaFreeHost(h_SigmaX4[i]);
  cudaFreeHost(h_SigmaX5[i]);
  cudaFreeHost(h_prevSigmaZ[i]);
  cudaFreeHost(h_curSigmaZ[i]);
  cudaFreeHost(h_SigmaZ4[i]);
  cudaFreeHost(h_SigmaZ5[i]);
  cudaFreeHost(h_data[i]);
  cudaFree(d_data[i]);
 }
 
 for(int i=0;i<nd_Sigma;++i){
  for(int j=0;j<nbuffSigma[i];++j){
   cudaFree(d_SigmaX[i][j]); 
   cudaFree(d_SigmaZ[i][j]); 
  }
  delete []d_SigmaX[i];
  delete []d_SigmaZ[i];
 }
 
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaFree(d_v[i]);
  cudaFree(d_eps[i]);
  cudaFree(d_del[i]);
 }

 cudaFree(d_recIndex);
 
 cudaStreamDestroy(computeStream);
 cudaStreamDestroy(transfInStream);
 cudaStreamDestroy(transfOutStream);
 
 return;
}

__global__ void gradientKernelBottomBlock(float *gv,float *geps,float *gdel,float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *curSigmaX1a,float *curSigmaZ1a,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2){
 
 __shared__ float sSigmaX[BLOCK_DIM+2*HALF_STENCIL][BLOCK_DIM+2*HALF_STENCIL]; 
 
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int i=ix+iy*nx;
  
  int six=threadIdx.x+HALF_STENCIL;
  int siy=threadIdx.y+HALF_STENCIL;
  
  float zSigmaZ[2*HALF_STENCIL+1];
  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   zSigmaZ[iz+1]=curSigmaZ0[j];
   zSigmaZ[iz+1+HALF_STENCIL]=curSigmaZ1[j];
  }

  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   
   zSigmaZ[0]=zSigmaZ[1];
   zSigmaZ[1]=zSigmaZ[2];
   zSigmaZ[2]=zSigmaZ[3];
   zSigmaZ[3]=zSigmaZ[4];
   zSigmaZ[4]=zSigmaZ[5];
   zSigmaZ[5]=zSigmaZ[6];
   zSigmaZ[6]=zSigmaZ[7];
   zSigmaZ[7]=zSigmaZ[8];
   zSigmaZ[8]=0.;
   
   __syncthreads();
   
   sSigmaX[six][siy]=curSigmaX1[j];
   
   if(threadIdx.x<HALF_STENCIL){
    int k=min(blockDim.x,nx-2*HALF_STENCIL-blockIdx.x*blockDim.x);
    sSigmaX[threadIdx.x][siy]=curSigmaX1[j-HALF_STENCIL];
	sSigmaX[six+k][siy]=curSigmaX1[j+k];
   }
   
   if(threadIdx.y<HALF_STENCIL){
    int k=min(blockDim.y,ny-2*HALF_STENCIL-blockIdx.y*blockDim.y);
    sSigmaX[six][threadIdx.y]=curSigmaX1[j-HALF_STENCIL*nx];
	sSigmaX[six][siy+k]=curSigmaX1[j+k*nx];
   }

   __syncthreads();

   float c33=v[j]*v[j];
   float c11=c33*(1.+2.*eps[j]);
   float sqrt12del=sqrt(1.+2.*del[j]);
   float c13=c33*sqrt12del;
  
   float tx=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six-1][siy]+sSigmaX[six+1][siy])
                                 +C2*(sSigmaX[six-2][siy]+sSigmaX[six+2][siy])
                                 +C3*(sSigmaX[six-3][siy]+sSigmaX[six+3][siy])
                                 +C4*(sSigmaX[six-4][siy]+sSigmaX[six+4][siy]))/dx2;
   float ty=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six][siy-1]+sSigmaX[six][siy+1])
                                 +C2*(sSigmaX[six][siy-2]+sSigmaX[six][siy+2])
                                 +C3*(sSigmaX[six][siy-3]+sSigmaX[six][siy+3])
                                 +C4*(sSigmaX[six][siy-4]+sSigmaX[six][siy+4]))/dy2;
   float tz=(C0*zSigmaZ[HALF_STENCIL]+C1*(zSigmaZ[HALF_STENCIL-1]+zSigmaZ[HALF_STENCIL+1])
                                     +C2*(zSigmaZ[HALF_STENCIL-2]+zSigmaZ[HALF_STENCIL+2])
                                     +C3*(zSigmaZ[HALF_STENCIL-3]+zSigmaZ[HALF_STENCIL+3])
                                     +C4*(zSigmaZ[HALF_STENCIL-4]+zSigmaZ[HALF_STENCIL+4]))/dz2;
   
   //update source wavefields
   nextSigmaX[j]=dt2*(c11*(tx+ty)+c13*tz)+2.*sSigmaX[six][siy]-prevSigmaX[j];
   nextSigmaZ[j]=dt2*(c13*(tx+ty)+c33*tz)+2.*zSigmaZ[HALF_STENCIL]-prevSigmaZ[j];
   
   //imaging conditions to compute gradient
   float gc11=curSigmaX1a[j]*(tx+ty);
   float gc33=curSigmaZ1a[j]*tz;
   float gc13=curSigmaX1a[j]*tz+curSigmaZ1a[j]*(tx+ty);
   
   gv[j]+=2.*v[j]*(gc11*(1.+2.*eps[j])+gc13*sqrt12del+gc33);
   geps[j]+=gc11*2.*c33;
   gdel[j]+=gc13*c33/sqrt12del;
  }
 } 

 return;
}

__global__ void gradientKernelTopBlock(float *gv,float *geps,float *gdel,float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *curSigmaX1a,float *curSigmaZ1a,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2){
 
 __shared__ float sSigmaX[BLOCK_DIM+2*HALF_STENCIL][BLOCK_DIM+2*HALF_STENCIL]; 
 
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int i=ix+iy*nx;
  
  int six=threadIdx.x+HALF_STENCIL;
  int siy=threadIdx.y+HALF_STENCIL;
  
  float zSigmaZ[2*HALF_STENCIL+1];
  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   zSigmaZ[iz+1]=0.;
   zSigmaZ[iz+1+HALF_STENCIL]=curSigmaZ1[j];
  }

  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   
   zSigmaZ[0]=zSigmaZ[1];
   zSigmaZ[1]=zSigmaZ[2];
   zSigmaZ[2]=zSigmaZ[3];
   zSigmaZ[3]=zSigmaZ[4];
   zSigmaZ[4]=zSigmaZ[5];
   zSigmaZ[5]=zSigmaZ[6];
   zSigmaZ[6]=zSigmaZ[7];
   zSigmaZ[7]=zSigmaZ[8];
   zSigmaZ[8]=curSigmaZ2[j];
   
   __syncthreads();
   
   sSigmaX[six][siy]=curSigmaX1[j];
   
   if(threadIdx.x<HALF_STENCIL){
    int k=min(blockDim.x,nx-2*HALF_STENCIL-blockIdx.x*blockDim.x);
    sSigmaX[threadIdx.x][siy]=curSigmaX1[j-HALF_STENCIL];
	sSigmaX[six+k][siy]=curSigmaX1[j+k];
   }
   
   if(threadIdx.y<HALF_STENCIL){
    int k=min(blockDim.y,ny-2*HALF_STENCIL-blockIdx.y*blockDim.y);
    sSigmaX[six][threadIdx.y]=curSigmaX1[j-HALF_STENCIL*nx];
	sSigmaX[six][siy+k]=curSigmaX1[j+k*nx];
   }

   __syncthreads();

   float c33=v[j]*v[j];
   float c11=c33*(1.+2.*eps[j]);
   float sqrt12del=sqrt(1.+2.*del[j]);
   float c13=c33*sqrt12del;
  
   float tx=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six-1][siy]+sSigmaX[six+1][siy])
                                 +C2*(sSigmaX[six-2][siy]+sSigmaX[six+2][siy])
                                 +C3*(sSigmaX[six-3][siy]+sSigmaX[six+3][siy])
                                 +C4*(sSigmaX[six-4][siy]+sSigmaX[six+4][siy]))/dx2;
   float ty=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six][siy-1]+sSigmaX[six][siy+1])
                                 +C2*(sSigmaX[six][siy-2]+sSigmaX[six][siy+2])
                                 +C3*(sSigmaX[six][siy-3]+sSigmaX[six][siy+3])
                                 +C4*(sSigmaX[six][siy-4]+sSigmaX[six][siy+4]))/dy2;
   float tz=(C0*zSigmaZ[HALF_STENCIL]+C1*(zSigmaZ[HALF_STENCIL-1]+zSigmaZ[HALF_STENCIL+1])
                                     +C2*(zSigmaZ[HALF_STENCIL-2]+zSigmaZ[HALF_STENCIL+2])
                                     +C3*(zSigmaZ[HALF_STENCIL-3]+zSigmaZ[HALF_STENCIL+3])
                                     +C4*(zSigmaZ[HALF_STENCIL-4]+zSigmaZ[HALF_STENCIL+4]))/dz2;
   
   //update source wavefields
   nextSigmaX[j]=dt2*(c11*(tx+ty)+c13*tz)+2.*sSigmaX[six][siy]-prevSigmaX[j];
   nextSigmaZ[j]=dt2*(c13*(tx+ty)+c33*tz)+2.*zSigmaZ[HALF_STENCIL]-prevSigmaZ[j];
   
   //imaging conditions to compute gradient
   float gc11=curSigmaX1a[j]*(tx+ty);
   float gc33=curSigmaZ1a[j]*tz;
   float gc13=curSigmaX1a[j]*tz+curSigmaZ1a[j]*(tx+ty);
   
   gv[j]+=2.*v[j]*(gc11*(1.+2.*eps[j])+gc13*sqrt12del+gc33);
   geps[j]+=gc11*2.*c33;
   gdel[j]+=gc13*c33/sqrt12del;
  }
 } 

 return;
}

__global__ void gradientKernel(float *gv,float *geps,float *gdel,float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *curSigmaX1a,float *curSigmaZ1a,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2){
 
 __shared__ float sSigmaX[BLOCK_DIM+2*HALF_STENCIL][BLOCK_DIM+2*HALF_STENCIL]; 
 
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int i=ix+iy*nx;
  
  int six=threadIdx.x+HALF_STENCIL;
  int siy=threadIdx.y+HALF_STENCIL;
  
  float zSigmaZ[2*HALF_STENCIL+1];
  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   zSigmaZ[iz+1]=curSigmaZ0[j];
   zSigmaZ[iz+1+HALF_STENCIL]=curSigmaZ1[j];
  }

  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   
   zSigmaZ[0]=zSigmaZ[1];
   zSigmaZ[1]=zSigmaZ[2];
   zSigmaZ[2]=zSigmaZ[3];
   zSigmaZ[3]=zSigmaZ[4];
   zSigmaZ[4]=zSigmaZ[5];
   zSigmaZ[5]=zSigmaZ[6];
   zSigmaZ[6]=zSigmaZ[7];
   zSigmaZ[7]=zSigmaZ[8];
   zSigmaZ[8]=curSigmaZ2[j];
   
   __syncthreads();
   
   sSigmaX[six][siy]=curSigmaX1[j];
   
   if(threadIdx.x<HALF_STENCIL){
    int k=min(blockDim.x,nx-2*HALF_STENCIL-blockIdx.x*blockDim.x);
    sSigmaX[threadIdx.x][siy]=curSigmaX1[j-HALF_STENCIL];
	sSigmaX[six+k][siy]=curSigmaX1[j+k];
   }
   
   if(threadIdx.y<HALF_STENCIL){
    int k=min(blockDim.y,ny-2*HALF_STENCIL-blockIdx.y*blockDim.y);
    sSigmaX[six][threadIdx.y]=curSigmaX1[j-HALF_STENCIL*nx];
	sSigmaX[six][siy+k]=curSigmaX1[j+k*nx];
   }

   __syncthreads();

   float c33=v[j]*v[j];
   float c11=c33*(1.+2.*eps[j]);
   float sqrt12del=sqrt(1.+2.*del[j]);
   float c13=c33*sqrt12del;
  
   float tx=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six-1][siy]+sSigmaX[six+1][siy])
                                 +C2*(sSigmaX[six-2][siy]+sSigmaX[six+2][siy])
                                 +C3*(sSigmaX[six-3][siy]+sSigmaX[six+3][siy])
                                 +C4*(sSigmaX[six-4][siy]+sSigmaX[six+4][siy]))/dx2;
   float ty=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six][siy-1]+sSigmaX[six][siy+1])
                                 +C2*(sSigmaX[six][siy-2]+sSigmaX[six][siy+2])
                                 +C3*(sSigmaX[six][siy-3]+sSigmaX[six][siy+3])
                                 +C4*(sSigmaX[six][siy-4]+sSigmaX[six][siy+4]))/dy2;
   float tz=(C0*zSigmaZ[HALF_STENCIL]+C1*(zSigmaZ[HALF_STENCIL-1]+zSigmaZ[HALF_STENCIL+1])
                                     +C2*(zSigmaZ[HALF_STENCIL-2]+zSigmaZ[HALF_STENCIL+2])
                                     +C3*(zSigmaZ[HALF_STENCIL-3]+zSigmaZ[HALF_STENCIL+3])
                                     +C4*(zSigmaZ[HALF_STENCIL-4]+zSigmaZ[HALF_STENCIL+4]))/dz2;
   
   //update source wavefields
   nextSigmaX[j]=dt2*(c11*(tx+ty)+c13*tz)+2.*sSigmaX[six][siy]-prevSigmaX[j];
   nextSigmaZ[j]=dt2*(c13*(tx+ty)+c33*tz)+2.*zSigmaZ[HALF_STENCIL]-prevSigmaZ[j];
   
   //imaging conditions to compute gradient
   float gc11=curSigmaX1a[j]*(tx+ty);
   float gc33=curSigmaZ1a[j]*tz;
   float gc13=curSigmaX1a[j]*tz+curSigmaZ1a[j]*(tx+ty);
   
   gv[j]+=2.*v[j]*(gc11*(1.+2.*eps[j])+gc13*sqrt12del+gc33);
   geps[j]+=gc11*2.*c33;
   gdel[j]+=gc13*c33/sqrt12del;
  }
 } 

 return;
}

void gradient(float **gv,float **geps,float **gdel,int ndg,int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,float ***d_SigmaXa,float ***d_SigmaZa,const int *nbuffSigma,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,float *data,int nr,const int *recIndex,int recBlock,int nx,int ny,int nz,int npad,int nt,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu){
 dim3 block(BLOCK_DIM,BLOCK_DIM);
 dim3 grid((nx-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM,(ny-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM);
 
 for(int i=b;i<e;++i){
  int nb=nz/HALF_STENCIL;
  int ib=(k-3-i)%nb;
  int it=(k-3-i)/nb*NUPDATE*NGPU+gpu*NUPDATE+2+i;
  it=nt-1-it;
  if(ib==0){
   gradientKernelTopBlock<<<grid,block,0,*stream>>>(gv[(k-i)%ndg],geps[(k-i)%ndg],gdel[(k-i)%ndg],d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);

   adjointKernelTopBlock<<<grid,block,0,*stream>>>(d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaXa[i][(k-3)%nbuffSigma[i]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZa[i][(k-3)%nbuffSigma[i]],d_v[(k-i-3)%nbuffVEpsDel],d_v[(k-i-2)%nbuffVEpsDel],d_v[(k-i-1)%nbuffVEpsDel],d_eps[(k-i-3)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-1)%nbuffVEpsDel],d_del[(k-i-3)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],d_del[(k-i-1)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else if(ib==nb-1){
   gradientKernelBottomBlock<<<grid,block,0,*stream>>>(gv[(k-i)%ndg],geps[(k-i)%ndg],gdel[(k-i)%ndg],d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);

   adjointKernelBottomBlock<<<grid,block,0,*stream>>>(d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaXa[i][(k-3)%nbuffSigma[i]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZa[i][(k-3)%nbuffSigma[i]],d_v[(k-i-3)%nbuffVEpsDel],d_v[(k-i-2)%nbuffVEpsDel],d_v[(k-i-1)%nbuffVEpsDel],d_eps[(k-i-3)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-1)%nbuffVEpsDel],d_del[(k-i-3)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],d_del[(k-i-1)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else{
   gradientKernel<<<grid,block,0,*stream>>>(gv[(k-i)%ndg],geps[(k-i)%ndg],gdel[(k-i)%ndg],d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);

   adjointKernel<<<grid,block,0,*stream>>>(d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaXa[i][(k-3)%nbuffSigma[i]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZa[i][(k-3)%nbuffSigma[i]],d_v[(k-i-3)%nbuffVEpsDel],d_v[(k-i-2)%nbuffVEpsDel],d_v[(k-i-1)%nbuffVEpsDel],d_eps[(k-i-3)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-1)%nbuffVEpsDel],d_del[(k-i-3)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],d_del[(k-i-1)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  
  if(ib==souBlock){
   float source=dt2*wavelet[it+1];
   injectSource<<<1,1,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],source,souIndexBlock);
  }
 
  if(ib==recBlock){
   injectResidual<<<(nr+BLOCK_DIM-1)/BLOCK_DIM,BLOCK_DIM,0,*stream>>>(data,d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],nr,recIndex,dt2);
  }
  
  abc<<<grid,block,0,*stream>>>(ib,nx,ny,nz,npad,d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]]);
 }
 
 return;
}

void sumGradientTime(float *gv,float *geps,float *gdel,float *h_gv,float *h_geps,float *h_gdel,size_t nElemBlock){
	#pragma omp parallel for num_threads(16)
	for(size_t i=0;i<nElemBlock;i++){
		gv[i]+=h_gv[i];
		geps[i]+=h_geps[i];
		gdel[i]+=h_gdel[i];
	}
	return;
}

double objFuncGradient1Shot1GPU(float *gv,float *geps,float *gdel,float *observedData,float soulocX,float soulocY,float soulocZ,float *recloc,int nr,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float dx,float dy,float dz,float dt,float samplingRate,int device){
 cudaSetDevice(device);
 
 //model data
 float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;
 int nxy=nx*ny;
 long long nxyz=nx*ny*nz;
 
 int *recIndex=new int[nr];
 
 #pragma omp parallel for num_threads(16)
 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  recIndex[ir]=ixy+(iz%HALF_STENCIL)*nxy;
 }
 
 int *d_recIndex;
 cudaMalloc(&d_recIndex,nr*sizeof(int));
 cudaMemcpy(d_recIndex,recIndex,nr*sizeof(int),cudaMemcpyHostToDevice);

 int recBlock=recloc[2]/dz/HALF_STENCIL; //assuming all receivers at same depth

 int souIndexX=soulocX/dx;
 int souIndexY=soulocY/dy;
 int souIndexZ=soulocZ/dz;
 int souIndex=souIndexX+souIndexY*nx+souIndexZ*nxy;
 int souIndexBlock=souIndexX+souIndexY*nx+(souIndexZ%HALF_STENCIL)*nxy;
 int souBlock=souIndexZ/HALF_STENCIL;

 int samplingTimeStep=std::round(samplingRate/dt);
 int nnt=(nt-1)/samplingTimeStep+1;
 float *data=new float[nr*nnt]; //modeled data

 size_t nElemBlock=HALF_STENCIL*nxy;
 size_t nByteBlock=nElemBlock*sizeof(float);
 int nb=nz/HALF_STENCIL;

 float *prevSigmaX=new float[nxyz]();
 float *curSigmaX=new float[nxyz]();
 float *prevSigmaZ=new float[nxyz]();
 float *curSigmaZ=new float[nxyz]();

 float *h_v[2],*h_eps[2],*h_del[2];
 float *h_prevSigmaX[2],*h_curSigmaX[2],*h_SigmaX4[2],*h_SigmaX5[2];
 float *h_prevSigmaZ[2],*h_curSigmaZ[2],*h_SigmaZ4[2],*h_SigmaZ5[2];
 float *h_data[2],*d_data[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_v[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_eps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_del[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_data[i],nr*sizeof(float),cudaHostAllocDefault);
  cudaMalloc(&d_data[i],nr*sizeof(float));
 }

 const int nd_Sigma=NUPDATE+2;
 float **d_SigmaX[nd_Sigma],**d_SigmaZ[nd_Sigma];
 int nbuffSigma[nd_Sigma];
 
 for(int i=0;i<nd_Sigma;++i) nbuffSigma[i]=3;
 nbuffSigma[1]=4;nbuffSigma[nd_Sigma-2]=4;
 
 size_t nByteAlloc=0;

 for(int i=0;i<nd_Sigma;++i){
  d_SigmaX[i]=new float*[nbuffSigma[i]]();
  d_SigmaZ[i]=new float*[nbuffSigma[i]]();
  for(int j=0;j<nbuffSigma[i];++j){
   cudaMalloc(&d_SigmaX[i][j],nByteBlock); 
   cudaMalloc(&d_SigmaZ[i][j],nByteBlock); 
   nByteAlloc+=2*nByteBlock;
   cudaMemset(d_SigmaX[i][j],0,nByteBlock);
   cudaMemset(d_SigmaZ[i][j],0,nByteBlock);
  }
 }

 const int nbuffVEpsDel=NUPDATE+4;
 float *d_v[nbuffVEpsDel],*d_eps[nbuffVEpsDel],*d_del[nbuffVEpsDel];
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaMalloc(&d_v[i],nByteBlock);
  cudaMalloc(&d_eps[i],nByteBlock);
  cudaMalloc(&d_del[i],nByteBlock);
  nByteAlloc+=3*nByteBlock;
  cudaMemset(d_v[i],0,nByteBlock);
  cudaMemset(d_eps[i],0,nByteBlock);
  cudaMemset(d_del[i],0,nByteBlock);
 }

 fprintf(stderr,"for forward alloc %f MBs on GPU\n",nByteAlloc*1e-6);

 cudaStream_t computeStream,transfInStream,transfOutStream;
 cudaStreamCreate(&computeStream);
 cudaStreamCreate(&transfInStream);
 cudaStreamCreate(&transfOutStream);

 vector<thread> threads;

 int nsave=(nt-2)/NUPDATE+1;
 float *forwardSouWavefield=new float[nx*nz*nsave];

 //injecting source at time 0 to wavefields at time 1
 curSigmaX[souIndex]=dt2*wavelet[0];
 curSigmaZ[souIndex]=dt2*wavelet[0];
 
 int ntb=(nt-2)/NUPDATE*nb;
 int j=0,krecord=0;

 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 for(int k=0;k<ntb+NUPDATE+5;k++){
   if(k<ntb){
	int ib=k%nb;
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(k>0 && k<ntb+1){
    memcpyCpuToGpu3(d_v[k%nbuffVEpsDel],h_v[(k-1)%2],d_eps[k%nbuffVEpsDel],h_eps[(k-1)%2],d_del[k%nbuffVEpsDel],h_del[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k-1)%2],d_SigmaX[1][k%nbuffSigma[1]],h_curSigmaX[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k-1)%2],d_SigmaZ[1][k%nbuffSigma[1]],h_curSigmaZ[(k-1)%2],nByteBlock,&transfInStream);
   }
   
   if(k>2 && k<ntb+NUPDATE+2){
    bool record=forwardAbc(max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nr,d_recIndex,recBlock,samplingTimeStep,d_data[k%2],nx,ny,nz,npad,dx2,dy2,dz2,dt2,&computeStream);
    if(record){ 
	 krecord=k;
	 j++;
	}
   }
   
   if(k>NUPDATE+3 && k<ntb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[k%2],d_SigmaX[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[k%2],d_SigmaX[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[k%2],d_SigmaZ[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[k%2],d_SigmaZ[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
   }
    
    if(k-1==krecord){
     cudaMemcpyAsync(h_data[k%2],d_data[(k+1)%2],nr*sizeof(float),cudaMemcpyDeviceToHost,transfOutStream);
    }
   
   if(k>NUPDATE+4){
	int ib=(k-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(k-1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(k-1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(k-1)%2],nByteBlock);
   }
	
    if(k-2==krecord){
     memcpy(data+j*nr,h_data[(k+1)%2],nr*sizeof(float));
    }
   
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(forwardSouWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,prevSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

 write("forwardSouWavefieldAbc",forwardSouWavefield,nx*nz*nsave);
 to_header("forwardSouWavefieldAbc","n1",nx,"o1",0.,"d1",dx);
 to_header("forwardSouWavefieldAbc","n2",nz,"o2",0.,"d2",dz);
 to_header("forwardSouWavefieldAbc","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 long long ncells=nxyz*nt;
 cout<<"ncells "<<ncells<<endl;
 cout<<"for forward speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl;
 
 cudaError_t e=cudaGetLastError();
 if(e!=cudaSuccess) fprintf(stderr,"error in forward %s\n",cudaGetErrorString(e));

 write("modeledData",data,nr*nnt);
 to_header("modeledData","n1",nx,"o1",0.,"d1",dx);
 to_header("modeledData","n2",ny,"o2",0.,"d2",dy);
 to_header("modeledData","n3",nnt,"o3",0.,"d3",samplingRate);

 //residual and objective
 double obj=0.;
 #pragma omp parallel for reduction(+:obj) num_threads(16)
 for(size_t i=0;i<nr*nnt;i++){
	 data[i]=data[i]-observedData[i];
	 obj+=data[i]*data[i];
 }

 //source wavefields with random boundary
 memset(prevSigmaX,0,nxyz*sizeof(float));
 memset(curSigmaX,0,nxyz*sizeof(float));
 memset(prevSigmaZ,0,nxyz*sizeof(float));
 memset(curSigmaZ,0,nxyz*sizeof(float));

 curSigmaX[souIndex]=dt2*wavelet[0];
 curSigmaZ[souIndex]=dt2*wavelet[0];
 
 for(int k=0;k<ntb+NUPDATE+5;k++){
   if(k<ntb){
	int ib=k%nb;
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(k>0 && k<ntb+1){
    memcpyCpuToGpu3(d_v[k%nbuffVEpsDel],h_v[(k-1)%2],d_eps[k%nbuffVEpsDel],h_eps[(k-1)%2],d_del[k%nbuffVEpsDel],h_del[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k-1)%2],d_SigmaX[1][k%nbuffSigma[1]],h_curSigmaX[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k-1)%2],d_SigmaZ[1][k%nbuffSigma[1]],h_curSigmaZ[(k-1)%2],nByteBlock,&transfInStream);
   }
   
   if(k>2 && k<ntb+NUPDATE+2){
    forwardRandom(max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nx,ny,nz,npad,dx2,dy2,dz2,dt2,&computeStream);
   }
   
   if(k>NUPDATE+3 && k<ntb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[k%2],d_SigmaX[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[k%2],d_SigmaX[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[k%2],d_SigmaZ[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[k%2],d_SigmaZ[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
   }
    
   if(k>NUPDATE+4){
	int ib=(k-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(k-1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(k-1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(k-1)%2],nByteBlock);
   }
	
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(forwardSouWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

 write("forwardSouWavefield",forwardSouWavefield,nx*nz*nsave);
 to_header("forwardSouWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("forwardSouWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("forwardSouWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []forwardSouWavefield;

 //flip time direction
 float *pt;
 pt=curSigmaX;curSigmaX=prevSigmaX;prevSigmaX=pt;
 pt=curSigmaZ;curSigmaZ=prevSigmaZ;prevSigmaZ=pt;

 //receiver wavefields and cross-correlation imaging
 float *backwardSouWavefield=new float[nx*nz*nsave];

 float *recWavefield=new float[nx*nz*nsave];

 float *prevSigmaXa=new float[nxyz]();
 float *curSigmaXa=new float[nxyz]();
 float *prevSigmaZa=new float[nxyz]();
 float *curSigmaZa=new float[nxyz]();
 
 #pragma omp parallel for num_threads(16)
 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  int i=ixy+iz*nxy;
  curSigmaXa[i]=2./3.*dt2*data[ir+(nnt-1)*nr];
  curSigmaZa[i]=1./3.*dt2*data[ir+(nnt-1)*nr];
 }

 float *h_prevSigmaXa[2],*h_curSigmaXa[2],*h_SigmaXa4[2],*h_SigmaXa5[2];
 float *h_prevSigmaZa[2],*h_curSigmaZa[2],*h_SigmaZa4[2],*h_SigmaZa5[2];
 float *h_gv[2],*h_geps[2],*h_gdel[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_prevSigmaXa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaXa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaXa4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaXa5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZa4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZa5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_gv[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_geps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_gdel[i],nByteBlock,cudaHostAllocDefault);
 }
 
 float **d_SigmaXa[nd_Sigma],**d_SigmaZa[nd_Sigma];
 
 nByteAlloc=0;

 for(int i=0;i<nd_Sigma;++i){
  d_SigmaXa[i]=new float*[nbuffSigma[i]]();
  d_SigmaZa[i]=new float*[nbuffSigma[i]]();
  for(int j=0;j<nbuffSigma[i];++j){
   cudaMalloc(&d_SigmaXa[i][j],nByteBlock); 
   cudaMalloc(&d_SigmaZa[i][j],nByteBlock); 
   nByteAlloc+=2*nByteBlock;
   cudaMemset(d_SigmaXa[i][j],0,nByteBlock);
   cudaMemset(d_SigmaZa[i][j],0,nByteBlock);
  }
 }

 const int ndg=NUPDATE+2;
 float *d_gv[ndg],*d_geps[ndg],*d_gdel[ndg];
 for(int i=0;i<ndg;i++){
  cudaMalloc(&d_gv[i],nByteBlock);
  cudaMalloc(&d_geps[i],nByteBlock);
  cudaMalloc(&d_gdel[i],nByteBlock);
  nByteAlloc+=3*nByteBlock;
 }

 fprintf(stderr,"for adjoint alloc %f MBs on GPU\n",nByteAlloc*1e-6);

 start=chrono::high_resolution_clock::now();
 
 for(int k=0;k<ntb+NUPDATE+5;k++){
   int ib=k%nb,it=k/nb;
   if(k<ntb){
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaXa[k%2],prevSigmaXa+ib*nElemBlock,h_curSigmaXa[k%2],curSigmaXa+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZa[k%2],prevSigmaZa+ib*nElemBlock,h_curSigmaZa[k%2],curSigmaZa+ib*nElemBlock,nByteBlock));
   }
   
   if(ib>recBlock && ib<recBlock+NUPDATE+1) threads.push_back(thread(interpolateResidual,h_data[k%2],data,nt-it*NUPDATE-ib-1+recBlock,nr,samplingTimeStep));
   
   if(k>0 && k<ntb+1){
    memcpyCpuToGpu3(d_v[k%nbuffVEpsDel],h_v[(k-1)%2],d_eps[k%nbuffVEpsDel],h_eps[(k-1)%2],d_del[k%nbuffVEpsDel],h_del[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k-1)%2],d_SigmaX[1][k%nbuffSigma[1]],h_curSigmaX[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k-1)%2],d_SigmaZ[1][k%nbuffSigma[1]],h_curSigmaZ[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaXa[0][(k-1)%nbuffSigma[0]],h_prevSigmaXa[(k-1)%2],d_SigmaXa[1][k%nbuffSigma[1]],h_curSigmaXa[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZa[0][(k-1)%nbuffSigma[0]],h_prevSigmaZa[(k-1)%2],d_SigmaZa[1][k%nbuffSigma[1]],h_curSigmaZa[(k-1)%2],nByteBlock,&transfInStream);
   }
   
   if(ib>recBlock+1 && ib<recBlock+NUPDATE+2) cudaMemcpyAsync(d_data[k%2],h_data[(k+1)%2],nr*sizeof(float),cudaMemcpyHostToDevice,transfInStream);

   if(k>2 && k<ntb+NUPDATE+2){
    gradient(d_gv,d_geps,d_gdel,ndg,max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,d_SigmaXa,d_SigmaZa,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,d_data[(k+1)%2],nr,d_recIndex,recBlock,nx,ny,nz,npad,nt,dx2,dy2,dz2,dt2,&computeStream);
   }
   
   if(k>NUPDATE+3 && k<ntb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[k%2],d_SigmaX[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[k%2],d_SigmaX[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[k%2],d_SigmaZ[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[k%2],d_SigmaZ[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaXa4[k%2],d_SigmaXa[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaXa5[k%2],d_SigmaXa[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZa4[k%2],d_SigmaZa[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZa5[k%2],d_SigmaZa[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu3(h_gv[k%2],d_gv[(k-NUPDATE-1)%ndg],h_geps[k%2],d_geps[(k-NUPDATE-1)%ndg],h_gdel[k%2],d_gdel[(k-NUPDATE-1)%ndg],nByteBlock,&transfOutStream);
	cudaMemsetAsync(d_gv[(k-NUPDATE-1)%ndg],0,nByteBlock,transfOutStream);
	cudaMemsetAsync(d_geps[(k-NUPDATE-1)%ndg],0,nByteBlock,transfOutStream);
	cudaMemsetAsync(d_gdel[(k-NUPDATE-1)%ndg],0,nByteBlock,transfOutStream);
   }
    
   if(k>NUPDATE+4){
	ib=(k-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(k-1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(k-1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaXa+ib*nElemBlock,h_SigmaXa4[(k-1)%2],curSigmaXa+ib*nElemBlock,h_SigmaXa5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZa+ib*nElemBlock,h_SigmaZa4[(k-1)%2],curSigmaZa+ib*nElemBlock,h_SigmaZa5[(k-1)%2],nByteBlock);
    sumGradientTime(gv+ib*nElemBlock,geps+ib*nElemBlock,gdel+ib*nElemBlock,h_gv[(k+1)%2],h_geps[(k+1)%2],h_gdel[(k+1)%2],nElemBlock);
   }
	
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(backwardSouWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,prevSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(recWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,prevSigmaXa+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

 end=chrono::high_resolution_clock::now();
 time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 cout<<"for adjoint speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl;
 
 e=cudaGetLastError();
 if(e!=cudaSuccess) fprintf(stderr,"error in adjoint %s\n",cudaGetErrorString(e));
 
 write("backwardSouWavefield",backwardSouWavefield,nx*nz*nsave);
 to_header("backwardSouWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("backwardSouWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("backwardSouWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []backwardSouWavefield;

 write("recWavefield",recWavefield,nx*nz*nsave);
 to_header("recWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("recWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("recWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []recWavefield;

 //delete arrays used in forward
 delete []prevSigmaX;delete []curSigmaX;
 delete []prevSigmaZ;delete []curSigmaZ;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_v[i]);
  cudaFreeHost(h_eps[i]);
  cudaFreeHost(h_del[i]);
  cudaFreeHost(h_prevSigmaX[i]);
  cudaFreeHost(h_curSigmaX[i]);
  cudaFreeHost(h_SigmaX4[i]);
  cudaFreeHost(h_SigmaX5[i]);
  cudaFreeHost(h_prevSigmaZ[i]);
  cudaFreeHost(h_curSigmaZ[i]);
  cudaFreeHost(h_SigmaZ4[i]);
  cudaFreeHost(h_SigmaZ5[i]);
  cudaFreeHost(h_data[i]);
  cudaFree(d_data[i]);
 }
 
 for(int i=0;i<nd_Sigma;++i){
  for(int j=0;j<nbuffSigma[i];++j){
   cudaFree(d_SigmaX[i][j]); 
   cudaFree(d_SigmaZ[i][j]); 
  }
  delete []d_SigmaX[i];
  delete []d_SigmaZ[i];
 }
 
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaFree(d_v[i]);
  cudaFree(d_eps[i]);
  cudaFree(d_del[i]);
 }
 
 delete []data;
 delete []recIndex;
 cudaFree(d_recIndex);
 
 cudaStreamDestroy(computeStream);
 cudaStreamDestroy(transfInStream);
 cudaStreamDestroy(transfOutStream);
 
 //delete arrays used in adjoint
 delete []prevSigmaXa;delete []curSigmaXa;
 delete []prevSigmaZa;delete []curSigmaZa;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_prevSigmaXa[i]);
  cudaFreeHost(h_curSigmaXa[i]);
  cudaFreeHost(h_SigmaXa4[i]);
  cudaFreeHost(h_SigmaXa5[i]);
  cudaFreeHost(h_prevSigmaZa[i]);
  cudaFreeHost(h_curSigmaZa[i]);
  cudaFreeHost(h_SigmaZa4[i]);
  cudaFreeHost(h_SigmaZa5[i]);
  cudaFreeHost(h_gv[i]);
  cudaFreeHost(h_geps[i]);
  cudaFreeHost(h_gdel[i]);
 }
 
 for(int i=0;i<nd_Sigma;++i){
  for(int j=0;j<nbuffSigma[i];++j){
   cudaFree(d_SigmaXa[i][j]); 
   cudaFree(d_SigmaZa[i][j]); 
  }
  delete []d_SigmaXa[i];
  delete []d_SigmaZa[i];
 }
 
 for(int i=0;i<ndg;i++){
  cudaFree(d_gv[i]);
  cudaFree(d_geps[i]);
  cudaFree(d_gdel[i]);
 }

 return 0.5*obj;
}

__global__ void RTMKernel(float *image,float *nextSigmaX,float *nextSigmaZ,float *nextSigmaXa,float *nextSigmaZa,int nx,int ny){
 
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int i=ix+iy*nx;
  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   image[j]+=(2./3.*nextSigmaX[j]+1./3.*nextSigmaZ[j])*(2./3.*nextSigmaXa[j]+1./3.*nextSigmaZa[j]);
  } 
 }

 return;
}

void RTM(float **image,int ndg,int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,float ***d_SigmaXa,float ***d_SigmaZa,const int *nbuffSigma,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,float *data,int nr,const int *recIndex,int recBlock,int nx,int ny,int nz,int npad,int nt,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu){
 dim3 block(BLOCK_DIM,BLOCK_DIM);
 dim3 grid((nx-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM,(ny-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM);
 
 for(int i=b;i<e;++i){
  int nb=nz/HALF_STENCIL;
  int ib=(k-3-i)%nb;
  int it=(k-3-i)/nb*NUPDATE*NGPU+gpu*NUPDATE+2+i;
  it=nt-1-it;
  if(ib==0){
   forwardKernelTopBlock<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);

   adjointKernelTopBlock<<<grid,block,0,*stream>>>(d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaXa[i][(k-3)%nbuffSigma[i]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZa[i][(k-3)%nbuffSigma[i]],d_v[(k-i-3)%nbuffVEpsDel],d_v[(k-i-2)%nbuffVEpsDel],d_v[(k-i-1)%nbuffVEpsDel],d_eps[(k-i-3)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-1)%nbuffVEpsDel],d_del[(k-i-3)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],d_del[(k-i-1)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else if(ib==nb-1){
   forwardKernelBottomBlock<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);

   adjointKernelBottomBlock<<<grid,block,0,*stream>>>(d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaXa[i][(k-3)%nbuffSigma[i]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZa[i][(k-3)%nbuffSigma[i]],d_v[(k-i-3)%nbuffVEpsDel],d_v[(k-i-2)%nbuffVEpsDel],d_v[(k-i-1)%nbuffVEpsDel],d_eps[(k-i-3)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-1)%nbuffVEpsDel],d_del[(k-i-3)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],d_del[(k-i-1)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else{
   forwardKernel<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);

   adjointKernel<<<grid,block,0,*stream>>>(d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaXa[i][(k-3)%nbuffSigma[i]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZa[i][(k-3)%nbuffSigma[i]],d_v[(k-i-3)%nbuffVEpsDel],d_v[(k-i-2)%nbuffVEpsDel],d_v[(k-i-1)%nbuffVEpsDel],d_eps[(k-i-3)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-1)%nbuffVEpsDel],d_del[(k-i-3)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],d_del[(k-i-1)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  
  if(ib==souBlock){
   float source=dt2*wavelet[it+1];
   injectSource<<<1,1,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],source,souIndexBlock);
  }
 
  if(ib==recBlock){
   injectResidual<<<(nr+BLOCK_DIM-1)/BLOCK_DIM,BLOCK_DIM,0,*stream>>>(data,d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],nr,recIndex,dt2);
  }
  
  abc<<<grid,block,0,*stream>>>(ib,nx,ny,nz,npad,d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]]);

  RTMKernel<<<grid,block,0,*stream>>>(image[(k-i)%ndg],d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],nx,ny);
 }
 
 return;
}

void sumImageTime(float *image,float *h_image,size_t nElemBlock){
	#pragma omp parallel for num_threads(16)
	for(size_t i=0;i<nElemBlock;i++){
		image[i]+=h_image[i];
	}
	return;
}

void RTM1Shot1GPU(float *image,float *observedData,float soulocX,float soulocY,float soulocZ,float *recloc,int nr,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float dx,float dy,float dz,float dt,float samplingRate,int device){
 cudaSetDevice(device);
 
 float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;
 int nxy=nx*ny;
 long long nxyz=nx*ny*nz;
 
 int *recIndex=new int[nr];
 
 #pragma omp parallel for num_threads(16)
 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  recIndex[ir]=ixy+(iz%HALF_STENCIL)*nxy;
 }
 
 int *d_recIndex;
 cudaMalloc(&d_recIndex,nr*sizeof(int));
 cudaMemcpy(d_recIndex,recIndex,nr*sizeof(int),cudaMemcpyHostToDevice);

 int recBlock=recloc[2]/dz/HALF_STENCIL; //assuming all receivers at same depth

 int souIndexX=soulocX/dx;
 int souIndexY=soulocY/dy;
 int souIndexZ=soulocZ/dz;
 int souIndex=souIndexX+souIndexY*nx+souIndexZ*nxy;
 int souIndexBlock=souIndexX+souIndexY*nx+(souIndexZ%HALF_STENCIL)*nxy;
 int souBlock=souIndexZ/HALF_STENCIL;

 int samplingTimeStep=std::round(samplingRate/dt);
 int nnt=(nt-1)/samplingTimeStep+1;

 size_t nElemBlock=HALF_STENCIL*nxy;
 size_t nByteBlock=nElemBlock*sizeof(float);
 int nb=nz/HALF_STENCIL;

 float *prevSigmaX=new float[nxyz]();
 float *curSigmaX=new float[nxyz]();
 float *prevSigmaZ=new float[nxyz]();
 float *curSigmaZ=new float[nxyz]();

 float *h_v[2],*h_eps[2],*h_del[2];
 float *h_prevSigmaX[2],*h_curSigmaX[2],*h_SigmaX4[2],*h_SigmaX5[2];
 float *h_prevSigmaZ[2],*h_curSigmaZ[2],*h_SigmaZ4[2],*h_SigmaZ5[2];
 float *h_data[2],*d_data[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_v[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_eps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_del[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_data[i],nr*sizeof(float),cudaHostAllocDefault);
  cudaMalloc(&d_data[i],nr*sizeof(float));
 }

 const int nd_Sigma=NUPDATE+2;
 float **d_SigmaX[nd_Sigma],**d_SigmaZ[nd_Sigma];
 int nbuffSigma[nd_Sigma];
 
 for(int i=0;i<nd_Sigma;++i) nbuffSigma[i]=3;
 nbuffSigma[1]=4;nbuffSigma[nd_Sigma-2]=4;
 
 size_t nByteAlloc=0;

 for(int i=0;i<nd_Sigma;++i){
  d_SigmaX[i]=new float*[nbuffSigma[i]]();
  d_SigmaZ[i]=new float*[nbuffSigma[i]]();
  for(int j=0;j<nbuffSigma[i];++j){
   cudaMalloc(&d_SigmaX[i][j],nByteBlock); 
   cudaMalloc(&d_SigmaZ[i][j],nByteBlock); 
   nByteAlloc+=2*nByteBlock;
   cudaMemset(d_SigmaX[i][j],0,nByteBlock);
   cudaMemset(d_SigmaZ[i][j],0,nByteBlock);
  }
 }

 const int nbuffVEpsDel=NUPDATE+4;
 float *d_v[nbuffVEpsDel],*d_eps[nbuffVEpsDel],*d_del[nbuffVEpsDel];
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaMalloc(&d_v[i],nByteBlock);
  cudaMalloc(&d_eps[i],nByteBlock);
  cudaMalloc(&d_del[i],nByteBlock);
  nByteAlloc+=3*nByteBlock;
  cudaMemset(d_v[i],0,nByteBlock);
  cudaMemset(d_eps[i],0,nByteBlock);
  cudaMemset(d_del[i],0,nByteBlock);
 }

 fprintf(stderr,"for forward alloc %f MBs on GPU\n",nByteAlloc*1e-6);

 cudaStream_t computeStream,transfInStream,transfOutStream;
 cudaStreamCreate(&computeStream);
 cudaStreamCreate(&transfInStream);
 cudaStreamCreate(&transfOutStream);

 vector<thread> threads;

 int nsave=(nt-2)/NUPDATE+1;
 float *forwardSouWavefield=new float[nx*nz*nsave];

 //injecting source at time 0 to wavefields at time 1
 curSigmaX[souIndex]=dt2*wavelet[0];
 curSigmaZ[souIndex]=dt2*wavelet[0];
 
 int ntb=(nt-2)/NUPDATE*nb;

 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 for(int k=0;k<ntb+NUPDATE+5;k++){
   if(k<ntb){
	int ib=k%nb;
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(k>0 && k<ntb+1){
    memcpyCpuToGpu3(d_v[k%nbuffVEpsDel],h_v[(k-1)%2],d_eps[k%nbuffVEpsDel],h_eps[(k-1)%2],d_del[k%nbuffVEpsDel],h_del[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k-1)%2],d_SigmaX[1][k%nbuffSigma[1]],h_curSigmaX[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k-1)%2],d_SigmaZ[1][k%nbuffSigma[1]],h_curSigmaZ[(k-1)%2],nByteBlock,&transfInStream);
   }
   
   if(k>2 && k<ntb+NUPDATE+2){
    forwardRandom(max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nx,ny,nz,npad,dx2,dy2,dz2,dt2,&computeStream);
   }
   
   if(k>NUPDATE+3 && k<ntb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[k%2],d_SigmaX[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[k%2],d_SigmaX[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[k%2],d_SigmaZ[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[k%2],d_SigmaZ[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
   }
    
   if(k>NUPDATE+4){
	int ib=(k-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(k-1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(k-1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(k-1)%2],nByteBlock);
   }
	
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(forwardSouWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 long long n=nx*ny*nz;
 long long ncells=n*(nt-2);
 cout<<"ncells "<<ncells<<endl;
 cout<<"speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl;
 
 cudaError_t e=cudaGetLastError();
 if(e!=cudaSuccess) fprintf(stderr,"error %s\n",cudaGetErrorString(e));

 write("forwardSouWavefield",forwardSouWavefield,nx*nz*nsave);
 to_header("forwardSouWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("forwardSouWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("forwardSouWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []forwardSouWavefield;

 //flip time direction
 float *pt;
 pt=curSigmaX;curSigmaX=prevSigmaX;prevSigmaX=pt;
 pt=curSigmaZ;curSigmaZ=prevSigmaZ;prevSigmaZ=pt;

 //receiver wavefields and cross-correlation imaging
 float *backwardSouWavefield=new float[nx*nz*nsave];

 float *recWavefield=new float[nx*nz*nsave];

 float *prevSigmaXa=new float[nxyz]();
 float *curSigmaXa=new float[nxyz]();
 float *prevSigmaZa=new float[nxyz]();
 float *curSigmaZa=new float[nxyz]();
 
 #pragma omp parallel for num_threads(16)
 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  int i=ixy+iz*nxy;
  curSigmaXa[i]=2./3.*dt2*observedData[ir+(nnt-1)*nr];
  curSigmaZa[i]=1./3.*dt2*observedData[ir+(nnt-1)*nr];
 }

 float *h_prevSigmaXa[2],*h_curSigmaXa[2],*h_SigmaXa4[2],*h_SigmaXa5[2];
 float *h_prevSigmaZa[2],*h_curSigmaZa[2],*h_SigmaZa4[2],*h_SigmaZa5[2];
 float *h_image[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_prevSigmaXa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaXa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaXa4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaXa5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZa4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZa5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_image[i],nByteBlock,cudaHostAllocDefault);
 }
 
 float **d_SigmaXa[nd_Sigma],**d_SigmaZa[nd_Sigma];
 
 nByteAlloc=0;

 for(int i=0;i<nd_Sigma;++i){
  d_SigmaXa[i]=new float*[nbuffSigma[i]]();
  d_SigmaZa[i]=new float*[nbuffSigma[i]]();
  for(int j=0;j<nbuffSigma[i];++j){
   cudaMalloc(&d_SigmaXa[i][j],nByteBlock); 
   cudaMalloc(&d_SigmaZa[i][j],nByteBlock); 
   nByteAlloc+=2*nByteBlock;
   cudaMemset(d_SigmaXa[i][j],0,nByteBlock);
   cudaMemset(d_SigmaZa[i][j],0,nByteBlock);
  }
 }

 const int ndg=NUPDATE+2;
 float *d_image[ndg];
 for(int i=0;i<ndg;i++){
  cudaMalloc(&d_image[i],nByteBlock);
  nByteAlloc+=nByteBlock;
 }

 fprintf(stderr,"for adjoint alloc %f MBs on GPU\n",nByteAlloc*1e-6);

 start=chrono::high_resolution_clock::now();
 
 for(int k=0;k<ntb+NUPDATE+5;k++){
   int ib=k%nb,it=k/nb;
   if(k<ntb){
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaXa[k%2],prevSigmaXa+ib*nElemBlock,h_curSigmaXa[k%2],curSigmaXa+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZa[k%2],prevSigmaZa+ib*nElemBlock,h_curSigmaZa[k%2],curSigmaZa+ib*nElemBlock,nByteBlock));
   }
   
   if(ib>recBlock && ib<recBlock+NUPDATE+1) threads.push_back(thread(interpolateResidual,h_data[k%2],observedData,nt-it*NUPDATE-ib-1+recBlock,nr,samplingTimeStep));
   
   if(k>0 && k<ntb+1){
    memcpyCpuToGpu3(d_v[k%nbuffVEpsDel],h_v[(k-1)%2],d_eps[k%nbuffVEpsDel],h_eps[(k-1)%2],d_del[k%nbuffVEpsDel],h_del[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k-1)%2],d_SigmaX[1][k%nbuffSigma[1]],h_curSigmaX[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k-1)%2],d_SigmaZ[1][k%nbuffSigma[1]],h_curSigmaZ[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaXa[0][(k-1)%nbuffSigma[0]],h_prevSigmaXa[(k-1)%2],d_SigmaXa[1][k%nbuffSigma[1]],h_curSigmaXa[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZa[0][(k-1)%nbuffSigma[0]],h_prevSigmaZa[(k-1)%2],d_SigmaZa[1][k%nbuffSigma[1]],h_curSigmaZa[(k-1)%2],nByteBlock,&transfInStream);
   }
   
   if(ib>recBlock+1 && ib<recBlock+NUPDATE+2) cudaMemcpyAsync(d_data[k%2],h_data[(k+1)%2],nr*sizeof(float),cudaMemcpyHostToDevice,transfInStream);

   if(k>2 && k<ntb+NUPDATE+2){
    RTM(d_image,ndg,max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,d_SigmaXa,d_SigmaZa,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,d_data[(k+1)%2],nr,d_recIndex,recBlock,nx,ny,nz,npad,nt,dx2,dy2,dz2,dt2,&computeStream);
   }
   
   if(k>NUPDATE+3 && k<ntb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[k%2],d_SigmaX[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[k%2],d_SigmaX[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[k%2],d_SigmaZ[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[k%2],d_SigmaZ[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaXa4[k%2],d_SigmaXa[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaXa5[k%2],d_SigmaXa[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZa4[k%2],d_SigmaZa[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZa5[k%2],d_SigmaZa[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    cudaMemcpyAsync(h_image[k%2],d_image[(k-NUPDATE-1)%ndg],nByteBlock,cudaMemcpyDeviceToHost,transfOutStream);
	cudaMemsetAsync(d_image[(k-NUPDATE-1)%ndg],0,nByteBlock,transfOutStream);
   }
    
   if(k>NUPDATE+4){
	ib=(k-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(k-1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(k-1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaXa+ib*nElemBlock,h_SigmaXa4[(k-1)%2],curSigmaXa+ib*nElemBlock,h_SigmaXa5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZa+ib*nElemBlock,h_SigmaZa4[(k-1)%2],curSigmaZa+ib*nElemBlock,h_SigmaZa5[(k-1)%2],nByteBlock);
    sumImageTime(image+ib*nElemBlock,h_image[(k+1)%2],nElemBlock);
   }
	
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(backwardSouWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,prevSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(recWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,prevSigmaXa+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

 end=chrono::high_resolution_clock::now();
 time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 cout<<"for adjoint speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl;
 
 e=cudaGetLastError();
 if(e!=cudaSuccess) fprintf(stderr,"error in adjoint %s\n",cudaGetErrorString(e));
 
 write("backwardSouWavefield",backwardSouWavefield,nx*nz*nsave);
 to_header("backwardSouWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("backwardSouWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("backwardSouWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []backwardSouWavefield;

 write("recWavefield",recWavefield,nx*nz*nsave);
 to_header("recWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("recWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("recWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []recWavefield;

 //delete arrays used in forward
 delete []prevSigmaX;delete []curSigmaX;
 delete []prevSigmaZ;delete []curSigmaZ;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_v[i]);
  cudaFreeHost(h_eps[i]);
  cudaFreeHost(h_del[i]);
  cudaFreeHost(h_prevSigmaX[i]);
  cudaFreeHost(h_curSigmaX[i]);
  cudaFreeHost(h_SigmaX4[i]);
  cudaFreeHost(h_SigmaX5[i]);
  cudaFreeHost(h_prevSigmaZ[i]);
  cudaFreeHost(h_curSigmaZ[i]);
  cudaFreeHost(h_SigmaZ4[i]);
  cudaFreeHost(h_SigmaZ5[i]);
  cudaFreeHost(h_data[i]);
  cudaFree(d_data[i]);
 }
 
 for(int i=0;i<nd_Sigma;++i){
  for(int j=0;j<nbuffSigma[i];++j){
   cudaFree(d_SigmaX[i][j]); 
   cudaFree(d_SigmaZ[i][j]); 
  }
  delete []d_SigmaX[i];
  delete []d_SigmaZ[i];
 }
 
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaFree(d_v[i]);
  cudaFree(d_eps[i]);
  cudaFree(d_del[i]);
 }
 
 delete []recIndex;
 cudaFree(d_recIndex);
 
 cudaStreamDestroy(computeStream);
 cudaStreamDestroy(transfInStream);
 cudaStreamDestroy(transfOutStream);
 
 //delete arrays used in adjoint
 delete []prevSigmaXa;delete []curSigmaXa;
 delete []prevSigmaZa;delete []curSigmaZa;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_prevSigmaXa[i]);
  cudaFreeHost(h_curSigmaXa[i]);
  cudaFreeHost(h_SigmaXa4[i]);
  cudaFreeHost(h_SigmaXa5[i]);
  cudaFreeHost(h_prevSigmaZa[i]);
  cudaFreeHost(h_curSigmaZa[i]);
  cudaFreeHost(h_SigmaZa4[i]);
  cudaFreeHost(h_SigmaZa5[i]);
  cudaFreeHost(h_image[i]);
 }
 
 for(int i=0;i<nd_Sigma;++i){
  for(int j=0;j<nbuffSigma[i];++j){
   cudaFree(d_SigmaXa[i][j]); 
   cudaFree(d_SigmaZa[i][j]); 
  }
  delete []d_SigmaXa[i];
  delete []d_SigmaZa[i];
 }
 
 for(int i=0;i<ndg;i++){
  cudaFree(d_image[i]);
 }

 return;
}

__global__ void extendedImagingKernel(float *image,float *nextSigmaX,float *nextSigmaZ,float *nextSigmaXa,float *nextSigmaZa,int nx,int ny){
 int nElemBlock=HALF_STENCIL*nx*ny;
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx-HALF_STENCIL && iy<ny-HALF_STENCIL){
  int i=ix+iy*nx;
  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   float temp=2./3.*nextSigmaX[j]+1./3.*nextSigmaZ[j];
   for(int lag=-NLAG;lag<=NLAG;lag++){
    image[j+(lag+NLAG)*nElemBlock]+=temp*(2./3.*nextSigmaXa[j+lag]+1./3.*nextSigmaZa[j+lag]);
   }
  } 
 }

 return;
}

void extendedImaging(float **image,int ndg,int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,float ***d_SigmaXa,float ***d_SigmaZa,const int *nbuffSigma,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,float *data,int nr,const int *recIndex,int recBlock,int nx,int ny,int nz,int npad,int nt,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu){
 dim3 block(BLOCK_DIM,BLOCK_DIM);
 dim3 grid((nx-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM,(ny-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM);
 
 for(int i=b;i<e;++i){
  int nb=nz/HALF_STENCIL;
  int ib=(k-3-i)%nb;
  int it=(k-3-i)/nb*NUPDATE*NGPU+gpu*NUPDATE+2+i;
  it=nt-1-it;
  if(ib==0){
   forwardKernelTopBlock<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);

   adjointKernelTopBlock<<<grid,block,0,*stream>>>(d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaXa[i][(k-3)%nbuffSigma[i]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZa[i][(k-3)%nbuffSigma[i]],d_v[(k-i-3)%nbuffVEpsDel],d_v[(k-i-2)%nbuffVEpsDel],d_v[(k-i-1)%nbuffVEpsDel],d_eps[(k-i-3)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-1)%nbuffVEpsDel],d_del[(k-i-3)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],d_del[(k-i-1)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else if(ib==nb-1){
   forwardKernelBottomBlock<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);

   adjointKernelBottomBlock<<<grid,block,0,*stream>>>(d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaXa[i][(k-3)%nbuffSigma[i]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZa[i][(k-3)%nbuffSigma[i]],d_v[(k-i-3)%nbuffVEpsDel],d_v[(k-i-2)%nbuffVEpsDel],d_v[(k-i-1)%nbuffVEpsDel],d_eps[(k-i-3)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-1)%nbuffVEpsDel],d_del[(k-i-3)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],d_del[(k-i-1)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  else{
   forwardKernel<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);

   adjointKernel<<<grid,block,0,*stream>>>(d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaXa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaXa[i][(k-3)%nbuffSigma[i]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZa[i][(k-3)%nbuffSigma[i]],d_v[(k-i-3)%nbuffVEpsDel],d_v[(k-i-2)%nbuffVEpsDel],d_v[(k-i-1)%nbuffVEpsDel],d_eps[(k-i-3)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-1)%nbuffVEpsDel],d_del[(k-i-3)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],d_del[(k-i-1)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
  }
  
  if(ib==souBlock){
   float source=dt2*wavelet[it+1];
   injectSource<<<1,1,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],source,souIndexBlock);
  }
 
  if(ib==recBlock){
   injectResidual<<<(nr+BLOCK_DIM-1)/BLOCK_DIM,BLOCK_DIM,0,*stream>>>(data,d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],nr,recIndex,dt2);
  }
  
  abc<<<grid,block,0,*stream>>>(ib,nx,ny,nz,npad,d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+1][(k-2)%nbuffSigma[i+1]]);

  extendedImagingKernel<<<grid,block,0,*stream>>>(image[(k-i)%ndg],d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaXa[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZa[i+2][(k-1)%nbuffSigma[i+2]],nx,ny);
 }
 
 return;
}

void ODCIG1Shot1GPU(float *image,float *observedData,float soulocX,float soulocY,float soulocZ,float *recloc,int nr,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float dx,float dy,float dz,float dt,float samplingRate,int device){
 cudaSetDevice(device);

 fprintf(stderr,"NUPDATE %d NLAG %d\n",NUPDATE,NLAG);

 float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;
 int nxy=nx*ny;
 long long nxyz=nx*ny*nz;
 
 int *recIndex=new int[nr];
 
 #pragma omp parallel for num_threads(16)
 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  recIndex[ir]=ixy+(iz%HALF_STENCIL)*nxy;
 }
 
 int *d_recIndex;
 cudaMalloc(&d_recIndex,nr*sizeof(int));
 cudaMemcpy(d_recIndex,recIndex,nr*sizeof(int),cudaMemcpyHostToDevice);

 int recBlock=recloc[2]/dz/HALF_STENCIL; //assuming all receivers at same depth

 int souIndexX=soulocX/dx;
 int souIndexY=soulocY/dy;
 int souIndexZ=soulocZ/dz;
 int souIndex=souIndexX+souIndexY*nx+souIndexZ*nxy;
 int souIndexBlock=souIndexX+souIndexY*nx+(souIndexZ%HALF_STENCIL)*nxy;
 int souBlock=souIndexZ/HALF_STENCIL;

 int samplingTimeStep=std::round(samplingRate/dt);
 int nnt=(nt-1)/samplingTimeStep+1;

 size_t nElemBlock=HALF_STENCIL*nxy;
 size_t nByteBlock=nElemBlock*sizeof(float);
 int nb=nz/HALF_STENCIL;

 float *prevSigmaX=new float[nxyz]();
 float *curSigmaX=new float[nxyz]();
 float *prevSigmaZ=new float[nxyz]();
 float *curSigmaZ=new float[nxyz]();

 float *h_v[2],*h_eps[2],*h_del[2];
 float *h_prevSigmaX[2],*h_curSigmaX[2],*h_SigmaX4[2],*h_SigmaX5[2];
 float *h_prevSigmaZ[2],*h_curSigmaZ[2],*h_SigmaZ4[2],*h_SigmaZ5[2];
 float *h_data[2],*d_data[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_v[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_eps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_del[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_data[i],nr*sizeof(float),cudaHostAllocDefault);
  cudaMalloc(&d_data[i],nr*sizeof(float));
 }

 const int nd_Sigma=NUPDATE+2;
 float **d_SigmaX[nd_Sigma],**d_SigmaZ[nd_Sigma];
 int nbuffSigma[nd_Sigma];
 
 for(int i=0;i<nd_Sigma;++i) nbuffSigma[i]=3;
 nbuffSigma[1]=4;nbuffSigma[nd_Sigma-2]=4;
 
 size_t nByteAlloc=0;

 for(int i=0;i<nd_Sigma;++i){
  d_SigmaX[i]=new float*[nbuffSigma[i]]();
  d_SigmaZ[i]=new float*[nbuffSigma[i]]();
  for(int j=0;j<nbuffSigma[i];++j){
   cudaMalloc(&d_SigmaX[i][j],nByteBlock); 
   cudaMalloc(&d_SigmaZ[i][j],nByteBlock); 
   nByteAlloc+=2*nByteBlock;
   cudaMemset(d_SigmaX[i][j],0,nByteBlock);
   cudaMemset(d_SigmaZ[i][j],0,nByteBlock);
  }
 }

 const int nbuffVEpsDel=NUPDATE+4;
 float *d_v[nbuffVEpsDel],*d_eps[nbuffVEpsDel],*d_del[nbuffVEpsDel];
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaMalloc(&d_v[i],nByteBlock);
  cudaMalloc(&d_eps[i],nByteBlock);
  cudaMalloc(&d_del[i],nByteBlock);
  nByteAlloc+=3*nByteBlock;
  cudaMemset(d_v[i],0,nByteBlock);
  cudaMemset(d_eps[i],0,nByteBlock);
  cudaMemset(d_del[i],0,nByteBlock);
 }

 fprintf(stderr,"for forward alloc %f MBs on GPU\n",nByteAlloc*1e-6);

 cudaStream_t computeStream,transfInStream,transfOutStream;
 cudaStreamCreate(&computeStream);
 cudaStreamCreate(&transfInStream);
 cudaStreamCreate(&transfOutStream);

 vector<thread> threads;

 int nsave=(nt-2)/NUPDATE+1;
 float *forwardSouWavefield=new float[nx*nz*nsave];

 //injecting source at time 0 to wavefields at time 1
 curSigmaX[souIndex]=dt2*wavelet[0];
 curSigmaZ[souIndex]=dt2*wavelet[0];
 
 int ntb=(nt-2)/NUPDATE*nb;

 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 for(int k=0;k<ntb+NUPDATE+5;k++){
   if(k<ntb){
	int ib=k%nb;
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(k>0 && k<ntb+1){
    memcpyCpuToGpu3(d_v[k%nbuffVEpsDel],h_v[(k-1)%2],d_eps[k%nbuffVEpsDel],h_eps[(k-1)%2],d_del[k%nbuffVEpsDel],h_del[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k-1)%2],d_SigmaX[1][k%nbuffSigma[1]],h_curSigmaX[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k-1)%2],d_SigmaZ[1][k%nbuffSigma[1]],h_curSigmaZ[(k-1)%2],nByteBlock,&transfInStream);
   }
   
   if(k>2 && k<ntb+NUPDATE+2){
    forwardRandom(max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nx,ny,nz,npad,dx2,dy2,dz2,dt2,&computeStream);
   }
   
   if(k>NUPDATE+3 && k<ntb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[k%2],d_SigmaX[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[k%2],d_SigmaX[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[k%2],d_SigmaZ[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[k%2],d_SigmaZ[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
   }
    
   if(k>NUPDATE+4){
	int ib=(k-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(k-1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(k-1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(k-1)%2],nByteBlock);
   }
	
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(forwardSouWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 long long n=nx*ny*nz;
 long long ncells=n*(nt-2);
 cout<<"ncells "<<ncells<<endl;
 cout<<"speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl;
 
 cudaError_t e=cudaGetLastError();
 if(e!=cudaSuccess) fprintf(stderr,"error %s\n",cudaGetErrorString(e));

 write("forwardSouWavefield",forwardSouWavefield,nx*nz*nsave);
 to_header("forwardSouWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("forwardSouWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("forwardSouWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []forwardSouWavefield;

 //flip time direction
 float *pt;
 pt=curSigmaX;curSigmaX=prevSigmaX;prevSigmaX=pt;
 pt=curSigmaZ;curSigmaZ=prevSigmaZ;prevSigmaZ=pt;

 //receiver wavefields and cross-correlation imaging
 float *backwardSouWavefield=new float[nx*nz*nsave];

 float *recWavefield=new float[nx*nz*nsave];

 float *prevSigmaXa=new float[nxyz]();
 float *curSigmaXa=new float[nxyz]();
 float *prevSigmaZa=new float[nxyz]();
 float *curSigmaZa=new float[nxyz]();
 
 #pragma omp parallel for num_threads(16)
 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  int i=ixy+iz*nxy;
  curSigmaXa[i]=2./3.*dt2*observedData[ir+(nnt-1)*nr];
  curSigmaZa[i]=1./3.*dt2*observedData[ir+(nnt-1)*nr];
 }

 float *h_prevSigmaXa[2],*h_curSigmaXa[2],*h_SigmaXa4[2],*h_SigmaXa5[2];
 float *h_prevSigmaZa[2],*h_curSigmaZa[2],*h_SigmaZa4[2],*h_SigmaZa5[2];
 float *h_image[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_prevSigmaXa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaXa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaXa4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaXa5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZa4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZa5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_image[i],nByteBlock*(2*NLAG+1),cudaHostAllocDefault);
 }
 
 float **d_SigmaXa[nd_Sigma],**d_SigmaZa[nd_Sigma];
 
 nByteAlloc=0;

 for(int i=0;i<nd_Sigma;++i){
  d_SigmaXa[i]=new float*[nbuffSigma[i]]();
  d_SigmaZa[i]=new float*[nbuffSigma[i]]();
  for(int j=0;j<nbuffSigma[i];++j){
   cudaMalloc(&d_SigmaXa[i][j],nByteBlock); 
   cudaMalloc(&d_SigmaZa[i][j],nByteBlock); 
   nByteAlloc+=2*nByteBlock;
   cudaMemset(d_SigmaXa[i][j],0,nByteBlock);
   cudaMemset(d_SigmaZa[i][j],0,nByteBlock);
  }
 }

 const int ndg=NUPDATE+2;
 float *d_image[ndg];
 for(int i=0;i<ndg;i++){
  cudaMalloc(&d_image[i],nByteBlock*(2*NLAG+1));
  nByteAlloc+=(2*NLAG+1)*nByteBlock;
 }

 fprintf(stderr,"for adjoint alloc %f MBs on GPU\n",nByteAlloc*1e-6);

 start=chrono::high_resolution_clock::now();
 
 for(int k=0;k<ntb+NUPDATE+5;k++){
   int ib=k%nb,it=k/nb;
   if(k<ntb){
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaXa[k%2],prevSigmaXa+ib*nElemBlock,h_curSigmaXa[k%2],curSigmaXa+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZa[k%2],prevSigmaZa+ib*nElemBlock,h_curSigmaZa[k%2],curSigmaZa+ib*nElemBlock,nByteBlock));
   }
   
   if(ib>recBlock && ib<recBlock+NUPDATE+1) threads.push_back(thread(interpolateResidual,h_data[k%2],observedData,nt-it*NUPDATE-ib-1+recBlock,nr,samplingTimeStep));
   
   if(k>0 && k<ntb+1){
    memcpyCpuToGpu3(d_v[k%nbuffVEpsDel],h_v[(k-1)%2],d_eps[k%nbuffVEpsDel],h_eps[(k-1)%2],d_del[k%nbuffVEpsDel],h_del[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k-1)%2],d_SigmaX[1][k%nbuffSigma[1]],h_curSigmaX[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k-1)%2],d_SigmaZ[1][k%nbuffSigma[1]],h_curSigmaZ[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaXa[0][(k-1)%nbuffSigma[0]],h_prevSigmaXa[(k-1)%2],d_SigmaXa[1][k%nbuffSigma[1]],h_curSigmaXa[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZa[0][(k-1)%nbuffSigma[0]],h_prevSigmaZa[(k-1)%2],d_SigmaZa[1][k%nbuffSigma[1]],h_curSigmaZa[(k-1)%2],nByteBlock,&transfInStream);
   }
   
   if(ib>recBlock+1 && ib<recBlock+NUPDATE+2) cudaMemcpyAsync(d_data[k%2],h_data[(k+1)%2],nr*sizeof(float),cudaMemcpyHostToDevice,transfInStream);

   if(k>2 && k<ntb+NUPDATE+2){
    extendedImaging(d_image,ndg,max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,d_SigmaXa,d_SigmaZa,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,d_data[(k+1)%2],nr,d_recIndex,recBlock,nx,ny,nz,npad,nt,dx2,dy2,dz2,dt2,&computeStream);
   }
   
   if(k>NUPDATE+3 && k<ntb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[k%2],d_SigmaX[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[k%2],d_SigmaX[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[k%2],d_SigmaZ[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[k%2],d_SigmaZ[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaXa4[k%2],d_SigmaXa[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaXa5[k%2],d_SigmaXa[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZa4[k%2],d_SigmaZa[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZa5[k%2],d_SigmaZa[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    cudaMemcpyAsync(h_image[k%2],d_image[(k-NUPDATE-1)%ndg],nByteBlock*(2*NLAG+1),cudaMemcpyDeviceToHost,transfOutStream);
	cudaMemsetAsync(d_image[(k-NUPDATE-1)%ndg],0,nByteBlock*(2*NLAG+1),transfOutStream);
   }
    
   if(k>NUPDATE+4){
	ib=(k-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(k-1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(k-1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaXa+ib*nElemBlock,h_SigmaXa4[(k-1)%2],curSigmaXa+ib*nElemBlock,h_SigmaXa5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZa+ib*nElemBlock,h_SigmaZa4[(k-1)%2],curSigmaZa+ib*nElemBlock,h_SigmaZa5[(k-1)%2],nByteBlock);
    for(int lag=0;lag<(2*NLAG+1);lag++) sumImageTime(image+ib*nElemBlock+lag*nx*ny*nz,h_image[(k+1)%2]+lag*nElemBlock,nElemBlock);
   }
	
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(backwardSouWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,prevSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(recWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,prevSigmaXa+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

 end=chrono::high_resolution_clock::now();
 time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 cout<<"for adjoint speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl;
 
 e=cudaGetLastError();
 if(e!=cudaSuccess) fprintf(stderr,"error in adjoint %s\n",cudaGetErrorString(e));
 
 write("backwardSouWavefield",backwardSouWavefield,nx*nz*nsave);
 to_header("backwardSouWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("backwardSouWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("backwardSouWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []backwardSouWavefield;

 write("recWavefield",recWavefield,nx*nz*nsave);
 to_header("recWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("recWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("recWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []recWavefield;

 //delete arrays used in forward
 delete []prevSigmaX;delete []curSigmaX;
 delete []prevSigmaZ;delete []curSigmaZ;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_v[i]);
  cudaFreeHost(h_eps[i]);
  cudaFreeHost(h_del[i]);
  cudaFreeHost(h_prevSigmaX[i]);
  cudaFreeHost(h_curSigmaX[i]);
  cudaFreeHost(h_SigmaX4[i]);
  cudaFreeHost(h_SigmaX5[i]);
  cudaFreeHost(h_prevSigmaZ[i]);
  cudaFreeHost(h_curSigmaZ[i]);
  cudaFreeHost(h_SigmaZ4[i]);
  cudaFreeHost(h_SigmaZ5[i]);
  cudaFreeHost(h_data[i]);
  cudaFree(d_data[i]);
 }
 
 for(int i=0;i<nd_Sigma;++i){
  for(int j=0;j<nbuffSigma[i];++j){
   cudaFree(d_SigmaX[i][j]); 
   cudaFree(d_SigmaZ[i][j]); 
  }
  delete []d_SigmaX[i];
  delete []d_SigmaZ[i];
 }
 
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaFree(d_v[i]);
  cudaFree(d_eps[i]);
  cudaFree(d_del[i]);
 }
 
 delete []recIndex;
 cudaFree(d_recIndex);
 
 cudaStreamDestroy(computeStream);
 cudaStreamDestroy(transfInStream);
 cudaStreamDestroy(transfOutStream);
 
 //delete arrays used in adjoint
 delete []prevSigmaXa;delete []curSigmaXa;
 delete []prevSigmaZa;delete []curSigmaZa;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_prevSigmaXa[i]);
  cudaFreeHost(h_curSigmaXa[i]);
  cudaFreeHost(h_SigmaXa4[i]);
  cudaFreeHost(h_SigmaXa5[i]);
  cudaFreeHost(h_prevSigmaZa[i]);
  cudaFreeHost(h_curSigmaZa[i]);
  cudaFreeHost(h_SigmaZa4[i]);
  cudaFreeHost(h_SigmaZa5[i]);
  cudaFreeHost(h_image[i]);
 }
 
 for(int i=0;i<nd_Sigma;++i){
  for(int j=0;j<nbuffSigma[i];++j){
   cudaFree(d_SigmaXa[i][j]); 
   cudaFree(d_SigmaZa[i][j]); 
  }
  delete []d_SigmaXa[i];
  delete []d_SigmaZa[i];
 }
 
 for(int i=0;i<ndg;i++){
  cudaFree(d_image[i]);
 }

 return;
}

double objFuncGradient1ShotNGpu(float *gv,float *geps,float *gdel,float *observedData,float soulocX,float soulocY,float soulocZ,float *recloc,int nr,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float dx,float dy,float dz,float dt,float samplingRate){
 
 fprintf(stderr,"NUPDATE %d NGPU %d\n",NUPDATE,NGPU);
 float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;
 int nxy=nx*ny;
 long long nxyz=nx*ny*nz;
 
 int *recIndex=new int[nr];
 
 #pragma omp parallel for num_threads(16)
 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  recIndex[ir]=ixy+(iz%HALF_STENCIL)*nxy;
 }
 
 int recBlock=recloc[2]/dz/HALF_STENCIL; //assuming all receivers at same depth

 int souIndexX=soulocX/dx;
 int souIndexY=soulocY/dy;
 int souIndexZ=soulocZ/dz;
 int souIndex=souIndexX+souIndexY*nx+souIndexZ*nxy;
 int souIndexBlock=souIndexX+souIndexY*nx+(souIndexZ%HALF_STENCIL)*nxy;
 int souBlock=souIndexZ/HALF_STENCIL;

 int samplingTimeStep=std::round(samplingRate/dt);
 int nnt=(nt-1)/samplingTimeStep+1;
 float *data=new float[nr*nnt]; //modeled data

 size_t nElemBlock=HALF_STENCIL*nxy;
 size_t nByteBlock=nElemBlock*sizeof(float);
 int nb=nz/HALF_STENCIL;

 float *prevSigmaX=new float[nxyz]();
 float *curSigmaX=new float[nxyz]();
 float *prevSigmaZ=new float[nxyz]();
 float *curSigmaZ=new float[nxyz]();

 float *h_v[2],*h_eps[2],*h_del[2];
 float *h_prevSigmaX[2],*h_curSigmaX[2],*h_SigmaX4[2],*h_SigmaX5[2];
 float *h_prevSigmaZ[2],*h_curSigmaZ[2],*h_SigmaZ4[2],*h_SigmaZ5[2];
 float *h_data[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_v[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_eps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_del[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_data[i],nr*sizeof(float),cudaHostAllocDefault);
 }

 const int nd_Sigma=NUPDATE+2;
 int nbuffSigma[nd_Sigma];
 
 int **d_recIndex=new int*[NGPU]();
 float ***d_data=new float**[NGPU]();
 
 for(int i=0;i<nd_Sigma;++i) nbuffSigma[i]=3;
 nbuffSigma[1]=4;nbuffSigma[nd_Sigma-2]=4;

 float ****d_SigmaX=new float ***[NGPU]();
 float ****d_SigmaZ=new float ***[NGPU]();
 
 const int nbuffVEpsDel=NUPDATE+4;
 float ***d_v=new float**[NGPU]();
 float ***d_eps=new float**[NGPU]();
 float ***d_del=new float**[NGPU]();
 
 cudaStream_t *transfInStream=new cudaStream_t[NGPU]();
 cudaStream_t *transfOutStream=new cudaStream_t[NGPU]();
 cudaStream_t *computeStream=new cudaStream_t[NGPU]();
 
 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(gpu);
  
  cudaMalloc(&d_recIndex[gpu],nr*sizeof(int));
  cudaMemcpy(d_recIndex[gpu],recIndex,nr*sizeof(int),cudaMemcpyHostToDevice);
  
  d_data[gpu]=new float*[2]();
  for(int i=0;i<2;i++) cudaMalloc(&d_data[gpu][i],nr*sizeof(float));
  
  d_SigmaX[gpu]=new float**[nd_Sigma]();
  d_SigmaZ[gpu]=new float**[nd_Sigma]();
  for(int i=0;i<nd_Sigma;++i){
   d_SigmaX[gpu][i]=new float*[nbuffSigma[i]]();
   d_SigmaZ[gpu][i]=new float*[nbuffSigma[i]]();
   for(int j=0;j<nbuffSigma[i];++j){
    cudaMalloc(&d_SigmaX[gpu][i][j],nByteBlock); 
    cudaMalloc(&d_SigmaZ[gpu][i][j],nByteBlock); 
    cudaMemset(d_SigmaX[gpu][i][j],0,nByteBlock);
    cudaMemset(d_SigmaZ[gpu][i][j],0,nByteBlock);
   }
  }

  d_v[gpu]=new float*[nbuffVEpsDel]();
  d_eps[gpu]=new float*[nbuffVEpsDel]();
  d_del[gpu]=new float*[nbuffVEpsDel]();
  for(int i=0;i<nbuffVEpsDel;++i){
   cudaMalloc(&d_v[gpu][i],nByteBlock);
   cudaMalloc(&d_eps[gpu][i],nByteBlock);
   cudaMalloc(&d_del[gpu][i],nByteBlock);
   cudaMemset(d_v[gpu][i],0,nByteBlock);
   cudaMemset(d_eps[gpu][i],0,nByteBlock);
   cudaMemset(d_del[gpu][i],0,nByteBlock);
  }

  cudaStreamCreate(&transfInStream[gpu]);
  cudaStreamCreate(&computeStream[gpu]);
  cudaStreamCreate(&transfOutStream[gpu]);
 }

 float *prevSigmaXa=new float[nxyz]();
 float *curSigmaXa=new float[nxyz]();
 float *prevSigmaZa=new float[nxyz]();
 float *curSigmaZa=new float[nxyz]();
 
 float *h_prevSigmaXa[2],*h_curSigmaXa[2],*h_SigmaXa4[2],*h_SigmaXa5[2];
 float *h_prevSigmaZa[2],*h_curSigmaZa[2],*h_SigmaZa4[2],*h_SigmaZa5[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_prevSigmaXa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaXa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaXa4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaXa5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZa4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZa5[i],nByteBlock,cudaHostAllocDefault);
 }
 
 float ****d_SigmaXa=new float ***[NGPU]();
 float ****d_SigmaZa=new float ***[NGPU]();
 
 const int ndg=NUPDATE+2;
 float ***d_gv=new float**[NGPU]();
 float ***d_geps=new float**[NGPU]();
 float ***d_gdel=new float**[NGPU]();

 float ***h_gv=new float**[NGPU]();
 float ***h_geps=new float**[NGPU]();
 float ***h_gdel=new float**[NGPU]();
 
 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(gpu);
  
  d_SigmaXa[gpu]=new float**[nd_Sigma]();
  d_SigmaZa[gpu]=new float**[nd_Sigma]();
  for(int i=0;i<nd_Sigma;++i){
   d_SigmaXa[gpu][i]=new float*[nbuffSigma[i]]();
   d_SigmaZa[gpu][i]=new float*[nbuffSigma[i]]();
   for(int j=0;j<nbuffSigma[i];++j){
    cudaMalloc(&d_SigmaXa[gpu][i][j],nByteBlock); 
    cudaMalloc(&d_SigmaZa[gpu][i][j],nByteBlock); 
    cudaMemset(d_SigmaXa[gpu][i][j],0,nByteBlock);
    cudaMemset(d_SigmaZa[gpu][i][j],0,nByteBlock);
   }
  }
 
  d_gv[gpu]=new float*[ndg]();
  d_geps[gpu]=new float*[ndg]();
  d_gdel[gpu]=new float*[ndg]();
  for(int i=0;i<ndg;i++){
   cudaMalloc(&d_gv[gpu][i],nByteBlock);
   cudaMalloc(&d_geps[gpu][i],nByteBlock);
   cudaMalloc(&d_gdel[gpu][i],nByteBlock);
  }

  h_gv[gpu]=new float*[2]();
  h_geps[gpu]=new float*[2]();
  h_gdel[gpu]=new float*[2]();
  for(int i=0;i<2;i++){
   cudaHostAlloc(&h_gv[gpu][i],nByteBlock,cudaHostAllocDefault);
   cudaHostAlloc(&h_geps[gpu][i],nByteBlock,cudaHostAllocDefault);
   cudaHostAlloc(&h_gdel[gpu][i],nByteBlock,cudaHostAllocDefault);
  }
 }
 
 int nsave=(nt-2)/NUPDATE/NGPU+1;
 float *forwardSouWavefield=new float[nx*nz*nsave];

 //injecting source at time 0 to wavefields at time 1
 curSigmaX[souIndex]=dt2*wavelet[0];
 curSigmaZ[souIndex]=dt2*wavelet[0];
 
 vector<thread> threads;
 
 int ntb=(nt-2)/NUPDATE/NGPU*nb;
 int j=0,krecord=0;

 cout<<"model data"<<endl;
 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 for(int k=0;k<ntb+NGPU*(NUPDATE+3)+2;k++){
   if(k<ntb){
    int ib=k%nb;
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(k>0 && k<ntb+1){
    cudaSetDevice(0);
    memcpyCpuToGpu3(d_v[0][k%nbuffVEpsDel],h_v[(k+1)%2],d_eps[0][k%nbuffVEpsDel],h_eps[(k+1)%2],d_del[0][k%nbuffVEpsDel],h_del[(k+1)%2],nByteBlock,transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k+1)%2],d_SigmaX[0][1][k%nbuffSigma[1]],h_curSigmaX[(k+1)%2],nByteBlock,transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k+1)%2],d_SigmaZ[0][1][k%nbuffSigma[1]],h_curSigmaZ[(k+1)%2],nByteBlock,transfInStream);
   }
  
   for(int gpu=0;gpu<NGPU;gpu++){
    int kgpu=k-gpu*(NUPDATE+3);

    cudaSetDevice(gpu);

    if(kgpu>2 && kgpu<ntb+NUPDATE+2){
     bool record=forwardAbc(max(0,kgpu-ntb-2),min(kgpu-2,NUPDATE),kgpu,d_SigmaX[gpu],d_SigmaZ[gpu],nbuffSigma,d_v[gpu],d_eps[gpu],d_del[gpu],nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nr,d_recIndex[gpu],recBlock,samplingTimeStep,d_data[gpu][kgpu%2],nx,ny,nz,npad,dx2,dy2,dz2,dt2,computeStream+gpu,gpu);
     if(record){
	  krecord=kgpu;
	  j++;
	 }
	}
    
    if(kgpu>NUPDATE+3 && kgpu<ntb+NUPDATE+4){
     if(NGPU>1 && gpu<NGPU-1){
      memcpyGpuToGpu3(d_v[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_v[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],nByteBlock,transfOutStream+gpu);
      memcpyGpuToGpu2(d_SigmaX[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaX[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToGpu2(d_SigmaZ[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaZ[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
     }
     else{
      memcpyGpuToCpu2(h_SigmaX4[kgpu%2],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[kgpu%2],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToCpu2(h_SigmaZ4[kgpu%2],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[kgpu%2],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
     }
    }
    
	if(kgpu-1==krecord){
     cudaMemcpyAsync(h_data[kgpu%2],d_data[gpu][(kgpu+1)%2],nr*sizeof(float),cudaMemcpyDeviceToHost,transfOutStream[gpu]);
    }
 
    if(kgpu-2==krecord){
     memcpy(data+j*nr,h_data[(kgpu+1)%2],nr*sizeof(float));
    }
   }
   
   if(k>NGPU*(NUPDATE+3)+1){
    int kgpu=k-(NGPU-1)*(NUPDATE+3);
	int ib=(kgpu-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(kgpu+1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(kgpu+1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(kgpu+1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(kgpu+1)%2],nByteBlock);
   }
  
   for(int gpu=0;gpu<NGPU;gpu++){
    cudaSetDevice(gpu);
    cudaDeviceSynchronize();
   }
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
   
   int ksave=k-NGPU*(NUPDATE+3)-1;
   if(ksave>=0 && ksave%nb==0){
	int itsave=ksave/nb;
    for(int iz=0;iz<nz;iz++) memcpy(forwardSouWavefield+iz*nx+itsave*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
   }
 }

 write("forwardSouWavefieldAbc",forwardSouWavefield,nx*nz*nsave);
 to_header("forwardSouWavefieldAbc","n1",nx,"o1",0.,"d1",dx);
 to_header("forwardSouWavefieldAbc","n2",nz,"o2",0.,"d2",dz);
 to_header("forwardSouWavefieldAbc","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(gpu);
  cudaError_t e=cudaGetLastError();
  if(e!=cudaSuccess) fprintf(stderr,"error in forward %s gpu %d\n",cudaGetErrorString(e),gpu);
 }

 write("modeledData",data,nr*nnt);
 to_header("modeledData","n1",nx,"o1",0.,"d1",dx);
 to_header("modeledData","n2",ny,"o2",0.,"d2",dy);
 to_header("modeledData","n3",nnt,"o3",0.,"d3",samplingRate);

 cout<<"residual and objective"<<endl;
 double obj=0.;
 #pragma omp parallel for reduction(+:obj) num_threads(16)
 for(size_t i=0;i<nr*nnt;i++){
	 data[i]=data[i]-observedData[i];
	 obj+=data[i]*data[i];
 }

 cout<<"source wavefields with random boundary"<<endl;
 memset(prevSigmaX,0,nxyz*sizeof(float));
 memset(curSigmaX,0,nxyz*sizeof(float));
 memset(prevSigmaZ,0,nxyz*sizeof(float));
 memset(curSigmaZ,0,nxyz*sizeof(float));

 curSigmaX[souIndex]=dt2*wavelet[0];
 curSigmaZ[souIndex]=dt2*wavelet[0];
 
 for(int k=0;k<ntb+NGPU*(NUPDATE+3)+2;k++){
   if(k<ntb){
    int ib=k%nb;
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(k>0 && k<ntb+1){
    cudaSetDevice(0);
    memcpyCpuToGpu3(d_v[0][k%nbuffVEpsDel],h_v[(k+1)%2],d_eps[0][k%nbuffVEpsDel],h_eps[(k+1)%2],d_del[0][k%nbuffVEpsDel],h_del[(k+1)%2],nByteBlock,transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k+1)%2],d_SigmaX[0][1][k%nbuffSigma[1]],h_curSigmaX[(k+1)%2],nByteBlock,transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k+1)%2],d_SigmaZ[0][1][k%nbuffSigma[1]],h_curSigmaZ[(k+1)%2],nByteBlock,transfInStream);
   }
  
   for(int gpu=0;gpu<NGPU;gpu++){
    int kgpu=k-gpu*(NUPDATE+3);

    cudaSetDevice(gpu);

    if(kgpu>2 && kgpu<ntb+NUPDATE+2){
     forwardRandom(max(0,kgpu-ntb-2),min(kgpu-2,NUPDATE),kgpu,d_SigmaX[gpu],d_SigmaZ[gpu],nbuffSigma,d_v[gpu],d_eps[gpu],d_del[gpu],nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nx,ny,nz,npad,dx2,dy2,dz2,dt2,computeStream+gpu,gpu);
	}
    
    if(kgpu>NUPDATE+3 && kgpu<ntb+NUPDATE+4){
     if(NGPU>1 && gpu<NGPU-1){
      memcpyGpuToGpu3(d_v[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_v[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],nByteBlock,transfOutStream+gpu);
      memcpyGpuToGpu2(d_SigmaX[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaX[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToGpu2(d_SigmaZ[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaZ[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
     }
     else{
      memcpyGpuToCpu2(h_SigmaX4[kgpu%2],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[kgpu%2],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToCpu2(h_SigmaZ4[kgpu%2],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[kgpu%2],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
     }
    }
   }
   
   if(k>NGPU*(NUPDATE+3)+1){
    int kgpu=k-(NGPU-1)*(NUPDATE+3);
	int ib=(kgpu-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(kgpu+1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(kgpu+1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(kgpu+1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(kgpu+1)%2],nByteBlock);
   }
  
   for(int gpu=0;gpu<NGPU;gpu++){
    cudaSetDevice(gpu);
    cudaDeviceSynchronize();
   }
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
   
   int ksave=k-NGPU*(NUPDATE+3)-1;
   if(ksave>=0 && ksave%nb==0){
	int itsave=ksave/nb;
    for(int iz=0;iz<nz;iz++) memcpy(forwardSouWavefield+iz*nx+itsave*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
   }
 }

 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(gpu);
  cudaError_t e=cudaGetLastError();
  if(e!=cudaSuccess) fprintf(stderr,"error in forward random %s gpu %d\n",cudaGetErrorString(e),gpu);
 }

 write("forwardSouWavefield",forwardSouWavefield,nx*nz*nsave);
 to_header("forwardSouWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("forwardSouWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("forwardSouWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []forwardSouWavefield;

 cout<<"flip time direction"<<endl;
 float *pt;
 pt=curSigmaX;curSigmaX=prevSigmaX;prevSigmaX=pt;
 pt=curSigmaZ;curSigmaZ=prevSigmaZ;prevSigmaZ=pt;

 cout<<"receiver wavefields and cross-correlation imaging"<<endl;
 float *backwardSouWavefield=new float[nx*nz*nsave];
 float *recWavefield=new float[nx*nz*nsave];

 #pragma omp parallel for num_threads(16)
 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  int i=ixy+iz*nxy;
  curSigmaXa[i]=2./3.*dt2*data[ir+(nnt-1)*nr];
  curSigmaZa[i]=1./3.*dt2*data[ir+(nnt-1)*nr];
 }

 for(int k=0;k<ntb+NGPU*(NUPDATE+3)+2;k++){
   if(k<ntb){
    int ib=k%nb;
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaXa[k%2],prevSigmaXa+ib*nElemBlock,h_curSigmaXa[k%2],curSigmaXa+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZa[k%2],prevSigmaZa+ib*nElemBlock,h_curSigmaZa[k%2],curSigmaZa+ib*nElemBlock,nByteBlock));
   }
   
   if(k>0 && k<ntb+1){
    cudaSetDevice(0);
    memcpyCpuToGpu3(d_v[0][k%nbuffVEpsDel],h_v[(k+1)%2],d_eps[0][k%nbuffVEpsDel],h_eps[(k+1)%2],d_del[0][k%nbuffVEpsDel],h_del[(k+1)%2],nByteBlock,transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k+1)%2],d_SigmaX[0][1][k%nbuffSigma[1]],h_curSigmaX[(k+1)%2],nByteBlock,transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k+1)%2],d_SigmaZ[0][1][k%nbuffSigma[1]],h_curSigmaZ[(k+1)%2],nByteBlock,transfInStream);
    memcpyCpuToGpu2(d_SigmaXa[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaXa[(k+1)%2],d_SigmaXa[0][1][k%nbuffSigma[1]],h_curSigmaXa[(k+1)%2],nByteBlock,transfInStream);
    memcpyCpuToGpu2(d_SigmaZa[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaZa[(k+1)%2],d_SigmaZa[0][1][k%nbuffSigma[1]],h_curSigmaZa[(k+1)%2],nByteBlock,transfInStream);
   }
   
   for(int gpu=0;gpu<NGPU;gpu++){
    int kgpu=k-gpu*(NUPDATE+3);
	int ibgpu=kgpu%nb,itgpu=kgpu/nb;
    cudaSetDevice(gpu);

    if(ibgpu>recBlock && ibgpu<recBlock+NUPDATE+1) threads.push_back(thread(interpolateResidual,h_data[kgpu%2],data,nt-(itgpu*NGPU+gpu)*NUPDATE-ibgpu-1+recBlock,nr,samplingTimeStep));
    if(ibgpu>recBlock+1 && ibgpu<recBlock+NUPDATE+2) cudaMemcpyAsync(d_data[gpu][kgpu%2],h_data[(kgpu+1)%2],nr*sizeof(float),cudaMemcpyHostToDevice,transfInStream[gpu]);
 
    if(kgpu>2 && kgpu<ntb+NUPDATE+2){
     gradient(d_gv[gpu],d_geps[gpu],d_gdel[gpu],ndg,max(0,kgpu-ntb-2),min(kgpu-2,NUPDATE),kgpu,d_SigmaX[gpu],d_SigmaZ[gpu],d_SigmaXa[gpu],d_SigmaZa[gpu],nbuffSigma,d_v[gpu],d_eps[gpu],d_del[gpu],nbuffVEpsDel,wavelet,souIndexBlock,souBlock,d_data[gpu][(kgpu+1)%2],nr,d_recIndex[gpu],recBlock,nx,ny,nz,npad,nt,dx2,dy2,dz2,dt2,computeStream+gpu);
    }
    
    if(kgpu>NUPDATE+3 && kgpu<ntb+NUPDATE+4){
     if(NGPU>1 && gpu<NGPU-1){
      memcpyGpuToGpu3(d_v[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_v[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],nByteBlock,transfOutStream+gpu);
      memcpyGpuToGpu2(d_SigmaX[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaX[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToGpu2(d_SigmaZ[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaZ[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToGpu2(d_SigmaXa[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaXa[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaXa[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaXa[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToGpu2(d_SigmaZa[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaZa[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaZa[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaZa[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
     }
     else{
      memcpyGpuToCpu2(h_SigmaX4[kgpu%2],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[kgpu%2],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToCpu2(h_SigmaZ4[kgpu%2],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[kgpu%2],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToCpu2(h_SigmaXa4[kgpu%2],d_SigmaXa[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaXa5[kgpu%2],d_SigmaXa[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToCpu2(h_SigmaZa4[kgpu%2],d_SigmaZa[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZa5[kgpu%2],d_SigmaZa[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
     }
    
    memcpyGpuToCpu3(h_gv[gpu][kgpu%2],d_gv[gpu][(kgpu-NUPDATE-1)%ndg],h_geps[gpu][kgpu%2],d_geps[gpu][(kgpu-NUPDATE-1)%ndg],h_gdel[gpu][kgpu%2],d_gdel[gpu][(kgpu-NUPDATE-1)%ndg],nByteBlock,transfOutStream+gpu);
	cudaMemsetAsync(d_gv[gpu][(kgpu-NUPDATE-1)%ndg],0,nByteBlock,transfOutStream[gpu]);
	cudaMemsetAsync(d_geps[gpu][(kgpu-NUPDATE-1)%ndg],0,nByteBlock,transfOutStream[gpu]);
	cudaMemsetAsync(d_gdel[gpu][(kgpu-NUPDATE-1)%ndg],0,nByteBlock,transfOutStream[gpu]);
   }
  }
  
   if(k>NGPU*(NUPDATE+3)+1){
    int kgpu=k-(NGPU-1)*(NUPDATE+3);
	int ibgpu=(kgpu-NUPDATE-5)%nb;
    threads.push_back(thread(memcpyCpuToCpu2,prevSigmaX+ibgpu*nElemBlock,h_SigmaX4[(kgpu+1)%2],curSigmaX+ibgpu*nElemBlock,h_SigmaX5[(kgpu+1)%2],nByteBlock));
    threads.push_back(thread(memcpyCpuToCpu2,prevSigmaZ+ibgpu*nElemBlock,h_SigmaZ4[(kgpu+1)%2],curSigmaZ+ibgpu*nElemBlock,h_SigmaZ5[(kgpu+1)%2],nByteBlock));
    threads.push_back(thread(memcpyCpuToCpu2,prevSigmaXa+ibgpu*nElemBlock,h_SigmaXa4[(kgpu+1)%2],curSigmaXa+ibgpu*nElemBlock,h_SigmaXa5[(kgpu+1)%2],nByteBlock));
    threads.push_back(thread(memcpyCpuToCpu2,prevSigmaZa+ibgpu*nElemBlock,h_SigmaZa4[(kgpu+1)%2],curSigmaZa+ibgpu*nElemBlock,h_SigmaZa5[(kgpu+1)%2],nByteBlock));
   }
  
   for(int gpu=0;gpu<NGPU;gpu++){
    int kgpu=k-gpu*(NUPDATE+3);
	int ibgpu=(kgpu-NUPDATE-5)%nb;
    if(kgpu>NUPDATE+4) sumGradientTime(gv+ibgpu*nElemBlock,geps+ibgpu*nElemBlock,gdel+ibgpu*nElemBlock,h_gv[gpu][(kgpu+1)%2],h_geps[gpu][(kgpu+1)%2],h_gdel[gpu][(kgpu+1)%2],nElemBlock);
   }
   
   for(int gpu=0;gpu<NGPU;gpu++){
    cudaSetDevice(gpu);
    cudaDeviceSynchronize();
   }
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
   int ksave=k-NGPU*(NUPDATE+3)-1;
   if(ksave>=0 && ksave%nb==0){
	int itsave=ksave/nb;
    for(int iz=0;iz<nz;iz++) memcpy(backwardSouWavefield+iz*nx+itsave*nx*nz,prevSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
    for(int iz=0;iz<nz;iz++) memcpy(recWavefield+iz*nx+itsave*nx*nz,prevSigmaXa+ny/2*nx+iz*nx*ny,nx*sizeof(float));
   }
 }

 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 long long ncells=nxyz*(nt-2);
 cout<<"ncells "<<ncells<<endl;
 cout<<"for forward speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl; 
 
 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(gpu);
  cudaError_t e=cudaGetLastError();
  if(e!=cudaSuccess) fprintf(stderr,"error in adjoint and imaging %s gpu %d\n",cudaGetErrorString(e),gpu);
 }
 
 write("backwardSouWavefield",backwardSouWavefield,nx*nz*nsave);
 to_header("backwardSouWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("backwardSouWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("backwardSouWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []backwardSouWavefield;

 write("recWavefield",recWavefield,nx*nz*nsave);
 to_header("recWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("recWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("recWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []recWavefield;

 //delete arrays used in forward
 delete []prevSigmaX;delete []curSigmaX;
 delete []prevSigmaZ;delete []curSigmaZ;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_v[i]);
  cudaFreeHost(h_eps[i]);
  cudaFreeHost(h_del[i]);
  cudaFreeHost(h_prevSigmaX[i]);
  cudaFreeHost(h_curSigmaX[i]);
  cudaFreeHost(h_SigmaX4[i]);
  cudaFreeHost(h_SigmaX5[i]);
  cudaFreeHost(h_prevSigmaZ[i]);
  cudaFreeHost(h_curSigmaZ[i]);
  cudaFreeHost(h_SigmaZ4[i]);
  cudaFreeHost(h_SigmaZ5[i]);
  cudaFreeHost(h_data[i]);
 }
 
 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(gpu);

  cudaFree(d_recIndex[gpu]);

  for(int i=0;i<2;i++) cudaFree(d_data[gpu][i]);
  delete []d_data[gpu];
  
  for(int i=0;i<nd_Sigma;++i){
   for(int j=0;j<nbuffSigma[i];++j){
    cudaFree(d_SigmaX[gpu][i][j]); 
    cudaFree(d_SigmaZ[gpu][i][j]); 
   }
   delete []d_SigmaX[gpu][i];
   delete []d_SigmaZ[gpu][i];
  }
  delete []d_SigmaX[gpu];
  delete []d_SigmaZ[gpu];

  for(int i=0;i<nbuffVEpsDel;++i){
   cudaFree(d_v[gpu][i]);
   cudaFree(d_eps[gpu][i]);
   cudaFree(d_del[gpu][i]);
  }
  delete []d_v[gpu];
  delete []d_eps[gpu];
  delete []d_del[gpu];

  cudaStreamDestroy(transfInStream[gpu]);
  cudaStreamDestroy(computeStream[gpu]);
  cudaStreamDestroy(transfOutStream[gpu]);
 }

 delete []d_recIndex;
 delete []d_data;
 delete []d_SigmaX;
 delete []d_SigmaZ;
 delete []d_v;
 delete []d_eps;
 delete []d_del;
 delete []transfInStream;
 delete []computeStream;
 delete []transfOutStream;
 
 delete []data;
 delete []recIndex;
 
 //delete arrays used in adjoint
 delete []prevSigmaXa;delete []curSigmaXa;
 delete []prevSigmaZa;delete []curSigmaZa;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_prevSigmaXa[i]);
  cudaFreeHost(h_curSigmaXa[i]);
  cudaFreeHost(h_SigmaXa4[i]);
  cudaFreeHost(h_SigmaXa5[i]);
  cudaFreeHost(h_prevSigmaZa[i]);
  cudaFreeHost(h_curSigmaZa[i]);
  cudaFreeHost(h_SigmaZa4[i]);
  cudaFreeHost(h_SigmaZa5[i]);
 }
 
 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(gpu);
  
  for(int i=0;i<nd_Sigma;++i){
   for(int j=0;j<nbuffSigma[i];++j){
    cudaFree(d_SigmaXa[gpu][i][j]); 
    cudaFree(d_SigmaZa[gpu][i][j]); 
   }
   delete []d_SigmaXa[gpu][i];
   delete []d_SigmaZa[gpu][i];
  }
  delete []d_SigmaXa[gpu];
  delete []d_SigmaZa[gpu];
 
  for(int i=0;i<ndg;i++){
   cudaFree(d_gv[gpu][i]);
   cudaFree(d_geps[gpu][i]);
   cudaFree(d_gdel[gpu][i]);
  }
  delete []d_gv[gpu];
  delete []d_geps[gpu];
  delete []d_gdel[gpu];
  
  for(int i=0;i<2;i++){
   cudaFreeHost(h_gv[gpu][i]);
   cudaFreeHost(h_geps[gpu][i]);
   cudaFreeHost(h_gdel[gpu][i]);
  }
  delete []h_gv[gpu];
  delete []h_geps[gpu];
  delete []h_gdel[gpu];
 }
 
 delete []d_SigmaXa;
 delete []d_SigmaZa;
 
 delete []d_gv;
 delete []d_geps;
 delete []d_gdel;
 
 delete []h_gv;
 delete []h_geps;
 delete []h_gdel;

 for(int gpu=0;gpu<NGPU;gpu++){
  cudaError_t e=cudaGetLastError();
  if(e!=cudaSuccess) fprintf(stderr,"gpu %d error %s\n",gpu,cudaGetErrorString(e));
 }
 
 return 0.5*obj;
}

