#include <cmath>
#include "matrixSqrt.h"

void sqrtVEpsDel(float *r11,float *r13,float *r33,const float *v,const float *eps,const float *del,int n){
    #pragma omp parallel for num_threads(16)
    for(int i=0;i<n;i++){
        float tau=2.*(eps[i]+1.);
        float delta=2.*(eps[i]-del[i]);
        float s=sqrt(delta);
        float t=sqrt(tau+2.*s);
        float vt=v[i]/t;
        r11[i]=vt*(1.+2.*eps[i]+s);
        r13[i]=vt*sqrt(1.+2.*del[i]);
        r33[i]=vt*(1.+s);
    }
    return;
}
