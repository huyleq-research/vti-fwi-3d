#include <cstdio>
#include <chrono>
#include "myio.h"
#include "mylib.h"
#include "wave3d.h"
#include "ModelingOp.h"
#include "LinearSolver.h"

using namespace std;

int main(int argc,char **argv){
 myio_init(argc,argv);

 int nx,ny,nz,npad,nt;
 float ox,oy,oz,ot,dx,dy,dz,dt;
 
 from_header("v","n1",nx,"o1",ox,"d1",dx);
 from_header("v","n2",ny,"o2",oy,"d2",dy);
 from_header("v","n3",nz,"o3",oz,"d3",dz);
 get_param("npad",npad);
 get_param("nt",nt,"ot",ot,"dt",dt);
 
 long long nxy=nx*ny;
 long long nxyz=nxy*nz;
 
 float samplingRate;
 get_param("samplingRate",samplingRate);
 int samplingTimeStep=std::round(samplingRate/dt);
 
 float *vepsdel=new float[3*nxyz];
 float *v=vepsdel,*eps=vepsdel+nxyz,*del=vepsdel+2*nxyz;
 read("v",v,nxyz);
 read("eps",eps,nxyz);
 read("del",del,nxyz);

 int ns,nr;
 from_header("souloc","n2",ns);
 float *souloc=new float[5*ns];
 read("souloc",souloc,5*ns);

 from_header("recloc","n2",nr);
 float *recloc=new float[3*nr];
 read("recloc",recloc,3*nr);

 float *wavelet=new float[nt]();

 int nnt=(nt-1)/samplingTimeStep+1;
 int nrtotal=souloc[5*(ns-1)+3]+souloc[5*(ns-1)+4];

 float *data=new float[nnt*nrtotal];

 ModelingOp Mop(souloc,ns,recloc,v,eps,del,nx,ny,nz,nt,npad,ox,oy,oz,ot,dx,dy,dz,dt,samplingRate);

// test forward
// read("wavelet",wavelet,nt);
// Mop.forward(false,wavelet,data);
//
// to_header("data","n1",nnt,"o1",ot,"d1",samplingRate);
// to_header("data","n2",nrtotal,"o2",0,"d2",1);
// write("data",data,nnt*nrtotal);

// test adjoint
// memset(wavelet,0,nt*sizeof(float));
 read("wavelet",wavelet,nt);
 
 long long nboundary=nxyz-(nx-2*npad)*(ny-2*npad)*(nz-2*npad);
 float *padboundary=new float[nboundary];
 read("padboundary",padboundary,nboundary);
 
 float *gwavelet1=new float[nt];
 
 float objfunc=objFuncGradientWavelet_f(gwavelet1,souloc,ns,recloc,wavelet,v,eps,del,padboundary,nx,ny,nz,nt,npad,ox,oy,oz,ot,dx,dy,dz,dt,samplingRate);
 
 fprintf(stderr,"objfunc %10.16f\n",objfunc);

 write("gwavelet1",gwavelet1,nt);
 to_header("gwavelet1","n1",nt,"o1",ot,"d1",dt);

 read("data",data,nnt*nrtotal);

 mynegate(data,data,nnt*nrtotal);
 float *gwavelet2=new float[nt];
 Mop.adjoint(false,gwavelet2,data);

 write("gwavelet2",gwavelet2,nt);
 to_header("gwavelet2","n1",nt,"o1",ot,"d1",dt);

 delete []gwavelet1;delete []gwavelet2;

 delete []vepsdel;
 
 delete []wavelet;delete []souloc;delete []recloc;
 delete []data;

 myio_close();
 return 0;
}
