void modelData1Shot1GPU(float *data,float soulocX,float soulocY,float soulocZ,float *recloc,int nr,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float dx,float dy,float dz,float dt,float samplingRate,int device){
 cudaSetDevice(device);
 fprintf(stderr,"NUPDATE %d\n",NUPDATE);
 
 float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;
 long long nxyz=nx*ny*nz;
 int nxy=nx*ny;
 
 int *recIndexBlock=new int[nr];

 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  recIndexBlock[ir]=ixy+(iz%HALF_STENCIL)*nxy;
 }
 int recBlock=recloc[2]/dz/HALF_STENCIL;
 
 int *d_recIndex;
 cudaMalloc(&d_recIndex,nr*sizeof(int));
 cudaMemcpy(d_recIndex,recIndexBlock,nr*sizeof(int),cudaMemcpyHostToDevice);

 int samplingTimeStep=std::round(samplingRate/dt);

 float *prevSigmaX=new float[nxyz]();
 float *curSigmaX=new float[nxyz]();
 float *prevSigmaZ=new float[nxyz]();
 float *curSigmaZ=new float[nxyz]();

 int souIndexX=soulocX/dx;
 int souIndexY=soulocY/dy;
 int souIndexZ=soulocZ/dz;
 int souIndex=souIndexX+souIndexY*nx+souIndexZ*nxy;
 int souIndexBlock=souIndexX+souIndexY*nx+(souIndexZ%HALF_STENCIL)*nxy;
 int souBlock=souIndexZ/HALF_STENCIL;

 int nsave=(nt-2)/NUPDATE+1;
 float *shotWavefield=new float[nx*nz*nsave];

 //injecting source at time 0 to wavefields at time 1
 curSigmaX[souIndex]=dt2*wavelet[0];
 curSigmaZ[souIndex]=dt2*wavelet[0];
 
 size_t nElemBlock=HALF_STENCIL*nxy;
 size_t nByteBlock=nElemBlock*sizeof(float);
 int nb=nz/HALF_STENCIL;

 float *h_v[2],*h_eps[2],*h_del[2];
 float *h_prevSigmaX[2],*h_curSigmaX[2],*h_SigmaX4[2],*h_SigmaX5[2];
 float *h_prevSigmaZ[2],*h_curSigmaZ[2],*h_SigmaZ4[2],*h_SigmaZ5[2];
 float *h_data[2],*d_data[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_v[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_eps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_del[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_data[i],nr*sizeof(float),cudaHostAllocDefault);
  cudaMalloc(&d_data[i],nr*sizeof(float));
 }

 const int nd_Sigma=NUPDATE+2;
 float **d_SigmaX[nd_Sigma],**d_SigmaZ[nd_Sigma];
 int nbuffSigma[nd_Sigma];
 
 for(int i=0;i<nd_Sigma;++i) nbuffSigma[i]=3;
 nbuffSigma[1]=4;nbuffSigma[nd_Sigma-2]=4;
 
 size_t nByteAlloc=0;

 for(int i=0;i<nd_Sigma;++i){
  d_SigmaX[i]=new float*[nbuffSigma[i]]();
  d_SigmaZ[i]=new float*[nbuffSigma[i]]();
  for(int j=0;j<nbuffSigma[i];++j){
   cudaMalloc(&d_SigmaX[i][j],nByteBlock); 
   cudaMalloc(&d_SigmaZ[i][j],nByteBlock); 
   nByteAlloc+=2*nByteBlock;
   cudaMemset(d_SigmaX[i][j],0,nByteBlock);
   cudaMemset(d_SigmaZ[i][j],0,nByteBlock);
  }
 }

 const int nbuffVEpsDel=NUPDATE+4;
 float *d_v[nbuffVEpsDel],*d_eps[nbuffVEpsDel],*d_del[nbuffVEpsDel];
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaMalloc(&d_v[i],nByteBlock);
  cudaMalloc(&d_eps[i],nByteBlock);
  cudaMalloc(&d_del[i],nByteBlock);
  nByteAlloc+=3*nByteBlock;
  cudaMemset(d_v[i],0,nByteBlock);
  cudaMemset(d_eps[i],0,nByteBlock);
  cudaMemset(d_del[i],0,nByteBlock);
 }

 fprintf(stderr,"alloc %f MBs on GPU\n",nByteAlloc*1e-6);

 cudaStream_t computeStream,transfInStream,transfOutStream;
 cudaStreamCreate(&computeStream);
 cudaStreamCreate(&transfInStream);
 cudaStreamCreate(&transfOutStream);

 vector<thread> threads;
 
 int ntb=(nt-2)/NUPDATE*nb;
 int j=0,krecord=0;

 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 for(int k=0;k<ntb+NUPDATE+5;k++){
   if(k<ntb){
	int ib=k%nb;
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(k>0 && k<ntb+1){
    memcpyCpuToGpu3(d_v[k%nbuffVEpsDel],h_v[(k-1)%2],d_eps[k%nbuffVEpsDel],h_eps[(k-1)%2],d_del[k%nbuffVEpsDel],h_del[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k-1)%2],d_SigmaX[1][k%nbuffSigma[1]],h_curSigmaX[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k-1)%2],d_SigmaZ[1][k%nbuffSigma[1]],h_curSigmaZ[(k-1)%2],nByteBlock,&transfInStream);
   }
   
   if(k>2 && k<ntb+NUPDATE+2){
    bool record=forwardAbc(max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nr,d_recIndex,recBlock,samplingTimeStep,d_data[k%2],nx,ny,nz,npad,dx2,dy2,dz2,dt2,&computeStream);
    if(record){ 
	 krecord=k;
	 j++;
	}
   }
   
   if(k>NUPDATE+3 && k<ntb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[k%2],d_SigmaX[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[k%2],d_SigmaX[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[k%2],d_SigmaZ[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[k%2],d_SigmaZ[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
   }
    
    if(k-1==krecord){
     cudaMemcpyAsync(h_data[k%2],d_data[(k+1)%2],nr*sizeof(float),cudaMemcpyDeviceToHost,transfOutStream);
    }
   
   if(k>NUPDATE+4){
	int ib=(k-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(k-1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(k-1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(k-1)%2],nByteBlock);
   }
	
    if(k-2==krecord){
     memcpy(data+j*nr,h_data[(k+1)%2],nr*sizeof(float));
    }
   
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(shotWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

 
 
 
 
 
 for(int it=0;it<(nt-2)/NUPDATE;++it){
  for(int ib=0;ib<nb+NUPDATE+5;++ib){
   if(ib<nb){
    threads.push_back(thread(memcpyCpuToCpu3,h_v[ib%2],v+ib*nElemBlock,h_eps[ib%2],eps+ib*nElemBlock,h_del[ib%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[ib%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[ib%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[ib%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[ib%2],curSigmaZ+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaXa[ib%2],prevSigmaXa+ib*nElemBlock,h_curSigmaXa[ib%2],curSigmaXa+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZa[ib%2],prevSigmaZa+ib*nElemBlock,h_curSigmaZa[ib%2],curSigmaZa+ib*nElemBlock,nByteBlock));
   }
   
   if(ib<recBlock+NUPDATE+1 && ib>recBlock) threads.push_back(thread(interpolateResidual,h_data[ib%2],data,nt-it*NUPDATE-ib-2+recBlock,nr,samplingTimeStep));
   
   if(ib>0 && ib<nb+1){
    memcpyCpuToGpu3(d_v[ib%nbuffVEpsDel],h_v[(ib+1)%2],d_eps[ib%nbuffVEpsDel],h_eps[(ib+1)%2],d_del[ib%nbuffVEpsDel],h_del[(ib+1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(ib-1)%nbuffSigma[0]],h_prevSigmaX[(ib+1)%2],d_SigmaX[1][ib%nbuffSigma[1]],h_curSigmaX[(ib+1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(ib-1)%nbuffSigma[0]],h_prevSigmaZ[(ib+1)%2],d_SigmaZ[1][ib%nbuffSigma[1]],h_curSigmaZ[(ib+1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaXa[0][(ib-1)%nbuffSigma[0]],h_prevSigmaXa[(ib+1)%2],d_SigmaXa[1][ib%nbuffSigma[1]],h_curSigmaXa[(ib+1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZa[0][(ib-1)%nbuffSigma[0]],h_prevSigmaZa[(ib+1)%2],d_SigmaZa[1][ib%nbuffSigma[1]],h_curSigmaZa[(ib+1)%2],nByteBlock,&transfInStream);
   }
   
   if(ib<recBlock+NUPDATE+2 && ib>recBlock+1) cudaMemcpyAsync(d_data[ib%2],h_data[(ib+1)%2],nr*sizeof(float),cudaMemcpyHostToDevice,transfInStream);

   if(ib>2 && ib<nb+NUPDATE+2){
    if(ib<NUPDATE+3){
     cudaMemsetAsync(d_SigmaZ[ib-2][(ib-3)%nbuffSigma[ib-2]],0,nByteBlock,computeStream);
     cudaMemsetAsync(d_SigmaZa[ib-2][(ib-3)%nbuffSigma[ib-2]],0,nByteBlock,computeStream);
	}
    if(ib>nb+1){
     cudaMemsetAsync(d_SigmaZ[ib-nb-1][(ib-1)%nbuffSigma[ib-nb-1]],0,nByteBlock,computeStream);
     cudaMemsetAsync(d_SigmaZa[ib-nb-1][(ib-1)%nbuffSigma[ib-nb-1]],0,nByteBlock,computeStream);
	}
    gradient(d_gv,d_geps,d_gdel,ndg,max(0,ib-nb-2),min(ib-2,NUPDATE),ib,d_SigmaX,d_SigmaZ,d_SigmaXa,d_SigmaZa,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,it,souIndexBlock,souBlock,d_data[(ib+1)%2],nr,d_recIndex,recBlock,nx,ny,nz,npad,nt,dx2,dy2,dz2,dt2,&computeStream);
   }
   
   if(ib>NUPDATE+3 && ib<nb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[ib%2],d_SigmaX[nd_Sigma-2][(ib-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[ib%2],d_SigmaX[nd_Sigma-1][(ib-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[ib%2],d_SigmaZ[nd_Sigma-2][(ib-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[ib%2],d_SigmaZ[nd_Sigma-1][(ib-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaXa4[ib%2],d_SigmaXa[nd_Sigma-2][(ib-4)%nbuffSigma[nd_Sigma-2]],h_SigmaXa5[ib%2],d_SigmaXa[nd_Sigma-1][(ib-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZa4[ib%2],d_SigmaZa[nd_Sigma-2][(ib-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZa5[ib%2],d_SigmaZa[nd_Sigma-1][(ib-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu3(h_gv[ib%2],d_gv[(ib-NUPDATE-1)%ndg],h_geps[ib%2],d_geps[(ib-NUPDATE-1)%ndg],h_gdel[ib%2],d_gdel[(ib-NUPDATE-1)%ndg],nByteBlock,&transfInStream);
	cudaMemsetAsync(d_gv[(ib-NUPDATE-1)%ndg],0,nByteBlock,transfInStream);
	cudaMemsetAsync(d_geps[(ib-NUPDATE-1)%ndg],0,nByteBlock,transfInStream);
	cudaMemsetAsync(d_gdel[(ib-NUPDATE-1)%ndg],0,nByteBlock,transfInStream);
   }
   
   if(ib>NUPDATE+4){
    memcpyCpuToCpu2(prevSigmaX+(ib-NUPDATE-5)*nElemBlock,h_SigmaX4[(ib+1)%2],curSigmaX+(ib-NUPDATE-5)*nElemBlock,h_SigmaX5[(ib+1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+(ib-NUPDATE-5)*nElemBlock,h_SigmaZ4[(ib+1)%2],curSigmaZ+(ib-NUPDATE-5)*nElemBlock,h_SigmaZ5[(ib+1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaXa+(ib-NUPDATE-5)*nElemBlock,h_SigmaXa4[(ib+1)%2],curSigmaXa+(ib-NUPDATE-5)*nElemBlock,h_SigmaXa5[(ib+1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZa+(ib-NUPDATE-5)*nElemBlock,h_SigmaZa4[(ib+1)%2],curSigmaZa+(ib-NUPDATE-5)*nElemBlock,h_SigmaZa5[(ib+1)%2],nByteBlock);
    sumGradientTime(gv+(ib-NUPDATE-5)*nElemBlock,geps+(ib-NUPDATE-5)*nElemBlock,gdel+(ib-NUPDATE-5)*nElemBlock,h_gv[(ib+1)%2],h_geps[(ib+1)%2],h_gdel[(ib+1)%2],nElemBlock);
   }
   
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  }
  for(int iz=0;iz<nz;iz++) memcpy(backwardSouWavefield+iz*nx+(it+1)*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
  for(int iz=0;iz<nz;iz++) memcpy(recWavefield+iz*nx+(it+1)*nx*nz,curSigmaXa+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }



bool forwardAbc(int b,int e,int k,float ***d_SigmaX,float ***d_SigmaZ,const int *nbuffSigma,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int souIndexBlock,int souBlock,int nr,const int *recIndex,int recBlock,int samplingTimeStep,float *data,int nx,int ny,int nz,int npad,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream,int gpu){
 dim3 block(BLOCK_DIM,BLOCK_DIM);
 dim3 grid((nx-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM,(ny-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM);
 
 bool record=false;
 for(int i=b;i<e;++i){
  forwardKernel<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaX[i][(k-3)%nbuffSigma[i]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(k-1)%nbuffSigma[i+1]],d_SigmaZ[i][(k-3)%nbuffSigma[i]],d_v[(k-i-2)%nbuffVEpsDel],d_eps[(k-i-2)%nbuffVEpsDel],d_del[(k-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
 
  int ib=(k-3-i)%(nz/HALF_STENCIL);
  int it=(k-3-i)/(nz/HALF_STENCIL)*NUPDATE*NGPU+gpu*NUPDATE+2+i;

  if(ib==souBlock){
   float source=dt2*wavelet[it-1];
   injectSource<<<1,1,0,*stream>>>(d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],source,souIndexBlock);
  }
 
  abc<<<grid,block,0,*stream>>>(ib,nx,ny,nz,npad,d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(k-2)%nbuffSigma[i+1]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(k-2)%nbuffSigma[i+1]]);
  
  if(ib==recBlock && it%samplingTimeStep==0){
   recordData<<<(nr+BLOCK_DIM-1)/BLOCK_DIM,BLOCK_DIM,0,*stream>>>(data,d_SigmaX[i+2][(k-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(k-1)%nbuffSigma[i+2]],nr,recIndex);
   record=true;
  }
 }
 
 return record;
}

 for(int k=0;k<ntb+NGPU*(NUPDATE+3)+2;k++){
   if(k<ntb){
    int ib=k%nb;
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(k>0 && k<ntb+1){
    cudaSetDevice(0);
    memcpyCpuToGpu3(d_v[0][k%nbuffVEpsDel],h_v[(k+1)%2],d_eps[0][k%nbuffVEpsDel],h_eps[(k+1)%2],d_del[0][k%nbuffVEpsDel],h_del[(k+1)%2],nByteBlock,transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k+1)%2],d_SigmaX[0][1][k%nbuffSigma[1]],h_curSigmaX[(k+1)%2],nByteBlock,transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k+1)%2],d_SigmaZ[0][1][k%nbuffSigma[1]],h_curSigmaZ[(k+1)%2],nByteBlock,transfInStream);
   }
  
   for(int gpu=0;gpu<NGPU;gpu++){
    int kgpu=k-gpu*(NUPDATE+3);

    cudaSetDevice(gpu);

    if(kgpu>2 && kgpu<ntb+NUPDATE+2){
     bool record=forwardAbc(max(0,kgpu-ntb-2),min(kgpu-2,NUPDATE),kgpu,d_SigmaX[gpu],d_SigmaZ[gpu],nbuffSigma,d_v[gpu],d_eps[gpu],d_del[gpu],nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nr,d_recIndex[gpu],recBlock,samplingTimeStep,d_data[gpu][kgpu%2],nx,ny,nz,npad,dx2,dy2,dz2,dt2,computeStream+gpu,gpu);
     if(record){
	  krecord=kgpu;
	  j++;
	 }
	}
    
    if(kgpu>NUPDATE+3 && kgpu<ntb+NUPDATE+4){
     if(NGPU>1 && gpu<NGPU-1){
      memcpyGpuToGpu3(d_v[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_v[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],nByteBlock,transfOutStream+gpu);
      memcpyGpuToGpu2(d_SigmaX[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaX[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToGpu2(d_SigmaZ[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaZ[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
     }
     else{
      memcpyGpuToCpu2(h_SigmaX4[kgpu%2],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[kgpu%2],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
      memcpyGpuToCpu2(h_SigmaZ4[kgpu%2],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[kgpu%2],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
     }
    }
    
	if(kgpu-1==krecord){
     cudaMemcpyAsync(h_data[kgpu%2],d_data[gpu][(kgpu+1)%2],nr*sizeof(float),cudaMemcpyDeviceToHost,transfOutStream[gpu]);
    }
 
    if(kgpu-2==krecord){
     memcpy(data+j*nr,h_data[(kgpu+1)%2],nr*sizeof(float));
    }
   }
   
   if(k>NGPU*(NUPDATE+3)+1){
    int kgpu=k-(NGPU-1)*(NUPDATE+3);
	int ib=(kgpu-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(kgpu+1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(kgpu+1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(kgpu+1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(kgpu+1)%2],nByteBlock);
   }
  
   for(int gpu=0;gpu<NGPU;gpu++){
    cudaSetDevice(gpu);
    cudaDeviceSynchronize();
   }
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
   
   if((k-NGPU*(NUPDATE+3)-1)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(forwardSouWavefield+iz*nx+(k-NGPU*(NUPDATE+3)-1)/nb*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }


 for(int k=0;k<ntb+NUPDATE+5;k++){
   int ib=k%nb,it=k/nb;
   if(k<ntb){
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaXa[k%2],prevSigmaXa+ib*nElemBlock,h_curSigmaXa[k%2],curSigmaXa+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZa[k%2],prevSigmaZa+ib*nElemBlock,h_curSigmaZa[k%2],curSigmaZa+ib*nElemBlock,nByteBlock));
   }
   
   if(ib>recBlock && ib<recBlock+NUPDATE+1) threads.push_back(thread(interpolateResidual,h_data[k%2],data,nt-it*NUPDATE-ib-1+recBlock,nr,samplingTimeStep));
   
   if(k>0 && k<ntb+1){
    memcpyCpuToGpu3(d_v[k%nbuffVEpsDel],h_v[(k-1)%2],d_eps[k%nbuffVEpsDel],h_eps[(k-1)%2],d_del[k%nbuffVEpsDel],h_del[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k-1)%2],d_SigmaX[1][k%nbuffSigma[1]],h_curSigmaX[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k-1)%2],d_SigmaZ[1][k%nbuffSigma[1]],h_curSigmaZ[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaXa[0][(k-1)%nbuffSigma[0]],h_prevSigmaXa[(k-1)%2],d_SigmaXa[1][k%nbuffSigma[1]],h_curSigmaXa[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZa[0][(k-1)%nbuffSigma[0]],h_prevSigmaZa[(k-1)%2],d_SigmaZa[1][k%nbuffSigma[1]],h_curSigmaZa[(k-1)%2],nByteBlock,&transfInStream);
   }
   
   if(ib>recBlock+1 && ib<recBlock+NUPDATE+2) cudaMemcpyAsync(d_data[k%2],h_data[(k+1)%2],nr*sizeof(float),cudaMemcpyHostToDevice,transfInStream);

   if(k>2 && k<ntb+NUPDATE+2){
    gradient(d_gv,d_geps,d_gdel,ndg,max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,d_SigmaXa,d_SigmaZa,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,d_data[(k+1)%2],nr,d_recIndex,recBlock,nx,ny,nz,npad,nt,dx2,dy2,dz2,dt2,&computeStream);
   }
   
   if(k>NUPDATE+3 && k<ntb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[k%2],d_SigmaX[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[k%2],d_SigmaX[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[k%2],d_SigmaZ[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[k%2],d_SigmaZ[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaXa4[k%2],d_SigmaXa[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaXa5[k%2],d_SigmaXa[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZa4[k%2],d_SigmaZa[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZa5[k%2],d_SigmaZa[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu3(h_gv[k%2],d_gv[(k-NUPDATE-1)%ndg],h_geps[k%2],d_geps[(k-NUPDATE-1)%ndg],h_gdel[k%2],d_gdel[(k-NUPDATE-1)%ndg],nByteBlock,&transfOutStream);
	cudaMemsetAsync(d_gv[(k-NUPDATE-1)%ndg],0,nByteBlock,transfOutStream);
	cudaMemsetAsync(d_geps[(k-NUPDATE-1)%ndg],0,nByteBlock,transfOutStream);
	cudaMemsetAsync(d_gdel[(k-NUPDATE-1)%ndg],0,nByteBlock,transfOutStream);
   }
    
   if(k>NUPDATE+4){
	ib=(k-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(k-1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(k-1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaXa+ib*nElemBlock,h_SigmaXa4[(k-1)%2],curSigmaXa+ib*nElemBlock,h_SigmaXa5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZa+ib*nElemBlock,h_SigmaZa4[(k-1)%2],curSigmaZa+ib*nElemBlock,h_SigmaZa5[(k-1)%2],nByteBlock);
    sumGradientTime(gv+ib*nElemBlock,geps+ib*nElemBlock,gdel+ib*nElemBlock,h_gv[(k+1)%2],h_geps[(k+1)%2],h_gdel[(k+1)%2],nElemBlock);
   }
	
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(backwardSouWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,prevSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(recWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,prevSigmaXa+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

