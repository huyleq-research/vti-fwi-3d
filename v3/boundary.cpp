#include <cstring>
#include <cstdlib>
#include <cstdio>

#include "boundary.h"

using namespace std;

void getBoundary(float *boundary,const float *v,int nx,int ny,int nz,int npad){
    size_t currentB=0,currentV=0,n=nx*ny*npad;
    memcpy(boundary+currentB,v+currentV,n*sizeof(float));
    currentB+=n;
    currentV+=n;
    for(int iz=0;iz<nz-2*npad;iz++){
        for(int iy=0;iy<npad;iy++){
            n=nx;
            memcpy(boundary+currentB,v+currentV,n*sizeof(float));
            currentB+=n;
            currentV+=n;
        }
        for(int iy=0;iy<ny-2*npad;iy++){
            n=npad;
            memcpy(boundary+currentB,v+currentV,n*sizeof(float));
            currentB+=n;
            currentV=currentV+nx-npad;
            memcpy(boundary+currentB,v+currentV,n*sizeof(float));
            currentB+=n;
            currentV+=n;
        }
        for(int iy=0;iy<npad;iy++){
            n=nx;
            memcpy(boundary+currentB,v+currentV,n*sizeof(float));
            currentB+=n;
            currentV+=n;
        }
    }
    n=nx*ny*npad;
    memcpy(boundary+currentB,v+currentV,n*sizeof(float));
//    currentB+=n;
//    currentV+=n;
//    long long nboundary=nx*ny*nz-(nx-2*npad)*(ny-2*npad)*(nz-2*npad);
//    fprintf(stderr,"currentB %u currentV %u\n",currentB,currentV);
//    fprintf(stderr,"nboundary %u nxyz %u\n",nboundary,nx*ny*nz);
    return;
}

void putBoundary(const float *boundary,float *v,int nx,int ny,int nz,int npad){
    size_t currentB=0,currentV=0,n=nx*ny*npad;
    memcpy(v+currentV,boundary+currentB,n*sizeof(float));
    currentB+=n;
    currentV+=n;
    for(int iz=0;iz<nz-2*npad;iz++){
        for(int iy=0;iy<npad;iy++){
            n=nx;
            memcpy(v+currentV,boundary+currentB,n*sizeof(float));
            currentB+=n;
            currentV+=n;
        }
        for(int iy=0;iy<ny-2*npad;iy++){
            n=npad;
            memcpy(v+currentV,boundary+currentB,n*sizeof(float));
            currentB+=n;
            currentV=currentV+nx-npad;
            memcpy(v+currentV,boundary+currentB,n*sizeof(float));
            currentB+=n;
            currentV+=n;
        }
        for(int iy=0;iy<npad;iy++){
            n=nx;
            memcpy(v+currentV,boundary+currentB,n*sizeof(float));
            currentB+=n;
            currentV+=n;
        }
    }
    n=nx*ny*npad;
    memcpy(v+currentV,boundary+currentB,n*sizeof(float));
//    currentB+=n;
//    currentV+=n;
//    long long nboundary=nx*ny*nz-(nx-2*npad)*(ny-2*npad)*(nz-2*npad);
//    fprintf(stderr,"currentB %u currentV %u\n",currentB,currentV);
//    fprintf(stderr,"nboundary %u nxyz %u\n",nboundary,nx*ny*nz);
    return;
}

void zeroBoundary(float *v,int nx,int ny,int nz,int npad){
    size_t currentV=0,n=nx*ny*npad;
    memset(v+currentV,0,n*sizeof(float));
    currentV+=n;
    for(int iz=0;iz<nz-2*npad;iz++){
        for(int iy=0;iy<npad;iy++){
            n=nx;
            memset(v+currentV,0,n*sizeof(float));
            currentV+=n;
        }
        for(int iy=0;iy<ny-2*npad;iy++){
            n=npad;
            memset(v+currentV,0,n*sizeof(float));
            currentV=currentV+nx-npad;
            memset(v+currentV,0,n*sizeof(float));
            currentV+=n;
        }
        for(int iy=0;iy<npad;iy++){
            n=nx;
            memset(v+currentV,0,n*sizeof(float));
            currentV+=n;
        }
    }
    n=nx*ny*npad;
    memset(v+currentV,0,n*sizeof(float));
//    currentB+=n;
//    currentV+=n;
//    long long nboundary=nx*ny*nz-(nx-2*npad)*(ny-2*npad)*(nz-2*npad);
//    fprintf(stderr,"currentB %u currentV %u\n",currentB,currentV);
//    fprintf(stderr,"nboundary %u nxyz %u\n",nboundary,nx*ny*nz);
    return;
}
