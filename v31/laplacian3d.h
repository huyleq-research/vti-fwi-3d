#ifndef LAPLACIAN3D_H
#define LAPLACIAN3D_H

extern "C"{
 void lap3d(int n,int nx,int nxy,float dx2,float dy2,float dz2,float *out,float *int);
}

#endif
