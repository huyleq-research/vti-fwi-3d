#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iostream>

#include <tbb/tbb.h>
#include <tbb/blocked_range.h>

#include "myio.h"
#include "mylib.h"
#include "init.h"
#include "laplacian3d.h"

#define HALF_STENCIL 4
#define DAMPER 0.95
#define PI 3.14159265359

using namespace std;

class Block{
    public:
    Block(int beginX,int endX,int beginY,int endY,int beginZ,int endZ):_beginX(beginX),_endX(endX),_beginY(beginY),_endY(endY),_beginZ(beginZ),_endZ(endZ){};
    int _beginX,_endX,_beginY,_endY,_beginZ,_endZ;
};

void abc(float *sigma,const float *damping,int nx,int ny,int nz,int npad){
    #pragma omp parallel for num_threads(16)
    for(int iz=0;iz<npad;iz++){
        for(int iy=0;iy<ny;iy++){
            for(int ix=0;ix<nx;ix++){
                sigma[ix+iy*nx+iz*nx*ny]*=damping[iz]; 
                sigma[ix+iy*nx+(nz-1-iz)*nx*ny]*=damping[iz]; 
            }
        }
    }
    #pragma omp parallel for num_threads(16)
    for(int iz=0;iz<nz;iz++){
        for(int iy=0;iy<npad;iy++){
            for(int ix=0;ix<nx;ix++){
                sigma[ix+iy*nx+iz*nx*ny]*=damping[iy]; 
                sigma[ix+(ny-1-iy)*nx+iz*nx*ny]*=damping[iy]; 
            }
        }
    }
    #pragma omp parallel for num_threads(16)
    for(int iz=0;iz<nz;iz++){
        for(int iy=0;iy<ny;iy++){
            for(int ix=0;ix<npad;ix++){
                sigma[ix+iy*nx+iz*nx*ny]*=damping[ix]; 
                sigma[nx-1-ix+iy*nx+iz*nx*ny]*=damping[ix]; 
            }
        }
    }
    return;
}

int main(int argc,char **argv){
    myio_init(argc,argv);

    int nx,ny,nz,nt,npad;
    float ox,oy,oz,ot,dx,dy,dz,dt;

    get_param("nx",nx,"ox",ox,"dx",dx);
    get_param("ny",ny,"oy",oy,"dy",dy);
    get_param("nz",nz,"oz",oz,"dz",dz);
    get_param("nt",nt,"ot",ot,"dt",dt);
    get_param("npad",npad);
    
    float *damping=new float[npad];
    for(int i=0;i<npad;i++) damping[i]=DAMPER+(1.-DAMPER)*cos(PI*(npad-i)/npad);

    float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;

    float *wavelet=new float[nt]();
    float freq,scalefactor;
    get_param("freq",freq,"scalefactor",scalefactor);
    ricker(wavelet,freq,nt,dt,scalefactor);

    float reclocZ;
    get_param("reclocZ",reclocZ);
    int recIndexZ=reclocZ/dz;

    int nxy=nx*ny;
    long long nxyz=nx*ny*nz;
    
    float *prevSigmaX=new float[nxyz];
    float *curSigmaX=new float[nxyz];
    float *prevSigmaZ=new float[nxyz];
    float *curSigmaZ=new float[nxyz];
    float *p=new float[nxyz]();

    float *v=new float[nxyz]();
    read("v",v,nxyz);
    float *eps=new float[nxyz]();
    read("eps",eps,nxyz);
    float *del=new float[nxyz]();
    read("del",del,nxyz);

    int blockSizeX,blockSizeY,blockSizeZ;
    get_param("blockSizeX",blockSizeX,"blockSizeY",blockSizeY,"blockSizeZ",blockSizeZ);

    vector<int> beginX(1,HALF_STENCIL),endX(1,HALF_STENCIL+blockSizeX);
    int nleft=nx-2*HALF_STENCIL-blockSizeX,i=0;
    while(nleft>0){
        int blockSize=min(nleft,blockSizeX);
        beginX.push_back(endX[i]);
        endX.push_back(endX[i]+blockSize);
        ++i;
        nleft-=blockSize;
    }
    
    vector<int> beginY(1,HALF_STENCIL),endY(1,HALF_STENCIL+blockSizeY);
    nleft=ny-2*HALF_STENCIL-blockSizeY;i=0;
    while(nleft>0){
        int blockSize=min(nleft,blockSizeY);
        beginY.push_back(endY[i]);
        endY.push_back(endY[i]+blockSize);
        ++i;
        nleft-=blockSize;
    }
    
    vector<int> beginZ(1,HALF_STENCIL),endZ(1,HALF_STENCIL+blockSizeZ);
    nleft=nz-2*HALF_STENCIL-blockSizeZ;i=0;
    while(nleft>0){
        int blockSize=min(nleft,blockSizeZ);
        beginZ.push_back(endZ[i]);
        endZ.push_back(endZ[i]+blockSize);
        ++i;
        nleft-=blockSize;
    }
    
    vector<Block> blocks;
    for(int iz=0;iz<beginZ.size();iz++){
        for(int iy=0;iy<beginY.size();iy++){
            for(int ix=0;ix<beginX.size();ix++){
                blocks.push_back(Block(beginX[ix],endX[ix],beginY[iy],endY[iy],beginZ[iz],endZ[iz]));
            }
        }
    }

    float samplingRate;
    get_param("samplingRate",samplingRate);
    
    int samplingTimeStep=std::round(samplingRate/dt);
    int nnt=(nt-1)/samplingTimeStep+1;
    
    float soulocZ;
    get_param("soulocZ",soulocZ);
    int souIndexZ=soulocZ/dz;
    
    int nsouX,nsouY;
    float osouX,osouY,dsouX,dsouY;
    get_param("nsouX",nsouX,"osouX",osouX,"dsouX",dsouX);
    get_param("nsouY",nsouY,"osouY",osouY,"dsouY",dsouY);
   
    //true data using abc
    fprintf(stderr,"computing observed data\n");

    int nsou=nsouX*nsouY;
    float *data=new float[nxy*nnt*nsou]();

    int nsave=nnt;
    float *forwardSouWavefield=new float[nx*nz*nsave];
    memset(forwardSouWavefield,0,nx*nz*sizeof(float));

    for(int isouY=0;isouY<nsouY;isouY++){
        float soulocY=isouY*dsouY+osouY;
        for(int isouX=0;isouX<nsouX;isouX++){
            float soulocX=isouX*dsouX+osouX;
            int isou=isouX+isouY*nsouX;
            int souIndexX=soulocX/dx;
            int souIndexY=soulocY/dy;

            fprintf(stderr,"shot %d\n",isou);
            
            memset(curSigmaX,0,nxyz*sizeof(float));
            memset(prevSigmaX,0,nxyz*sizeof(float));
            memset(curSigmaZ,0,nxyz*sizeof(float));
            memset(prevSigmaZ,0,nxyz*sizeof(float));

            curSigmaX[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[0]*dt2;
            curSigmaZ[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[0]*dt2;
            
            for(int it=2;it<nt;it++){
                tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),
           	                   [&](const tbb::blocked_range<int>&r){
               	    for(int ib=r.begin();ib!=r.end();++ib){
                        for(int iz=blocks[ib]._beginZ;iz<blocks[ib]._endZ;iz++){
                            for(int iy=blocks[ib]._beginY;iy<blocks[ib]._endY;iy++){
                                int i=blocks[ib]._beginX+iy*nx+iz*nxy;
           	                    laplacian3d(blocks[ib]._endX-blocks[ib]._beginX,nx,nxy,dx2,dy2,dz2,dt2,v+i,eps+i,del+i,prevSigmaX+i,curSigmaX+i,prevSigmaZ+i,curSigmaZ+i);
                            }                                                           
                        }                                                                        
                    }                                                                            
                });                                                                     
        
                float *pt=prevSigmaX;prevSigmaX=curSigmaX;curSigmaX=pt;
                pt=prevSigmaZ;prevSigmaZ=curSigmaZ;curSigmaZ=pt;
        
                curSigmaX[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[it-1]*dt2;
                curSigmaZ[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[it-1]*dt2;
                
                abc(prevSigmaX,damping,nx,ny,nz,npad);
                abc(prevSigmaZ,damping,nx,ny,nz,npad);
                abc(curSigmaX,damping,nx,ny,nz,npad);
                abc(curSigmaZ,damping,nx,ny,nz,npad);
        
                if(it%samplingTimeStep==0){
                    #pragma omp parallel for num_threads(16)
                    for(int i=0;i<nxy;i++) data[i+it/samplingTimeStep*nxy+isou*nxy*nnt]=2./3.*curSigmaX[i+recIndexZ*nxy]+1./3.*curSigmaZ[i+recIndexZ*nxy];
                    for(int iz=0;iz<nz;iz++) memcpy(forwardSouWavefield+iz*nx+(it/samplingTimeStep)*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
                }
                
            }
        }
    }
   
    write("data",data,nxy*nnt*nsou);
    to_header("data","n1",nx,"o1",ox,"d1",dx);
    to_header("data","n2",ny,"o2",oy,"d2",dy);
    to_header("data","n3",nnt,"o3",ot,"d3",samplingRate);
    to_header("data","n4",nsouX,"o4",osouX,"d4",dsouX);
    to_header("data","n5",nsouY,"o5",osouY,"d5",dsouY);
    
    write("shotWavefield",forwardSouWavefield,nx*nz*nsave);
    to_header("shotWavefield","n1",nx,"o1",0.,"d1",dx);
    to_header("shotWavefield","n2",nz,"o2",0.,"d2",dz);
    to_header("shotWavefield","n3",nsave,"o3",0.,"d3",samplingRate);

    //bg data using abc 
    read("bgv",v,nxyz);
    read("bgeps",eps,nxyz);
    read("bgdel",del,nxyz);
    
    float *bgdata=new float[nxy*nnt*nsou]();

    fprintf(stderr,"computing modeled data\n");

    for(int isouY=0;isouY<nsouY;isouY++){
        float soulocY=isouY*dsouY+osouY;
        for(int isouX=0;isouX<nsouX;isouX++){
            float soulocX=isouX*dsouX+osouX;
            int isou=isouX+isouY*nsouX;
            int souIndexX=soulocX/dx;
            int souIndexY=soulocY/dy;
            
            fprintf(stderr,"shot %d\n",isou);
            
            memset(curSigmaX,0,nxyz*sizeof(float));
            memset(prevSigmaX,0,nxyz*sizeof(float));
            memset(curSigmaZ,0,nxyz*sizeof(float));
            memset(prevSigmaZ,0,nxyz*sizeof(float));

            curSigmaX[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[0]*dt2;
            curSigmaZ[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[0]*dt2;
            
            for(int it=2;it<nt;it++){
                tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),
           	                   [&](const tbb::blocked_range<int>&r){
               	    for(int ib=r.begin();ib!=r.end();++ib){
                        for(int iz=blocks[ib]._beginZ;iz<blocks[ib]._endZ;iz++){
                            for(int iy=blocks[ib]._beginY;iy<blocks[ib]._endY;iy++){
                                int i=blocks[ib]._beginX+iy*nx+iz*nxy;
           	                    laplacian3d(blocks[ib]._endX-blocks[ib]._beginX,nx,nxy,dx2,dy2,dz2,dt2,v+i,eps+i,del+i,prevSigmaX+i,curSigmaX+i,prevSigmaZ+i,curSigmaZ+i);
                            }                                                           
                        }                                                                        
                    }                                                                            
                });                                                                     
        
                float *pt=prevSigmaX;prevSigmaX=curSigmaX;curSigmaX=pt;
                pt=prevSigmaZ;prevSigmaZ=curSigmaZ;curSigmaZ=pt;
        
                curSigmaX[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[it-1]*dt2;
                curSigmaZ[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[it-1]*dt2;
                
                abc(prevSigmaX,damping,nx,ny,nz,npad);
                abc(prevSigmaZ,damping,nx,ny,nz,npad);
                abc(curSigmaX,damping,nx,ny,nz,npad);
                abc(curSigmaZ,damping,nx,ny,nz,npad);
        
                if(it%samplingTimeStep==0){
                    #pragma omp parallel for num_threads(16)
                    for(int i=0;i<nxy;i++) bgdata[i+it/samplingTimeStep*nxy+isou*nxy*nnt]=2./3.*curSigmaX[i+recIndexZ*nxy]+1./3.*curSigmaZ[i+recIndexZ*nxy];
                    for(int iz=0;iz<nz;iz++) memcpy(forwardSouWavefield+iz*nx+(it/samplingTimeStep)*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
                }
            }
        }
    }
   
    write("bgdata",bgdata,nxy*nnt*nsou);
    to_header("bgdata","n1",nx,"o1",ox,"d1",dx);
    to_header("bgdata","n2",ny,"o2",oy,"d2",dy);
    to_header("bgdata","n3",nnt,"o3",ot,"d3",samplingRate);
    to_header("bgdata","n4",nsouX,"o4",osouX,"d4",dsouX);
    to_header("bgdata","n5",nsouY,"o5",osouY,"d5",dsouY);
    
    write("forwardSouWavefieldAbc",forwardSouWavefield,nx*nz*nsave);
    to_header("forwardSouWavefieldAbc","n1",nx,"o1",0.,"d1",dx);
    to_header("forwardSouWavefieldAbc","n2",nz,"o2",0.,"d2",dz);
    to_header("forwardSouWavefieldAbc","n3",nsave,"o3",0.,"d3",samplingRate);

    //residual and obj
    subtract(data,bgdata,data,nxy*nnt*nsou);
    
    double obj=0.5*dot_product(data,data,nxy*nnt*nsou);
    fprintf(stderr,"obj function %f\n",obj);

    //computing the gradients
    fprintf(stderr,"computing the gradients\n");

    float *gv=new float[nxyz]();
    float *geps=new float[nxyz]();
    float *gdel=new float[nxyz]();
    
    float *prevSigmaXa=new float[nxyz];
    float *curSigmaXa=new float[nxyz];
    float *prevSigmaZa=new float[nxyz];
    float *curSigmaZa=new float[nxyz];
    
    float *backwardSouWavefield=new float[nx*nz*nsave];
    float *recWavefield=new float[nx*nz*nsave];
    
    for(int isouY=0;isouY<nsouY;isouY++){
        float soulocY=isouY*dsouY+osouY;
        for(int isouX=0;isouX<nsouX;isouX++){
            float soulocX=isouX*dsouX+osouX;
            int isou=isouX+isouY*nsouX;
            int souIndexX=soulocX/dx;
            int souIndexY=soulocY/dy;
            
            fprintf(stderr,"shot %d\n",isou);
            
//            cout<<"forward modeling source wavefields with random boundary"<<endl;
            memset(curSigmaX,0,nxyz*sizeof(float));
            memset(prevSigmaX,0,nxyz*sizeof(float));
            memset(curSigmaZ,0,nxyz*sizeof(float));
            memset(prevSigmaZ,0,nxyz*sizeof(float));
        
            curSigmaX[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[0]*dt2;
            curSigmaZ[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[0]*dt2;
            
            for(int it=2;it<nt;it++){
                tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),
           	                   [&](const tbb::blocked_range<int>&r){
               	    for(int ib=r.begin();ib!=r.end();++ib){
                        for(int iz=blocks[ib]._beginZ;iz<blocks[ib]._endZ;iz++){
                            for(int iy=blocks[ib]._beginY;iy<blocks[ib]._endY;iy++){
                                int i=blocks[ib]._beginX+iy*nx+iz*nxy;
           	                    laplacian3d(blocks[ib]._endX-blocks[ib]._beginX,nx,nxy,dx2,dy2,dz2,dt2,v+i,eps+i,del+i,prevSigmaX+i,curSigmaX+i,prevSigmaZ+i,curSigmaZ+i);
                            }                                                           
                        }                                                                        
                    }                                                                            
                });                                                                     
        
                float *pt=prevSigmaX;prevSigmaX=curSigmaX;curSigmaX=pt;
                pt=prevSigmaZ;prevSigmaZ=curSigmaZ;curSigmaZ=pt;
        
                curSigmaX[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[it-1]*dt2;
                curSigmaZ[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[it-1]*dt2;
                
                if(it%samplingTimeStep==0) for(int iz=0;iz<nz;iz++) memcpy(forwardSouWavefield+iz*nx+(it/samplingTimeStep)*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
            }
            
           
//            cout<<"flip time direction of source wavefields"<<endl;
            float *pt;
            pt=curSigmaX;curSigmaX=prevSigmaX;prevSigmaX=pt;
            pt=curSigmaZ;curSigmaZ=prevSigmaZ;prevSigmaZ=pt;
            
//            cout<<"backward propagate source and receiver wavefields and imaging to get gradients"<<endl;
        
            memset(curSigmaXa,0,nxyz*sizeof(float));
            memset(prevSigmaXa,0,nxyz*sizeof(float));
            memset(curSigmaZa,0,nxyz*sizeof(float));
            memset(prevSigmaZa,0,nxyz*sizeof(float));
            
            #pragma omp parallel for num_threads(16)
            for(int i=0;i<nxy;i++){
                curSigmaXa[i+recIndexZ*nxy]+=2./3.*dt2*data[i+(nnt-1)*nxy+isou*nxy*nnt];
                curSigmaZa[i+recIndexZ*nxy]+=1./3.*dt2*data[i+(nnt-1)*nxy+isou*nxy*nnt];
            }
        
            for(int it=2;it<nt;it++){
//                cout<<"backward source wavefields and imaging"<<endl;
                tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),
           	                   [&](const tbb::blocked_range<int>&r){
               	    for(int ib=r.begin();ib!=r.end();++ib){
                        for(int iz=blocks[ib]._beginZ;iz<blocks[ib]._endZ;iz++){
                            for(int iy=blocks[ib]._beginY;iy<blocks[ib]._endY;iy++){
                                int i=blocks[ib]._beginX+iy*nx+iz*nxy;
           	                    imaging(gv+i,geps+i,gdel+i,blocks[ib]._endX-blocks[ib]._beginX,nx,nxy,dx2,dy2,dz2,dt2,v+i,eps+i,del+i,prevSigmaX+i,curSigmaX+i,prevSigmaZ+i,curSigmaZ+i,curSigmaXa+i,curSigmaZa+i);
                            }                                                           
                        }                                                                        
                    }                                                                            
                });                                                                     
        
                pt=prevSigmaX;prevSigmaX=curSigmaX;curSigmaX=pt;
                pt=prevSigmaZ;prevSigmaZ=curSigmaZ;curSigmaZ=pt;
        
                curSigmaX[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[nt-it]*dt2;
                curSigmaZ[souIndexX+souIndexY*nx+souIndexZ*nxy]+=wavelet[nt-it]*dt2;
                
//                cout<<"backward receiver wavefields"<<endl;
                tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),
           	                   [&](const tbb::blocked_range<int>&r){
               	    for(int ib=r.begin();ib!=r.end();++ib){
                        for(int iz=blocks[ib]._beginZ;iz<blocks[ib]._endZ;iz++){
                            for(int iy=blocks[ib]._beginY;iy<blocks[ib]._endY;iy++){
                                int i=blocks[ib]._beginX+iy*nx+iz*nxy;
           	                    laplacian3d(blocks[ib]._endX-blocks[ib]._beginX,nx,nxy,dx2,dy2,dz2,dt2,v+i,eps+i,del+i,prevSigmaXa+i,curSigmaXa+i,prevSigmaZa+i,curSigmaZa+i);
                            }                                                           
                        }                                                                        
                    }                                                                            
                });                                                                     
        
                pt=prevSigmaXa;prevSigmaXa=curSigmaXa;curSigmaXa=pt;
                pt=prevSigmaZa;prevSigmaZa=curSigmaZa;curSigmaZa=pt;
        
//                cout<<"injecting residual"<<endl;
                int timeIndex=nt-it;
                float f=float(timeIndex)/float(samplingTimeStep);
                int j=f;
                f=f-j;
                #pragma omp parallel for num_threads(16)
                for(int i=0;i<nxy;i++){
                    float residual=(1.-f)*data[i+j*nxy+isou*nxy*nnt]+f*data[i+(j+1)*nxy+isou*nxy*nnt];
                    curSigmaXa[i+recIndexZ*nxy]+=2./3.*dt2*residual;
                    curSigmaZa[i+recIndexZ*nxy]+=1./3.*dt2*residual;
                }
                
//                cout<<"abc"<<endl;
                abc(prevSigmaXa,damping,nx,ny,nz,npad);
                abc(prevSigmaZa,damping,nx,ny,nz,npad);
                abc(curSigmaXa,damping,nx,ny,nz,npad);
                abc(curSigmaZa,damping,nx,ny,nz,npad);
            
                if(it%samplingTimeStep==0){ 
                    for(int iz=0;iz<nz;iz++){
                        memcpy(backwardSouWavefield+iz*nx+(it/samplingTimeStep)*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
                        memcpy(recWavefield+iz*nx+(it/samplingTimeStep)*nx*nz,curSigmaXa+ny/2*nx+iz*nx*ny,nx*sizeof(float));
                    }
                }
            }
        }
    }
    
    write("forwardSouWavefield",forwardSouWavefield,nx*nz*nsave);
    to_header("forwardSouWavefield","n1",nx,"o1",0.,"d1",dx);
    to_header("forwardSouWavefield","n2",nz,"o2",0.,"d2",dz);
    to_header("forwardSouWavefield","n3",nsave,"o3",0.,"d3",samplingRate);

    write("backwardSouWavefield",backwardSouWavefield,nx*nz*nsave);
    to_header("backwardSouWavefield","n1",nx,"o1",0.,"d1",dx);
    to_header("backwardSouWavefield","n2",nz,"o2",0.,"d2",dz);
    to_header("backwardSouWavefield","n3",nsave,"o3",0.,"d3",samplingRate);

    write("recWavefield",recWavefield,nx*nz*nsave);
    to_header("recWavefield","n1",nx,"o1",0.,"d1",dx);
    to_header("recWavefield","n2",nz,"o2",0.,"d2",dz);
    to_header("recWavefield","n3",nsave,"o3",0.,"d3",samplingRate);

    delete []forwardSouWavefield;
    delete []backwardSouWavefield;
    delete []recWavefield;
            
    write("gv",gv,nxyz);
    to_header("gv","n1",nx,"o1",ox,"d1",dx);
    to_header("gv","n2",ny,"o2",oy,"d2",dy);
    to_header("gv","n3",nz,"o3",oz,"d3",dz);
    
    write("geps",geps,nxyz);
    to_header("geps","n1",nx,"o1",ox,"d1",dx);
    to_header("geps","n2",ny,"o2",oy,"d2",dy);
    to_header("geps","n3",nz,"o3",oz,"d3",dz);
    
    write("gdel",gdel,nxyz);
    to_header("gdel","n1",nx,"o1",ox,"d1",dx);
    to_header("gdel","n2",ny,"o2",oy,"d2",dy);
    to_header("gdel","n3",nz,"o3",oz,"d3",dz);
    
    delete []data;delete []bgdata;
    
    delete []wavelet;delete []damping;
    delete []prevSigmaX;delete []curSigmaX;
    delete []prevSigmaZ;delete []curSigmaZ;
    delete []p;
    delete []v;delete []eps;delete []del;
    
    delete []prevSigmaXa;delete []curSigmaXa;
    delete []prevSigmaZa;delete []curSigmaZa;
    delete []gv;delete []geps;delete []gdel;
    
    myio_close();
    return 0;
}

