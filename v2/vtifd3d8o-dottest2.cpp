#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iostream>

#include <tbb/tbb.h>
#include <tbb/blocked_range.h>

#include "myio.h"
#include "mylib.h"
#include "laplacian3d.h"
#include "boundary.h"

#define HALF_STENCIL 4
#define DAMPER 0.95
#define PI 3.14159265359

using namespace std;

class Block{
    public:
    Block(int beginX,int endX,int beginY,int endY,int beginZ,int endZ):_beginX(beginX),_endX(endX),_beginY(beginY),_endY(endY),_beginZ(beginZ),_endZ(endZ){};
    int _beginX,_endX,_beginY,_endY,_beginZ,_endZ;
};

void abc(float *sigma,const float *damping,int nx,int ny,int nz,int npad){
    #pragma omp parallel for num_threads(16)
    for(int iz=0;iz<npad;iz++){
        for(int iy=0;iy<ny;iy++){
            for(int ix=0;ix<nx;ix++){
                sigma[ix+iy*nx+iz*nx*ny]*=damping[iz]; 
                sigma[ix+iy*nx+(nz-1-iz)*nx*ny]*=damping[iz]; 
            }
        }
    }
    #pragma omp parallel for num_threads(16)
    for(int iz=0;iz<nz;iz++){
        for(int iy=0;iy<npad;iy++){
            for(int ix=0;ix<nx;ix++){
                sigma[ix+iy*nx+iz*nx*ny]*=damping[iy]; 
                sigma[ix+(ny-1-iy)*nx+iz*nx*ny]*=damping[iy]; 
            }
        }
    }
    #pragma omp parallel for num_threads(16)
    for(int iz=0;iz<nz;iz++){
        for(int iy=0;iy<ny;iy++){
            for(int ix=0;ix<npad;ix++){
                sigma[ix+iy*nx+iz*nx*ny]*=damping[ix]; 
                sigma[nx-1-ix+iy*nx+iz*nx*ny]*=damping[ix]; 
            }
        }
    }
    return;
}

int main(int argc,char **argv){
    myio_init(argc,argv);

    float cc[5]={C0,C1,C2,C3,C4};

    int nx,ny,nz,nt,npad;
    float ox,oy,oz,ot,dx,dy,dz,dt;

    from_header("v","n1",nx,"o1",ox,"d1",dx);
    from_header("v","n2",ny,"o2",oy,"d2",dy);
    from_header("v","n3",nz,"o3",oz,"d3",dz);
    get_param("npad",npad);
    get_param("nt",nt,"ot",ot,"dt",dt);
    
    long long nxy=nx*ny;
    long long nxyz=nxy*nz;
    long long nboundary=nxyz-(nx-2*npad)*(ny-2*npad)*(nz-2*npad);
    
    float *wavelet=new float[nt]();
    read("wavelet",wavelet,nt);
    float *rwavelet=new float[nt]();
    for(int it=0;it<nt;it++) rwavelet[it]=wavelet[nt-1-it];

    float samplingRate;
    get_param("samplingRate",samplingRate);
    int samplingTimeStep=std::round(samplingRate/dt);
    int nnt=(nt-1)/samplingTimeStep+1;

    float *dwavefield=new float[nx*nz*nnt]();
    
    float *damping=new float[npad];
    for(int i=0;i<npad;i++) damping[i]=DAMPER+(1.-DAMPER)*cos(PI*(npad-i)/npad);

    float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;

    float *v=new float[nxyz];
    read("v",v,nxyz);
    float *eps=new float[nxyz];
    read("eps",eps,nxyz);
    float *del=new float[nxyz];
    read("del",del,nxyz);

    float *c11=new float[nxyz]();
    float *c13=new float[nxyz]();
    float *c33=new float[nxyz]();

    #pragma omp parallel for num_threads(16)
    for(size_t i=0;i<nxyz;i++){
        c33[i]=v[i]*v[i];
        c11[i]=c33[i]*(1.+2.*eps[i]);
        c13[i]=c33[i]*sqrt(1.+2.*del[i]);
    }
    
    float *dc11=new float[nxyz]();
    float *dc13=new float[nxyz]();
    float *dc33=new float[nxyz]();
    
    int locx=nx/2,locy=ny/2,locz=nz/2+4;
    #pragma omp parallel for num_threads(16)
    for(int iz=locz;iz<locz+2;iz++){
        for(int iy=locy;iy<locy+2;iy++){
            for(int ix=locx;ix<locx+2;ix++){
                int i=ix+iy*nx+iz*nxy;
//                dc11[i]=0.1*c11[i];
//                dc13[i]=0.1*c13[i];
                dc33[i]=0.1*c33[i];
            }
        }
    }

//    float *randomboundary=new float[nboundary];
//    read("randomboundary",randomboundary,nboundary);
//    float *padboundary=new float[nboundary];
//    read("padboundary",padboundary,nboundary);
    
    int ns,nr;
    from_header("souloc","n2",ns);
    float *souloc=new float[5*ns];
    read("souloc",souloc,5*ns);
    
    from_header("recloc","n2",nr);
    float *recloc=new float[3*nr];
    read("recloc",recloc,3*nr);
    
    float *gc11=new float[nxyz]();
    float *gc13=new float[nxyz]();
    float *gc33=new float[nxyz]();

    float *prevSigmaX=new float[nxyz];
    float *curSigmaX=new float[nxyz];
    float *prevSigmaZ=new float[nxyz];
    float *curSigmaZ=new float[nxyz];

    float *prevDSigmaX=new float[nxyz];
    float *curDSigmaX=new float[nxyz];
    float *prevDSigmaZ=new float[nxyz];
    float *curDSigmaZ=new float[nxyz];

    float *prevSigmaXa=new float[nxyz];
    float *curSigmaXa=new float[nxyz];
    float *prevSigmaZa=new float[nxyz];
    float *curSigmaZa=new float[nxyz];
    
    int blockSizeX,blockSizeY,blockSizeZ;
    get_param("blockSizeX",blockSizeX,"blockSizeY",blockSizeY,"blockSizeZ",blockSizeZ);

    vector<int> beginX(1,HALF_STENCIL),endX(1,HALF_STENCIL+blockSizeX);
    int nleft=nx-2*HALF_STENCIL-blockSizeX,i=0;
    while(nleft>0){
        int blockSize=min(nleft,blockSizeX);
        beginX.push_back(endX[i]);
        endX.push_back(endX[i]+blockSize);
        ++i;
        nleft-=blockSize;
    }
    
    vector<int> beginY(1,HALF_STENCIL),endY(1,HALF_STENCIL+blockSizeY);
    nleft=ny-2*HALF_STENCIL-blockSizeY;i=0;
    while(nleft>0){
        int blockSize=min(nleft,blockSizeY);
        beginY.push_back(endY[i]);
        endY.push_back(endY[i]+blockSize);
        ++i;
        nleft-=blockSize;
    }
    
    vector<int> beginZ(1,HALF_STENCIL),endZ(1,HALF_STENCIL+blockSizeZ);
    nleft=nz-2*HALF_STENCIL-blockSizeZ;i=0;
    while(nleft>0){
        int blockSize=min(nleft,blockSizeZ);
        beginZ.push_back(endZ[i]);
        endZ.push_back(endZ[i]+blockSize);
        ++i;
        nleft-=blockSize;
    }
    
    vector<Block> blocks;
    for(int iz=0;iz<beginZ.size();iz++){
        for(int iy=0;iy<beginY.size();iy++){
            for(int ix=0;ix<beginX.size();ix++){
                blocks.push_back(Block(beginX[ix],endX[ix],beginY[iy],endY[iy],beginZ[iz],endZ[iz]));
            }
        }
    }

    for(int is=0;is<ns;is++){
        fprintf(stderr,"shot %d\n",is);

        int nr1=souloc[5*is+3];
        int irbegin=souloc[5*is+4];
        
        int *recIndex=new int[nr1];
        #pragma omp parallel for num_threads(16)
        for(int ir=0;ir<nr1;ir++){
            int ir1=irbegin+ir;
            int recIndexX=(recloc[3*ir1]-ox)/dx;
            int recIndexY=(recloc[3*ir1+1]-oy)/dy;
            int recIndexZ=(recloc[3*ir1+2]-oz)/dz;
            recIndex[ir]=recIndexX+recIndexY*nx+recIndexZ*nxy;
        }    
        
        float *ddata=new float[nnt*nr1]();
        float *sigmaX=new float[nt]();
        float *sigmaZ=new float[nt]();
        float *sigmaXa=new float[nt]();
        float *sigmaZa=new float[nt]();

	    int souIndexX=(souloc[5*is]-ox)/dx;
	    int souIndexY=(souloc[5*is+1]-oy)/dy;
	    int souIndexZ=(souloc[5*is+2]-oz)/dz;
	    int souIndex=souIndexX+souIndexY*nx+souIndexZ*nxy;

        memset(curSigmaX,0,nxyz*sizeof(float));
        memset(prevSigmaX,0,nxyz*sizeof(float));
        memset(curSigmaZ,0,nxyz*sizeof(float));
        memset(prevSigmaZ,0,nxyz*sizeof(float));

        memset(curDSigmaX,0,nxyz*sizeof(float));
        memset(prevDSigmaX,0,nxyz*sizeof(float));
        memset(curDSigmaZ,0,nxyz*sizeof(float));
        memset(prevDSigmaZ,0,nxyz*sizeof(float));

        float s=wavelet[0]*dt2;
        curSigmaX[souIndex]+=s;
        curSigmaZ[souIndex]+=s;
       
        for(int ir=0;ir<nr1;ir++){
            ddata[1+ir*nnt]=(2.*curDSigmaX[recIndex[ir]]+curDSigmaZ[recIndex[ir]])/3.; 
            sigmaX[1]=curDSigmaX[recIndex[ir]]; 
            sigmaZ[1]=curDSigmaZ[recIndex[ir]]; 
        }

        for(int it=2;it<nt;it++){
            tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),
       	                   [&](const tbb::blocked_range<int>&r){
           	    for(int ib=r.begin();ib!=r.end();++ib){
                    for(int iz=blocks[ib]._beginZ;iz<blocks[ib]._endZ;iz++){
                        for(int iy=blocks[ib]._beginY;iy<blocks[ib]._endY;iy++){
                            int i=blocks[ib]._beginX+iy*nx+iz*nxy;
       	                    bornCij(blocks[ib]._endX-blocks[ib]._beginX,nx,nxy,dx2,dy2,dz2,dt2,c11+i,c13+i,c33+i,dc11+i,dc13+i,dc33+i,prevSigmaX+i,curSigmaX+i,prevSigmaZ+i,curSigmaZ+i,prevDSigmaX+i,curDSigmaX+i,prevDSigmaZ+i,curDSigmaZ+i);
                        }                                                           
                    }                                                                        
                }                                                                            
            });                                                                     
    
            float *pt=prevSigmaX;prevSigmaX=curSigmaX;curSigmaX=pt;
            pt=prevSigmaZ;prevSigmaZ=curSigmaZ;curSigmaZ=pt;
            pt=prevDSigmaX;prevDSigmaX=curDSigmaX;curDSigmaX=pt;
            pt=prevDSigmaZ;prevDSigmaZ=curDSigmaZ;curDSigmaZ=pt;
            
            s=wavelet[it-1]*dt2;
            curSigmaX[souIndex]+=s;
            curSigmaZ[souIndex]+=s;
            
//            abc(prevSigmaX,damping,nx,ny,nz,npad);
//            abc(prevSigmaZ,damping,nx,ny,nz,npad);
//            abc(curSigmaX,damping,nx,ny,nz,npad);
//            abc(curSigmaZ,damping,nx,ny,nz,npad);
//    
//            abc(prevDSigmaX,damping,nx,ny,nz,npad);
//            abc(prevDSigmaZ,damping,nx,ny,nz,npad);
//            abc(curDSigmaX,damping,nx,ny,nz,npad);
//            abc(curDSigmaZ,damping,nx,ny,nz,npad);
    
            if(it%samplingTimeStep==0){
                int it1=it/samplingTimeStep;
                for(int ir=0;ir<nr1;ir++){
                    ddata[it1+ir*nnt]=(2.*curDSigmaX[recIndex[ir]]+curDSigmaZ[recIndex[ir]])/3.; 
                sigmaX[it1]=curDSigmaX[recIndex[ir]]; 
                sigmaZ[it1]=curDSigmaZ[recIndex[ir]]; 
                }
                #pragma omp parallel for num_threads(16)
                for(int iz=0;iz<nz;iz++){
                    memcpy(dwavefield+iz*nx+it1*nx*nz,curDSigmaX+ny/2*nx+iz*nxy,nx*sizeof(float));
                }
            }
        }

//        write("ddata",ddata,nnt*nr1,ios_base::app);
//        write("dwavefield",dwavefield,nx*nz*nnt,ios_base::app);
        
        memset(dwavefield,0,nx*nz*nnt*sizeof(float));
        
//        putBoundary(randomboundary,v,nx,ny,nz,npad);

        float *pt=prevSigmaX;prevSigmaX=curSigmaX;curSigmaX=pt;
        pt=prevSigmaZ;prevSigmaZ=curSigmaZ;curSigmaZ=pt;

        memset(curSigmaXa,0,nxyz*sizeof(float));
        memset(prevSigmaXa,0,nxyz*sizeof(float));
        memset(curSigmaZa,0,nxyz*sizeof(float));
        memset(prevSigmaZa,0,nxyz*sizeof(float));

        #pragma omp parallel for num_threads(16)
        for(int ir=0;ir<nr1;ir++){
            float temp=dt2*rwavelet[nt-1];
            curSigmaXa[recIndex[ir]]+=2./3.*temp;
            curSigmaZa[recIndex[ir]]+=1./3.*temp;
//            curSigmaXa[recIndex[ir]]+=temp;
//            curSigmaZa[recIndex[ir]]+=temp;
        }

        sigmaXa[nt-2]=curSigmaXa[souIndex];
        sigmaZa[nt-2]=curSigmaZa[souIndex];

        for(int it=2;it<nt;it++){
            tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),
       	                   [&](const tbb::blocked_range<int>&r){
           	    for(int ib=r.begin();ib!=r.end();++ib){
                    for(int iz=blocks[ib]._beginZ;iz<blocks[ib]._endZ;iz++){
                        for(int iy=blocks[ib]._beginY;iy<blocks[ib]._endY;iy++){
                            int i=blocks[ib]._beginX+iy*nx+iz*nxy;
       	                    gradientCij(gc11+i,gc13+i,gc33+i,blocks[ib]._endX-blocks[ib]._beginX,nx,nxy,dx2,dy2,dz2,dt2,v+i,eps+i,del+i,prevSigmaX+i,curSigmaX+i,prevSigmaZ+i,curSigmaZ+i,curSigmaXa+i,curSigmaZa+i);
       	                    adjoint(blocks[ib]._endX-blocks[ib]._beginX,nx,nxy,dx2,dy2,dz2,dt2,v+i,eps+i,del+i,prevSigmaXa+i,curSigmaXa+i,prevSigmaZa+i,curSigmaZa+i,cc);
                        }                                                           
                    }                                                                        
                }                                                                            
            });                                                                     

            float *pt=prevSigmaX;prevSigmaX=curSigmaX;curSigmaX=pt;
            pt=prevSigmaZ;prevSigmaZ=curSigmaZ;curSigmaZ=pt;
            pt=prevSigmaXa;prevSigmaXa=curSigmaXa;curSigmaXa=pt;
            pt=prevSigmaZa;prevSigmaZa=curSigmaZa;curSigmaZa=pt;
    
            int timeIndex=nt-it;
            
            s=wavelet[timeIndex]*dt2;
            curSigmaX[souIndex]+=s;
            curSigmaZ[souIndex]+=s;

            #pragma omp parallel for num_threads(16)
            for(int ir=0;ir<nr1;ir++){
                float temp=dt2*rwavelet[timeIndex];
                curSigmaXa[recIndex[ir]]+=2./3.*temp;
                curSigmaZa[recIndex[ir]]+=1./3.*temp;
//                curSigmaXa[recIndex[ir]]+=temp;
//                curSigmaZa[recIndex[ir]]+=temp;
            }
            
            sigmaXa[nt-it-1]=curSigmaXa[souIndex];
            sigmaZa[nt-it-1]=curSigmaZa[souIndex];
//            abc(prevSigmaXa,damping,nx,ny,nz,npad);
//            abc(prevSigmaZa,damping,nx,ny,nz,npad);
//            abc(curSigmaXa,damping,nx,ny,nz,npad);
//            abc(curSigmaZa,damping,nx,ny,nz,npad);
                #pragma omp parallel for num_threads(16)
                for(int iz=0;iz<nz;iz++){
                    memcpy(dwavefield+iz*nx+it*nx*nz,curSigmaX+ny/2*nx+iz*nxy,nx*sizeof(float));
                }
        }
        
//        write("backwardSouWavefield",dwavefield,nx*nz*nnt,ios_base::app);

        fprintf(stderr,"dot test 2\n");
        double Axy=0,xAty=0;
        for(int it=0;it<nt;it++){
            Axy+=ddata[it]*rwavelet[it];
//            Axy+=sigmaX[it]*rwavelet[it]+sigmaZ[it]*rwavelet[it];
//            xAty+=wavelet[it]*sigmaXa[it]+wavelet[it]*sigmaZa[it];
        }

        fprintf(stderr,"Axy is %10.16f\n",Axy);
//        fprintf(stderr,"xAty is %10.16f\n",xAty);

        xAty=0;
        for(int iz=locz;iz<locz+2;iz++){
            for(int iy=locy;iy<locy+2;iy++){
                for(int ix=locx;ix<locx+2;ix++){
                    int i=ix+iy*nx+iz*nxy;
//                    xAty+=gc11[i]*dc11[i];
//                    xAty+=gc13[i]*dc13[i];
                    xAty+=gc33[i]*dc33[i];
                }
            }
        }

        fprintf(stderr,"xAty is %10.16f\n",xAty);

        delete []recIndex;
        delete []ddata;
        delete []sigmaX;
        delete []sigmaZ;
        delete []sigmaXa;
        delete []sigmaZa;
    }

//    to_header("ddata","n1",nnt,"o1",ot,"d1",samplingRate);
//    to_header("ddata","n2",nr,"o2",0.,"d2",1);
//   
//    to_header("dwavefield","n1",nx,"o1",ox,"d1",dx);
//    to_header("dwavefield","n2",nz,"o2",oz,"d2",dz);
//    to_header("dwavefield","n3",nnt,"o3",ot,"d3",samplingRate);
//
//    to_header("backwardSouWavefield","n1",nx,"o1",ox,"d1",dx);
//    to_header("backwardSouWavefield","n2",nz,"o2",oz,"d2",dz);
//    to_header("backwardSouWavefield","n3",nnt,"o3",ot,"d3",samplingRate);

//    write("gc11",gc11,nxyz);
//    to_header("gc11","n1",nx,"o1",ox,"d1",dx);
//    to_header("gc11","n2",ny,"o2",oy,"d2",dy);
//    to_header("gc11","n3",nz,"o3",oz,"d3",dz);
//
//    write("gc13",gc13,nxyz);
//    to_header("gc13","n1",nx,"o1",ox,"d1",dx);
//    to_header("gc13","n2",ny,"o2",oy,"d2",dy);
//    to_header("gc13","n3",nz,"o3",oz,"d3",dz);
//
//    write("gc33",gc33,nxyz);
//    to_header("gc33","n1",nx,"o1",ox,"d1",dx);
//    to_header("gc33","n2",ny,"o2",oy,"d2",dy);
//    to_header("gc33","n3",nz,"o3",oz,"d3",dz);

    delete []wavelet;delete []rwavelet;delete []damping;
    delete []prevSigmaX;delete []curSigmaX;
    delete []prevSigmaZ;delete []curSigmaZ;
    delete []prevDSigmaX;delete []curDSigmaX;
    delete []prevDSigmaZ;delete []curDSigmaZ;
    delete []prevSigmaXa;delete []curSigmaXa;
    delete []prevSigmaZa;delete []curSigmaZa;
    delete []v;delete []eps;delete []del;
    delete []dc11;delete []dc13;delete []dc33;
    delete []c11;delete []c13;delete []c33;
    delete []dwavefield;
    
    myio_close();
    return 0;
}

