from seppyio import *

ngpu,nupdate=get_param("ngpu","nupdate")
nb,nt=get_param("nb","nt")
pipelen=ngpu*(nupdate+3)+3
nround=(nt-2)/(ngpu*nupdate)
roundlen=max(pipelen,nb)
nroundlen=nround*roundlen;
nk=(nround-1)*roundlen+pipelen+nb-1
print "pipelen",pipelen,"roundlen=",roundlen,"nk=",nk

for k in range(nk):
    print "iteration k=",k
    if k<nroundlen:
        ib=k%roundlen
        if ib<nb:
            print "transfer in block",ib,"from pageable to locked"
    if k>0 and k<=nroundlen:
        ib=(k-1)%roundlen
        if ib<nb:
            print "transfer in block",ib,"from locked to gpu"
    for gpu in range(ngpu):
        kgpu=k-gpu*(nupdate+3)
        if kgpu>2 and kgpu<=nupdate+1+nroundlen:
            for i in range(nupdate):
                ib=(kgpu-3-i)%roundlen
                iround=(kgpu-3-i)/roundlen
                if ib<nb and iround>=0 and iround<nround:
                    it=iround*ngpu*nupdate+gpu*nupdate+2+i
                    print "gpu",gpu,"kgpu",kgpu,"round",iround,"i",i,"updates block",ib,"to time",it
        if kgpu>nupdate+3 and kgpu<=nupdate+3+nroundlen:
            ib=(kgpu-nupdate-4)%roundlen
            if ib<nb:
                if ngpu>1 and gpu<ngpu-1:
                    print "gpu",gpu,"kgpu",kgpu,"transfer block",ib,"from gpu",gpu,"to gpu",gpu+1
                else:
                    print "gpu",gpu,"kgpu",kgpu,"transfer out block",ib,"from gpu",gpu,"to locked"
    if k>pipelen-2 and k<=pipelen-2+nroundlen:
        ib=(k-pipelen+1)%roundlen
        if ib<nb:
            print "transfer out block",ib,"from locked to pageable"

    print "\n"
