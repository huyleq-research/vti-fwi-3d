#include <iostream>
#include <chrono>

using namespace std;

int main(int argc,char **argv){
	cudaSetDevice(0);

	long long n=256e6;
	float *h_buff=new float[n]();
	
	float *d_buff;
	cudaMalloc(&d_buff,n*sizeof(float));
	
	int m=10;
	
	chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
	
	for(int i=0;i<m;i++){
		cudaMemcpy(d_buff,h_buff,n*sizeof(float),cudaMemcpyHostToDevice);
	}
	
	cudaDeviceSynchronize();

	chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
	chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
	cout<<"total time "<<time.count()<<" seconds"<<endl;
	cout<<"speed "<<10./time.count()<<" GB/s"<<endl;
	
	delete []h_buff;
	cudaFree(d_buff);

	return 0;
}
