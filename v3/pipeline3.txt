pipelen 19 roundlen= 19 nk= 54
iteration k= 0
transfer in block 0 from pageable to locked


iteration k= 1
transfer in block 1 from pageable to locked
transfer in block 0 from locked to gpu


iteration k= 2
transfer in block 2 from pageable to locked
transfer in block 1 from locked to gpu


iteration k= 3
transfer in block 3 from pageable to locked
transfer in block 2 from locked to gpu
gpu 0 kgpu 3 round 0 i 0 updates block 0 to time 2


iteration k= 4
transfer in block 4 from pageable to locked
transfer in block 3 from locked to gpu
gpu 0 kgpu 4 round 0 i 0 updates block 1 to time 2
gpu 0 kgpu 4 round 0 i 1 updates block 0 to time 3


iteration k= 5
transfer in block 5 from pageable to locked
transfer in block 4 from locked to gpu
gpu 0 kgpu 5 round 0 i 0 updates block 2 to time 2
gpu 0 kgpu 5 round 0 i 1 updates block 1 to time 3
gpu 0 kgpu 5 round 0 i 2 updates block 0 to time 4


iteration k= 6
transfer in block 6 from pageable to locked
transfer in block 5 from locked to gpu
gpu 0 kgpu 6 round 0 i 0 updates block 3 to time 2
gpu 0 kgpu 6 round 0 i 1 updates block 2 to time 3
gpu 0 kgpu 6 round 0 i 2 updates block 1 to time 4
gpu 0 kgpu 6 round 0 i 3 updates block 0 to time 5


iteration k= 7
transfer in block 7 from pageable to locked
transfer in block 6 from locked to gpu
gpu 0 kgpu 7 round 0 i 0 updates block 4 to time 2
gpu 0 kgpu 7 round 0 i 1 updates block 3 to time 3
gpu 0 kgpu 7 round 0 i 2 updates block 2 to time 4
gpu 0 kgpu 7 round 0 i 3 updates block 1 to time 5
gpu 0 kgpu 7 round 0 i 4 updates block 0 to time 6


iteration k= 8
transfer in block 8 from pageable to locked
transfer in block 7 from locked to gpu
gpu 0 kgpu 8 round 0 i 0 updates block 5 to time 2
gpu 0 kgpu 8 round 0 i 1 updates block 4 to time 3
gpu 0 kgpu 8 round 0 i 2 updates block 3 to time 4
gpu 0 kgpu 8 round 0 i 3 updates block 2 to time 5
gpu 0 kgpu 8 round 0 i 4 updates block 1 to time 6


iteration k= 9
transfer in block 9 from pageable to locked
transfer in block 8 from locked to gpu
gpu 0 kgpu 9 round 0 i 0 updates block 6 to time 2
gpu 0 kgpu 9 round 0 i 1 updates block 5 to time 3
gpu 0 kgpu 9 round 0 i 2 updates block 4 to time 4
gpu 0 kgpu 9 round 0 i 3 updates block 3 to time 5
gpu 0 kgpu 9 round 0 i 4 updates block 2 to time 6
gpu 0 kgpu 9 transfer block 0 from gpu 0 to gpu 1


iteration k= 10
transfer in block 10 from pageable to locked
transfer in block 9 from locked to gpu
gpu 0 kgpu 10 round 0 i 0 updates block 7 to time 2
gpu 0 kgpu 10 round 0 i 1 updates block 6 to time 3
gpu 0 kgpu 10 round 0 i 2 updates block 5 to time 4
gpu 0 kgpu 10 round 0 i 3 updates block 4 to time 5
gpu 0 kgpu 10 round 0 i 4 updates block 3 to time 6
gpu 0 kgpu 10 transfer block 1 from gpu 0 to gpu 1


iteration k= 11
transfer in block 11 from pageable to locked
transfer in block 10 from locked to gpu
gpu 0 kgpu 11 round 0 i 0 updates block 8 to time 2
gpu 0 kgpu 11 round 0 i 1 updates block 7 to time 3
gpu 0 kgpu 11 round 0 i 2 updates block 6 to time 4
gpu 0 kgpu 11 round 0 i 3 updates block 5 to time 5
gpu 0 kgpu 11 round 0 i 4 updates block 4 to time 6
gpu 0 kgpu 11 transfer block 2 from gpu 0 to gpu 1
gpu 1 kgpu 3 round 0 i 0 updates block 0 to time 7


iteration k= 12
transfer in block 12 from pageable to locked
transfer in block 11 from locked to gpu
gpu 0 kgpu 12 round 0 i 0 updates block 9 to time 2
gpu 0 kgpu 12 round 0 i 1 updates block 8 to time 3
gpu 0 kgpu 12 round 0 i 2 updates block 7 to time 4
gpu 0 kgpu 12 round 0 i 3 updates block 6 to time 5
gpu 0 kgpu 12 round 0 i 4 updates block 5 to time 6
gpu 0 kgpu 12 transfer block 3 from gpu 0 to gpu 1
gpu 1 kgpu 4 round 0 i 0 updates block 1 to time 7
gpu 1 kgpu 4 round 0 i 1 updates block 0 to time 8


iteration k= 13
transfer in block 13 from pageable to locked
transfer in block 12 from locked to gpu
gpu 0 kgpu 13 round 0 i 0 updates block 10 to time 2
gpu 0 kgpu 13 round 0 i 1 updates block 9 to time 3
gpu 0 kgpu 13 round 0 i 2 updates block 8 to time 4
gpu 0 kgpu 13 round 0 i 3 updates block 7 to time 5
gpu 0 kgpu 13 round 0 i 4 updates block 6 to time 6
gpu 0 kgpu 13 transfer block 4 from gpu 0 to gpu 1
gpu 1 kgpu 5 round 0 i 0 updates block 2 to time 7
gpu 1 kgpu 5 round 0 i 1 updates block 1 to time 8
gpu 1 kgpu 5 round 0 i 2 updates block 0 to time 9


iteration k= 14
transfer in block 14 from pageable to locked
transfer in block 13 from locked to gpu
gpu 0 kgpu 14 round 0 i 0 updates block 11 to time 2
gpu 0 kgpu 14 round 0 i 1 updates block 10 to time 3
gpu 0 kgpu 14 round 0 i 2 updates block 9 to time 4
gpu 0 kgpu 14 round 0 i 3 updates block 8 to time 5
gpu 0 kgpu 14 round 0 i 4 updates block 7 to time 6
gpu 0 kgpu 14 transfer block 5 from gpu 0 to gpu 1
gpu 1 kgpu 6 round 0 i 0 updates block 3 to time 7
gpu 1 kgpu 6 round 0 i 1 updates block 2 to time 8
gpu 1 kgpu 6 round 0 i 2 updates block 1 to time 9
gpu 1 kgpu 6 round 0 i 3 updates block 0 to time 10


iteration k= 15
transfer in block 15 from pageable to locked
transfer in block 14 from locked to gpu
gpu 0 kgpu 15 round 0 i 0 updates block 12 to time 2
gpu 0 kgpu 15 round 0 i 1 updates block 11 to time 3
gpu 0 kgpu 15 round 0 i 2 updates block 10 to time 4
gpu 0 kgpu 15 round 0 i 3 updates block 9 to time 5
gpu 0 kgpu 15 round 0 i 4 updates block 8 to time 6
gpu 0 kgpu 15 transfer block 6 from gpu 0 to gpu 1
gpu 1 kgpu 7 round 0 i 0 updates block 4 to time 7
gpu 1 kgpu 7 round 0 i 1 updates block 3 to time 8
gpu 1 kgpu 7 round 0 i 2 updates block 2 to time 9
gpu 1 kgpu 7 round 0 i 3 updates block 1 to time 10
gpu 1 kgpu 7 round 0 i 4 updates block 0 to time 11


iteration k= 16
transfer in block 16 from pageable to locked
transfer in block 15 from locked to gpu
gpu 0 kgpu 16 round 0 i 0 updates block 13 to time 2
gpu 0 kgpu 16 round 0 i 1 updates block 12 to time 3
gpu 0 kgpu 16 round 0 i 2 updates block 11 to time 4
gpu 0 kgpu 16 round 0 i 3 updates block 10 to time 5
gpu 0 kgpu 16 round 0 i 4 updates block 9 to time 6
gpu 0 kgpu 16 transfer block 7 from gpu 0 to gpu 1
gpu 1 kgpu 8 round 0 i 0 updates block 5 to time 7
gpu 1 kgpu 8 round 0 i 1 updates block 4 to time 8
gpu 1 kgpu 8 round 0 i 2 updates block 3 to time 9
gpu 1 kgpu 8 round 0 i 3 updates block 2 to time 10
gpu 1 kgpu 8 round 0 i 4 updates block 1 to time 11


iteration k= 17
transfer in block 16 from locked to gpu
gpu 0 kgpu 17 round 0 i 0 updates block 14 to time 2
gpu 0 kgpu 17 round 0 i 1 updates block 13 to time 3
gpu 0 kgpu 17 round 0 i 2 updates block 12 to time 4
gpu 0 kgpu 17 round 0 i 3 updates block 11 to time 5
gpu 0 kgpu 17 round 0 i 4 updates block 10 to time 6
gpu 0 kgpu 17 transfer block 8 from gpu 0 to gpu 1
gpu 1 kgpu 9 round 0 i 0 updates block 6 to time 7
gpu 1 kgpu 9 round 0 i 1 updates block 5 to time 8
gpu 1 kgpu 9 round 0 i 2 updates block 4 to time 9
gpu 1 kgpu 9 round 0 i 3 updates block 3 to time 10
gpu 1 kgpu 9 round 0 i 4 updates block 2 to time 11
gpu 1 kgpu 9 transfer out block 0 from gpu 1 to locked


iteration k= 18
gpu 0 kgpu 18 round 0 i 0 updates block 15 to time 2
gpu 0 kgpu 18 round 0 i 1 updates block 14 to time 3
gpu 0 kgpu 18 round 0 i 2 updates block 13 to time 4
gpu 0 kgpu 18 round 0 i 3 updates block 12 to time 5
gpu 0 kgpu 18 round 0 i 4 updates block 11 to time 6
gpu 0 kgpu 18 transfer block 9 from gpu 0 to gpu 1
gpu 1 kgpu 10 round 0 i 0 updates block 7 to time 7
gpu 1 kgpu 10 round 0 i 1 updates block 6 to time 8
gpu 1 kgpu 10 round 0 i 2 updates block 5 to time 9
gpu 1 kgpu 10 round 0 i 3 updates block 4 to time 10
gpu 1 kgpu 10 round 0 i 4 updates block 3 to time 11
gpu 1 kgpu 10 transfer out block 1 from gpu 1 to locked
transfer out block 0 from locked to pageable


iteration k= 19
transfer in block 0 from pageable to locked
gpu 0 kgpu 19 round 0 i 0 updates block 16 to time 2
gpu 0 kgpu 19 round 0 i 1 updates block 15 to time 3
gpu 0 kgpu 19 round 0 i 2 updates block 14 to time 4
gpu 0 kgpu 19 round 0 i 3 updates block 13 to time 5
gpu 0 kgpu 19 round 0 i 4 updates block 12 to time 6
gpu 0 kgpu 19 transfer block 10 from gpu 0 to gpu 1
gpu 1 kgpu 11 round 0 i 0 updates block 8 to time 7
gpu 1 kgpu 11 round 0 i 1 updates block 7 to time 8
gpu 1 kgpu 11 round 0 i 2 updates block 6 to time 9
gpu 1 kgpu 11 round 0 i 3 updates block 5 to time 10
gpu 1 kgpu 11 round 0 i 4 updates block 4 to time 11
gpu 1 kgpu 11 transfer out block 2 from gpu 1 to locked
transfer out block 1 from locked to pageable


iteration k= 20
transfer in block 1 from pageable to locked
transfer in block 0 from locked to gpu
gpu 0 kgpu 20 round 0 i 1 updates block 16 to time 3
gpu 0 kgpu 20 round 0 i 2 updates block 15 to time 4
gpu 0 kgpu 20 round 0 i 3 updates block 14 to time 5
gpu 0 kgpu 20 round 0 i 4 updates block 13 to time 6
gpu 0 kgpu 20 transfer block 11 from gpu 0 to gpu 1
gpu 1 kgpu 12 round 0 i 0 updates block 9 to time 7
gpu 1 kgpu 12 round 0 i 1 updates block 8 to time 8
gpu 1 kgpu 12 round 0 i 2 updates block 7 to time 9
gpu 1 kgpu 12 round 0 i 3 updates block 6 to time 10
gpu 1 kgpu 12 round 0 i 4 updates block 5 to time 11
gpu 1 kgpu 12 transfer out block 3 from gpu 1 to locked
transfer out block 2 from locked to pageable


iteration k= 21
transfer in block 2 from pageable to locked
transfer in block 1 from locked to gpu
gpu 0 kgpu 21 round 0 i 2 updates block 16 to time 4
gpu 0 kgpu 21 round 0 i 3 updates block 15 to time 5
gpu 0 kgpu 21 round 0 i 4 updates block 14 to time 6
gpu 0 kgpu 21 transfer block 12 from gpu 0 to gpu 1
gpu 1 kgpu 13 round 0 i 0 updates block 10 to time 7
gpu 1 kgpu 13 round 0 i 1 updates block 9 to time 8
gpu 1 kgpu 13 round 0 i 2 updates block 8 to time 9
gpu 1 kgpu 13 round 0 i 3 updates block 7 to time 10
gpu 1 kgpu 13 round 0 i 4 updates block 6 to time 11
gpu 1 kgpu 13 transfer out block 4 from gpu 1 to locked
transfer out block 3 from locked to pageable


iteration k= 22
transfer in block 3 from pageable to locked
transfer in block 2 from locked to gpu
gpu 0 kgpu 22 round 1 i 0 updates block 0 to time 12
gpu 0 kgpu 22 round 0 i 3 updates block 16 to time 5
gpu 0 kgpu 22 round 0 i 4 updates block 15 to time 6
gpu 0 kgpu 22 transfer block 13 from gpu 0 to gpu 1
gpu 1 kgpu 14 round 0 i 0 updates block 11 to time 7
gpu 1 kgpu 14 round 0 i 1 updates block 10 to time 8
gpu 1 kgpu 14 round 0 i 2 updates block 9 to time 9
gpu 1 kgpu 14 round 0 i 3 updates block 8 to time 10
gpu 1 kgpu 14 round 0 i 4 updates block 7 to time 11
gpu 1 kgpu 14 transfer out block 5 from gpu 1 to locked
transfer out block 4 from locked to pageable


iteration k= 23
transfer in block 4 from pageable to locked
transfer in block 3 from locked to gpu
gpu 0 kgpu 23 round 1 i 0 updates block 1 to time 12
gpu 0 kgpu 23 round 1 i 1 updates block 0 to time 13
gpu 0 kgpu 23 round 0 i 4 updates block 16 to time 6
gpu 0 kgpu 23 transfer block 14 from gpu 0 to gpu 1
gpu 1 kgpu 15 round 0 i 0 updates block 12 to time 7
gpu 1 kgpu 15 round 0 i 1 updates block 11 to time 8
gpu 1 kgpu 15 round 0 i 2 updates block 10 to time 9
gpu 1 kgpu 15 round 0 i 3 updates block 9 to time 10
gpu 1 kgpu 15 round 0 i 4 updates block 8 to time 11
gpu 1 kgpu 15 transfer out block 6 from gpu 1 to locked
transfer out block 5 from locked to pageable


iteration k= 24
transfer in block 5 from pageable to locked
transfer in block 4 from locked to gpu
gpu 0 kgpu 24 round 1 i 0 updates block 2 to time 12
gpu 0 kgpu 24 round 1 i 1 updates block 1 to time 13
gpu 0 kgpu 24 round 1 i 2 updates block 0 to time 14
gpu 0 kgpu 24 transfer block 15 from gpu 0 to gpu 1
gpu 1 kgpu 16 round 0 i 0 updates block 13 to time 7
gpu 1 kgpu 16 round 0 i 1 updates block 12 to time 8
gpu 1 kgpu 16 round 0 i 2 updates block 11 to time 9
gpu 1 kgpu 16 round 0 i 3 updates block 10 to time 10
gpu 1 kgpu 16 round 0 i 4 updates block 9 to time 11
gpu 1 kgpu 16 transfer out block 7 from gpu 1 to locked
transfer out block 6 from locked to pageable


iteration k= 25
transfer in block 6 from pageable to locked
transfer in block 5 from locked to gpu
gpu 0 kgpu 25 round 1 i 0 updates block 3 to time 12
gpu 0 kgpu 25 round 1 i 1 updates block 2 to time 13
gpu 0 kgpu 25 round 1 i 2 updates block 1 to time 14
gpu 0 kgpu 25 round 1 i 3 updates block 0 to time 15
gpu 0 kgpu 25 transfer block 16 from gpu 0 to gpu 1
gpu 1 kgpu 17 round 0 i 0 updates block 14 to time 7
gpu 1 kgpu 17 round 0 i 1 updates block 13 to time 8
gpu 1 kgpu 17 round 0 i 2 updates block 12 to time 9
gpu 1 kgpu 17 round 0 i 3 updates block 11 to time 10
gpu 1 kgpu 17 round 0 i 4 updates block 10 to time 11
gpu 1 kgpu 17 transfer out block 8 from gpu 1 to locked
transfer out block 7 from locked to pageable


iteration k= 26
transfer in block 7 from pageable to locked
transfer in block 6 from locked to gpu
gpu 0 kgpu 26 round 1 i 0 updates block 4 to time 12
gpu 0 kgpu 26 round 1 i 1 updates block 3 to time 13
gpu 0 kgpu 26 round 1 i 2 updates block 2 to time 14
gpu 0 kgpu 26 round 1 i 3 updates block 1 to time 15
gpu 0 kgpu 26 round 1 i 4 updates block 0 to time 16
gpu 1 kgpu 18 round 0 i 0 updates block 15 to time 7
gpu 1 kgpu 18 round 0 i 1 updates block 14 to time 8
gpu 1 kgpu 18 round 0 i 2 updates block 13 to time 9
gpu 1 kgpu 18 round 0 i 3 updates block 12 to time 10
gpu 1 kgpu 18 round 0 i 4 updates block 11 to time 11
gpu 1 kgpu 18 transfer out block 9 from gpu 1 to locked
transfer out block 8 from locked to pageable


iteration k= 27
transfer in block 8 from pageable to locked
transfer in block 7 from locked to gpu
gpu 0 kgpu 27 round 1 i 0 updates block 5 to time 12
gpu 0 kgpu 27 round 1 i 1 updates block 4 to time 13
gpu 0 kgpu 27 round 1 i 2 updates block 3 to time 14
gpu 0 kgpu 27 round 1 i 3 updates block 2 to time 15
gpu 0 kgpu 27 round 1 i 4 updates block 1 to time 16
gpu 1 kgpu 19 round 0 i 0 updates block 16 to time 7
gpu 1 kgpu 19 round 0 i 1 updates block 15 to time 8
gpu 1 kgpu 19 round 0 i 2 updates block 14 to time 9
gpu 1 kgpu 19 round 0 i 3 updates block 13 to time 10
gpu 1 kgpu 19 round 0 i 4 updates block 12 to time 11
gpu 1 kgpu 19 transfer out block 10 from gpu 1 to locked
transfer out block 9 from locked to pageable


iteration k= 28
transfer in block 9 from pageable to locked
transfer in block 8 from locked to gpu
gpu 0 kgpu 28 round 1 i 0 updates block 6 to time 12
gpu 0 kgpu 28 round 1 i 1 updates block 5 to time 13
gpu 0 kgpu 28 round 1 i 2 updates block 4 to time 14
gpu 0 kgpu 28 round 1 i 3 updates block 3 to time 15
gpu 0 kgpu 28 round 1 i 4 updates block 2 to time 16
gpu 0 kgpu 28 transfer block 0 from gpu 0 to gpu 1
gpu 1 kgpu 20 round 0 i 1 updates block 16 to time 8
gpu 1 kgpu 20 round 0 i 2 updates block 15 to time 9
gpu 1 kgpu 20 round 0 i 3 updates block 14 to time 10
gpu 1 kgpu 20 round 0 i 4 updates block 13 to time 11
gpu 1 kgpu 20 transfer out block 11 from gpu 1 to locked
transfer out block 10 from locked to pageable


iteration k= 29
transfer in block 10 from pageable to locked
transfer in block 9 from locked to gpu
gpu 0 kgpu 29 round 1 i 0 updates block 7 to time 12
gpu 0 kgpu 29 round 1 i 1 updates block 6 to time 13
gpu 0 kgpu 29 round 1 i 2 updates block 5 to time 14
gpu 0 kgpu 29 round 1 i 3 updates block 4 to time 15
gpu 0 kgpu 29 round 1 i 4 updates block 3 to time 16
gpu 0 kgpu 29 transfer block 1 from gpu 0 to gpu 1
gpu 1 kgpu 21 round 0 i 2 updates block 16 to time 9
gpu 1 kgpu 21 round 0 i 3 updates block 15 to time 10
gpu 1 kgpu 21 round 0 i 4 updates block 14 to time 11
gpu 1 kgpu 21 transfer out block 12 from gpu 1 to locked
transfer out block 11 from locked to pageable


iteration k= 30
transfer in block 11 from pageable to locked
transfer in block 10 from locked to gpu
gpu 0 kgpu 30 round 1 i 0 updates block 8 to time 12
gpu 0 kgpu 30 round 1 i 1 updates block 7 to time 13
gpu 0 kgpu 30 round 1 i 2 updates block 6 to time 14
gpu 0 kgpu 30 round 1 i 3 updates block 5 to time 15
gpu 0 kgpu 30 round 1 i 4 updates block 4 to time 16
gpu 0 kgpu 30 transfer block 2 from gpu 0 to gpu 1
gpu 1 kgpu 22 round 1 i 0 updates block 0 to time 17
gpu 1 kgpu 22 round 0 i 3 updates block 16 to time 10
gpu 1 kgpu 22 round 0 i 4 updates block 15 to time 11
gpu 1 kgpu 22 transfer out block 13 from gpu 1 to locked
transfer out block 12 from locked to pageable


iteration k= 31
transfer in block 12 from pageable to locked
transfer in block 11 from locked to gpu
gpu 0 kgpu 31 round 1 i 0 updates block 9 to time 12
gpu 0 kgpu 31 round 1 i 1 updates block 8 to time 13
gpu 0 kgpu 31 round 1 i 2 updates block 7 to time 14
gpu 0 kgpu 31 round 1 i 3 updates block 6 to time 15
gpu 0 kgpu 31 round 1 i 4 updates block 5 to time 16
gpu 0 kgpu 31 transfer block 3 from gpu 0 to gpu 1
gpu 1 kgpu 23 round 1 i 0 updates block 1 to time 17
gpu 1 kgpu 23 round 1 i 1 updates block 0 to time 18
gpu 1 kgpu 23 round 0 i 4 updates block 16 to time 11
gpu 1 kgpu 23 transfer out block 14 from gpu 1 to locked
transfer out block 13 from locked to pageable


iteration k= 32
transfer in block 13 from pageable to locked
transfer in block 12 from locked to gpu
gpu 0 kgpu 32 round 1 i 0 updates block 10 to time 12
gpu 0 kgpu 32 round 1 i 1 updates block 9 to time 13
gpu 0 kgpu 32 round 1 i 2 updates block 8 to time 14
gpu 0 kgpu 32 round 1 i 3 updates block 7 to time 15
gpu 0 kgpu 32 round 1 i 4 updates block 6 to time 16
gpu 0 kgpu 32 transfer block 4 from gpu 0 to gpu 1
gpu 1 kgpu 24 round 1 i 0 updates block 2 to time 17
gpu 1 kgpu 24 round 1 i 1 updates block 1 to time 18
gpu 1 kgpu 24 round 1 i 2 updates block 0 to time 19
gpu 1 kgpu 24 transfer out block 15 from gpu 1 to locked
transfer out block 14 from locked to pageable


iteration k= 33
transfer in block 14 from pageable to locked
transfer in block 13 from locked to gpu
gpu 0 kgpu 33 round 1 i 0 updates block 11 to time 12
gpu 0 kgpu 33 round 1 i 1 updates block 10 to time 13
gpu 0 kgpu 33 round 1 i 2 updates block 9 to time 14
gpu 0 kgpu 33 round 1 i 3 updates block 8 to time 15
gpu 0 kgpu 33 round 1 i 4 updates block 7 to time 16
gpu 0 kgpu 33 transfer block 5 from gpu 0 to gpu 1
gpu 1 kgpu 25 round 1 i 0 updates block 3 to time 17
gpu 1 kgpu 25 round 1 i 1 updates block 2 to time 18
gpu 1 kgpu 25 round 1 i 2 updates block 1 to time 19
gpu 1 kgpu 25 round 1 i 3 updates block 0 to time 20
gpu 1 kgpu 25 transfer out block 16 from gpu 1 to locked
transfer out block 15 from locked to pageable


iteration k= 34
transfer in block 15 from pageable to locked
transfer in block 14 from locked to gpu
gpu 0 kgpu 34 round 1 i 0 updates block 12 to time 12
gpu 0 kgpu 34 round 1 i 1 updates block 11 to time 13
gpu 0 kgpu 34 round 1 i 2 updates block 10 to time 14
gpu 0 kgpu 34 round 1 i 3 updates block 9 to time 15
gpu 0 kgpu 34 round 1 i 4 updates block 8 to time 16
gpu 0 kgpu 34 transfer block 6 from gpu 0 to gpu 1
gpu 1 kgpu 26 round 1 i 0 updates block 4 to time 17
gpu 1 kgpu 26 round 1 i 1 updates block 3 to time 18
gpu 1 kgpu 26 round 1 i 2 updates block 2 to time 19
gpu 1 kgpu 26 round 1 i 3 updates block 1 to time 20
gpu 1 kgpu 26 round 1 i 4 updates block 0 to time 21
transfer out block 16 from locked to pageable


iteration k= 35
transfer in block 16 from pageable to locked
transfer in block 15 from locked to gpu
gpu 0 kgpu 35 round 1 i 0 updates block 13 to time 12
gpu 0 kgpu 35 round 1 i 1 updates block 12 to time 13
gpu 0 kgpu 35 round 1 i 2 updates block 11 to time 14
gpu 0 kgpu 35 round 1 i 3 updates block 10 to time 15
gpu 0 kgpu 35 round 1 i 4 updates block 9 to time 16
gpu 0 kgpu 35 transfer block 7 from gpu 0 to gpu 1
gpu 1 kgpu 27 round 1 i 0 updates block 5 to time 17
gpu 1 kgpu 27 round 1 i 1 updates block 4 to time 18
gpu 1 kgpu 27 round 1 i 2 updates block 3 to time 19
gpu 1 kgpu 27 round 1 i 3 updates block 2 to time 20
gpu 1 kgpu 27 round 1 i 4 updates block 1 to time 21


iteration k= 36
transfer in block 16 from locked to gpu
gpu 0 kgpu 36 round 1 i 0 updates block 14 to time 12
gpu 0 kgpu 36 round 1 i 1 updates block 13 to time 13
gpu 0 kgpu 36 round 1 i 2 updates block 12 to time 14
gpu 0 kgpu 36 round 1 i 3 updates block 11 to time 15
gpu 0 kgpu 36 round 1 i 4 updates block 10 to time 16
gpu 0 kgpu 36 transfer block 8 from gpu 0 to gpu 1
gpu 1 kgpu 28 round 1 i 0 updates block 6 to time 17
gpu 1 kgpu 28 round 1 i 1 updates block 5 to time 18
gpu 1 kgpu 28 round 1 i 2 updates block 4 to time 19
gpu 1 kgpu 28 round 1 i 3 updates block 3 to time 20
gpu 1 kgpu 28 round 1 i 4 updates block 2 to time 21
gpu 1 kgpu 28 transfer out block 0 from gpu 1 to locked


iteration k= 37
gpu 0 kgpu 37 round 1 i 0 updates block 15 to time 12
gpu 0 kgpu 37 round 1 i 1 updates block 14 to time 13
gpu 0 kgpu 37 round 1 i 2 updates block 13 to time 14
gpu 0 kgpu 37 round 1 i 3 updates block 12 to time 15
gpu 0 kgpu 37 round 1 i 4 updates block 11 to time 16
gpu 0 kgpu 37 transfer block 9 from gpu 0 to gpu 1
gpu 1 kgpu 29 round 1 i 0 updates block 7 to time 17
gpu 1 kgpu 29 round 1 i 1 updates block 6 to time 18
gpu 1 kgpu 29 round 1 i 2 updates block 5 to time 19
gpu 1 kgpu 29 round 1 i 3 updates block 4 to time 20
gpu 1 kgpu 29 round 1 i 4 updates block 3 to time 21
gpu 1 kgpu 29 transfer out block 1 from gpu 1 to locked
transfer out block 0 from locked to pageable


iteration k= 38
gpu 0 kgpu 38 round 1 i 0 updates block 16 to time 12
gpu 0 kgpu 38 round 1 i 1 updates block 15 to time 13
gpu 0 kgpu 38 round 1 i 2 updates block 14 to time 14
gpu 0 kgpu 38 round 1 i 3 updates block 13 to time 15
gpu 0 kgpu 38 round 1 i 4 updates block 12 to time 16
gpu 0 kgpu 38 transfer block 10 from gpu 0 to gpu 1
gpu 1 kgpu 30 round 1 i 0 updates block 8 to time 17
gpu 1 kgpu 30 round 1 i 1 updates block 7 to time 18
gpu 1 kgpu 30 round 1 i 2 updates block 6 to time 19
gpu 1 kgpu 30 round 1 i 3 updates block 5 to time 20
gpu 1 kgpu 30 round 1 i 4 updates block 4 to time 21
gpu 1 kgpu 30 transfer out block 2 from gpu 1 to locked
transfer out block 1 from locked to pageable


iteration k= 39
gpu 0 kgpu 39 round 1 i 1 updates block 16 to time 13
gpu 0 kgpu 39 round 1 i 2 updates block 15 to time 14
gpu 0 kgpu 39 round 1 i 3 updates block 14 to time 15
gpu 0 kgpu 39 round 1 i 4 updates block 13 to time 16
gpu 0 kgpu 39 transfer block 11 from gpu 0 to gpu 1
gpu 1 kgpu 31 round 1 i 0 updates block 9 to time 17
gpu 1 kgpu 31 round 1 i 1 updates block 8 to time 18
gpu 1 kgpu 31 round 1 i 2 updates block 7 to time 19
gpu 1 kgpu 31 round 1 i 3 updates block 6 to time 20
gpu 1 kgpu 31 round 1 i 4 updates block 5 to time 21
gpu 1 kgpu 31 transfer out block 3 from gpu 1 to locked
transfer out block 2 from locked to pageable


iteration k= 40
gpu 0 kgpu 40 round 1 i 2 updates block 16 to time 14
gpu 0 kgpu 40 round 1 i 3 updates block 15 to time 15
gpu 0 kgpu 40 round 1 i 4 updates block 14 to time 16
gpu 0 kgpu 40 transfer block 12 from gpu 0 to gpu 1
gpu 1 kgpu 32 round 1 i 0 updates block 10 to time 17
gpu 1 kgpu 32 round 1 i 1 updates block 9 to time 18
gpu 1 kgpu 32 round 1 i 2 updates block 8 to time 19
gpu 1 kgpu 32 round 1 i 3 updates block 7 to time 20
gpu 1 kgpu 32 round 1 i 4 updates block 6 to time 21
gpu 1 kgpu 32 transfer out block 4 from gpu 1 to locked
transfer out block 3 from locked to pageable


iteration k= 41
gpu 0 kgpu 41 round 1 i 3 updates block 16 to time 15
gpu 0 kgpu 41 round 1 i 4 updates block 15 to time 16
gpu 0 kgpu 41 transfer block 13 from gpu 0 to gpu 1
gpu 1 kgpu 33 round 1 i 0 updates block 11 to time 17
gpu 1 kgpu 33 round 1 i 1 updates block 10 to time 18
gpu 1 kgpu 33 round 1 i 2 updates block 9 to time 19
gpu 1 kgpu 33 round 1 i 3 updates block 8 to time 20
gpu 1 kgpu 33 round 1 i 4 updates block 7 to time 21
gpu 1 kgpu 33 transfer out block 5 from gpu 1 to locked
transfer out block 4 from locked to pageable


iteration k= 42
gpu 0 kgpu 42 round 1 i 4 updates block 16 to time 16
gpu 0 kgpu 42 transfer block 14 from gpu 0 to gpu 1
gpu 1 kgpu 34 round 1 i 0 updates block 12 to time 17
gpu 1 kgpu 34 round 1 i 1 updates block 11 to time 18
gpu 1 kgpu 34 round 1 i 2 updates block 10 to time 19
gpu 1 kgpu 34 round 1 i 3 updates block 9 to time 20
gpu 1 kgpu 34 round 1 i 4 updates block 8 to time 21
gpu 1 kgpu 34 transfer out block 6 from gpu 1 to locked
transfer out block 5 from locked to pageable


iteration k= 43
gpu 0 kgpu 43 transfer block 15 from gpu 0 to gpu 1
gpu 1 kgpu 35 round 1 i 0 updates block 13 to time 17
gpu 1 kgpu 35 round 1 i 1 updates block 12 to time 18
gpu 1 kgpu 35 round 1 i 2 updates block 11 to time 19
gpu 1 kgpu 35 round 1 i 3 updates block 10 to time 20
gpu 1 kgpu 35 round 1 i 4 updates block 9 to time 21
gpu 1 kgpu 35 transfer out block 7 from gpu 1 to locked
transfer out block 6 from locked to pageable


iteration k= 44
gpu 0 kgpu 44 transfer block 16 from gpu 0 to gpu 1
gpu 1 kgpu 36 round 1 i 0 updates block 14 to time 17
gpu 1 kgpu 36 round 1 i 1 updates block 13 to time 18
gpu 1 kgpu 36 round 1 i 2 updates block 12 to time 19
gpu 1 kgpu 36 round 1 i 3 updates block 11 to time 20
gpu 1 kgpu 36 round 1 i 4 updates block 10 to time 21
gpu 1 kgpu 36 transfer out block 8 from gpu 1 to locked
transfer out block 7 from locked to pageable


iteration k= 45
gpu 1 kgpu 37 round 1 i 0 updates block 15 to time 17
gpu 1 kgpu 37 round 1 i 1 updates block 14 to time 18
gpu 1 kgpu 37 round 1 i 2 updates block 13 to time 19
gpu 1 kgpu 37 round 1 i 3 updates block 12 to time 20
gpu 1 kgpu 37 round 1 i 4 updates block 11 to time 21
gpu 1 kgpu 37 transfer out block 9 from gpu 1 to locked
transfer out block 8 from locked to pageable


iteration k= 46
gpu 1 kgpu 38 round 1 i 0 updates block 16 to time 17
gpu 1 kgpu 38 round 1 i 1 updates block 15 to time 18
gpu 1 kgpu 38 round 1 i 2 updates block 14 to time 19
gpu 1 kgpu 38 round 1 i 3 updates block 13 to time 20
gpu 1 kgpu 38 round 1 i 4 updates block 12 to time 21
gpu 1 kgpu 38 transfer out block 10 from gpu 1 to locked
transfer out block 9 from locked to pageable


iteration k= 47
gpu 1 kgpu 39 round 1 i 1 updates block 16 to time 18
gpu 1 kgpu 39 round 1 i 2 updates block 15 to time 19
gpu 1 kgpu 39 round 1 i 3 updates block 14 to time 20
gpu 1 kgpu 39 round 1 i 4 updates block 13 to time 21
gpu 1 kgpu 39 transfer out block 11 from gpu 1 to locked
transfer out block 10 from locked to pageable


iteration k= 48
gpu 1 kgpu 40 round 1 i 2 updates block 16 to time 19
gpu 1 kgpu 40 round 1 i 3 updates block 15 to time 20
gpu 1 kgpu 40 round 1 i 4 updates block 14 to time 21
gpu 1 kgpu 40 transfer out block 12 from gpu 1 to locked
transfer out block 11 from locked to pageable


iteration k= 49
gpu 1 kgpu 41 round 1 i 3 updates block 16 to time 20
gpu 1 kgpu 41 round 1 i 4 updates block 15 to time 21
gpu 1 kgpu 41 transfer out block 13 from gpu 1 to locked
transfer out block 12 from locked to pageable


iteration k= 50
gpu 1 kgpu 42 round 1 i 4 updates block 16 to time 21
gpu 1 kgpu 42 transfer out block 14 from gpu 1 to locked
transfer out block 13 from locked to pageable


iteration k= 51
gpu 1 kgpu 43 transfer out block 15 from gpu 1 to locked
transfer out block 14 from locked to pageable


iteration k= 52
gpu 1 kgpu 44 transfer out block 16 from gpu 1 to locked
transfer out block 15 from locked to pageable


iteration k= 53
transfer out block 16 from locked to pageable


