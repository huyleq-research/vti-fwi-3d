#ifndef LAPLACIAN3D_H
#define LAPLACIAN3D_H

#define C0 -2.84722222222
#define C1 1.6
#define C2 -0.2
#define C3 0.02539682539
#define C4 -0.00178571428

extern "C"{
void forward(int n,int nx,int nxy,float dx2,float dy2,float dz2,float dt2,float *v,float *eps,float *del,float *prevSigmaX,float *curSigmaX,float *prevSigmaZ,float *curSigmaZ);
}

extern "C"{
void gradient(float *gv,float *geps,float *gdel,int n,int nx,int nxy,float dx2,float dy2,float dz2,float dt2,float *v,float *eps,float *del,float *prevSigmaX,float *curSigmaX,float *prevSigmaZ,float *curSigmaZ,float *curSigmaXa,float *curSigmaZa);
}

extern "C"{
void adjoint(int n,int nx,int nxy,float dx2,float dy2,float dz2,float dt2,float *v,float *eps,float *del,float *prevSigmaX,float *curSigmaX,float *prevSigmaZ,float *curSigmaZ,float *cc);
}

#endif
