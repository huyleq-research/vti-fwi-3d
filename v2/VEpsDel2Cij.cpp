#include <cstdio>
#include <cstdlib>
#include <cmath>

#include "myio.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);
    
    int vepsdel2cij;
    get_param("vepsdel2cij",vepsdel2cij);

    if(vepsdel2cij){
        int nx,ny,nz;
        float ox,oy,oz,dx,dy,dz;
        
        from_header("v","n1",nx,"o1",ox,"d1",dx);
        from_header("v","n2",ny,"o2",oy,"d2",dy);
        from_header("v","n3",nz,"o3",oz,"d3",dz);
        
        long long nxy=nx*ny;
        long long nxyz=nxy*nz;
        
        float *v=new float[nxyz];
        read("v",v,nxyz);
        float *eps=new float[nxyz];
        read("eps",eps,nxyz);
        float *del=new float[nxyz];
        read("del",del,nxyz);
        
        #pragma omp parallel for num_threads(16)
        for(size_t i=0;i<nxyz;i++){
            v[i]=v[i]*v[i];
            eps[i]=v[i]*(1.+2.*eps[i]);
            del[i]=v[i]*sqrt(1.+2.*del[i]);
        }

        write("c11",eps,nxyz);
        to_header("c11","n1",nx,"o1",ox,"d1",dx);
        to_header("c11","n2",ny,"o2",oy,"d2",dy);
        to_header("c11","n3",nz,"o3",oz,"d3",dz);

        write("c13",del,nxyz);
        to_header("c13","n1",nx,"o1",ox,"d1",dx);
        to_header("c13","n2",ny,"o2",oy,"d2",dy);
        to_header("c13","n3",nz,"o3",oz,"d3",dz);

        write("c33",v,nxyz);
        to_header("c33","n1",nx,"o1",ox,"d1",dx);
        to_header("c33","n2",ny,"o2",oy,"d2",dy);
        to_header("c33","n3",nz,"o3",oz,"d3",dz);

        delete []v;delete []eps;delete []del;
    }else{
        int nx,ny,nz;
        float ox,oy,oz,dx,dy,dz;
        
        from_header("c11","n1",nx,"o1",ox,"d1",dx);
        from_header("c11","n2",ny,"o2",oy,"d2",dy);
        from_header("c11","n3",nz,"o3",oz,"d3",dz);
        
        long long nxy=nx*ny;
        long long nxyz=nxy*nz;
        
        float *c11=new float[nxyz];
        read("c11",c11,nxyz);
        float *c13=new float[nxyz];
        read("c13",c13,nxyz);
        float *c33=new float[nxyz];
        read("c33",c33,nxyz);
        
        #pragma omp parallel for num_threads(16)
        for(size_t i=0;i<nxyz;i++){
            c11[i]=(c11[i]/c33[i]-1.)*0.5;
            float c13c33=c13[i]/c33[i];
            c13[i]=(c13c33*c13c33-1.)*0.5;
            c33[i]=sqrt(c33[i]);
        }

        write("eps",c11,nxyz);
        to_header("eps","n1",nx,"o1",ox,"d1",dx);
        to_header("eps","n2",ny,"o2",oy,"d2",dy);
        to_header("eps","n3",nz,"o3",oz,"d3",dz);

        write("del",c13,nxyz);
        to_header("del","n1",nx,"o1",ox,"d1",dx);
        to_header("del","n2",ny,"o2",oy,"d2",dy);
        to_header("del","n3",nz,"o3",oz,"d3",dz);

        write("v",c33,nxyz);
        to_header("v","n1",nx,"o1",ox,"d1",dx);
        to_header("v","n2",ny,"o2",oy,"d2",dy);
        to_header("v","n3",nz,"o3",oz,"d3",dz);

        delete []c11;delete []c13;delete []c33;

    }

    myio_close();
    return 0;
}
