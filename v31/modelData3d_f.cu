#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <iostream>
#include <vector>
#include <thread>
#include <chrono>

#include "myio.h"
#include "mylib.h"
#include "wave3d.h"

using namespace std;

void modelData3d_f(float *souloc,int ns,float *recloc,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float ox,float oy,float oz,float ot,float dx,float dy,float dz,float dt,float samplingRate){

 vector<int> GPUs;
 get_array("gpu",GPUs);
 int NGPU=GPUs.size();
 fprintf(stderr,"Total # GPUs = %d\n",NGPU);
 fprintf(stderr,"GPUs used are:\n");
 for(int i=0;i<NGPU;i++) fprintf(stderr,"%d",GPUs[i]);
 fprintf(stderr,"\n");

 float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;
 long long nxyz=nx*ny*nz;
 int nxy=nx*ny;
 int samplingTimeStep=std::round(samplingRate/dt);
 int nnt=(nt-1)/samplingTimeStep+1;

 int *jgpu=new int[NGPU]();
 
// float *damping=new float[nxy];
// init_abc(damping,nx,ny,npad);
 float *damping=new float[nxy+nz];
 init_abc(damping,nx,ny,nz,npad);
 float **d_damping=new float*[NGPU]();

 float *prevSigmaX=new float[nxyz];
 float *curSigmaX=new float[nxyz];
 float *prevSigmaZ=new float[nxyz];
 float *curSigmaZ=new float[nxyz];

 size_t nElemBlock=HALF_STENCIL*nxy;
 size_t nByteBlock=nElemBlock*sizeof(float);
 int nb=nz/HALF_STENCIL;

 float *h_v[2],*h_eps[2],*h_del[2];
 float *h_prevSigmaX[2],*h_curSigmaX[2],*h_SigmaX4[2],*h_SigmaX5[2];
 float *h_prevSigmaZ[2],*h_curSigmaZ[2],*h_SigmaZ4[2],*h_SigmaZ5[2];
 float *h_data[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_v[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_eps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_del[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ5[i],nByteBlock,cudaHostAllocDefault);
 }

 const int nd_Sigma=NUPDATE+2;
 int nbuffSigma[nd_Sigma];
 
 int **d_recIndex=new int*[NGPU]();
 float ***d_data=new float**[NGPU]();
 
 for(int i=0;i<nd_Sigma;++i) nbuffSigma[i]=3;
 nbuffSigma[1]=4;nbuffSigma[nd_Sigma-2]=4;

 float ****d_SigmaX=new float ***[NGPU]();
 float ****d_SigmaZ=new float ***[NGPU]();
 
 const int nbuffVEpsDel=NUPDATE+4;
 float ***d_v=new float**[NGPU]();
 float ***d_eps=new float**[NGPU]();
 float ***d_del=new float**[NGPU]();
 
 cudaStream_t *transfInStream=new cudaStream_t[1]();
 cudaStream_t *transfOutStream=new cudaStream_t[NGPU]();
 cudaStream_t *computeStream=new cudaStream_t[NGPU]();
 
 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(GPUs[gpu]);
  
//  cudaMalloc(&d_damping[gpu],nxy*sizeof(float));
//  cudaMemcpy(d_damping[gpu],damping,nxy*sizeof(float),cudaMemcpyHostToDevice);
  cudaMalloc(&d_damping[gpu],(nxy+nz)*sizeof(float));
  cudaMemcpy(d_damping[gpu],damping,(nxy+nz)*sizeof(float),cudaMemcpyHostToDevice);

  d_data[gpu]=new float*[2]();
  
  d_SigmaX[gpu]=new float**[nd_Sigma]();
  d_SigmaZ[gpu]=new float**[nd_Sigma]();
  for(int i=0;i<nd_Sigma;++i){
   d_SigmaX[gpu][i]=new float*[nbuffSigma[i]]();
   d_SigmaZ[gpu][i]=new float*[nbuffSigma[i]]();
   for(int j=0;j<nbuffSigma[i];++j){
    cudaMalloc(&d_SigmaX[gpu][i][j],nByteBlock); 
    cudaMalloc(&d_SigmaZ[gpu][i][j],nByteBlock); 
   }
  }

  d_v[gpu]=new float*[nbuffVEpsDel]();
  d_eps[gpu]=new float*[nbuffVEpsDel]();
  d_del[gpu]=new float*[nbuffVEpsDel]();
  for(int i=0;i<nbuffVEpsDel;++i){
   cudaMalloc(&d_v[gpu][i],nByteBlock);
   cudaMalloc(&d_eps[gpu][i],nByteBlock);
   cudaMalloc(&d_del[gpu][i],nByteBlock);
  }
 
  if(gpu==0) cudaStreamCreate(&transfInStream[gpu]);
  cudaStreamCreate(&computeStream[gpu]);
  cudaStreamCreate(&transfOutStream[gpu]);
 }

 vector<thread> threads;
 
 int pipelen=NGPU*(NUPDATE+3)+2;
 int ntb=(nt-2)*nb/(NGPU*NUPDATE);
 int nk=ntb+pipelen;
 
 int recBlock=(recloc[2]-oz)/dz/HALF_STENCIL; //assume all receivers are at same depth
 
 for(int is=0;is<ns;is++){
	 fprintf(stderr,"shot # %d\n",is);

	 int nr=souloc[5*is+3];
	 int irbegin=souloc[5*is+4];

	 int *recIndexBlock=new int[nr];
     float *data=new float[nnt*nr]();
	
	 #pragma omp parallel for num_threads(16)
	 for(int ir=0;ir<nr;ir++){
	  int ir1=ir+irbegin;
	  int ix=(recloc[3*ir1]-ox)/dx;
	  int iy=(recloc[3*ir1+1]-oy)/dy;
	  int iz=(recloc[3*ir1+2]-oz)/dz;
	  int ixy=ix+iy*nx;
	  recIndexBlock[ir]=ixy+(iz%HALF_STENCIL)*nxy;
	 }
	 
	 int souIndexX=(souloc[5*is]-ox)/dx;
	 int souIndexY=(souloc[5*is+1]-oy)/dy;
	 int souIndexZ=(souloc[5*is+2]-oz)/dz;
	 int souIndex=souIndexX+souIndexY*nx+souIndexZ*nxy;
	 int souIndexBlock=souIndexX+souIndexY*nx+(souIndexZ%HALF_STENCIL)*nxy;
	 int souBlock=souIndexZ/HALF_STENCIL;

	 memset(jgpu,0,NGPU*sizeof(int));
	
	 memset(prevSigmaX,0,nxyz*sizeof(float));
	 memset(curSigmaX,0,nxyz*sizeof(float));
	 memset(prevSigmaZ,0,nxyz*sizeof(float));
	 memset(curSigmaZ,0,nxyz*sizeof(float));
	 
	 //injecting source at time 0 to wavefields at time 1
     float temp=dt2*wavelet[0];
	 curSigmaX[souIndex]=temp;
	 curSigmaZ[souIndex]=temp;
     
	 for(int i=0;i<2;i++) cudaHostAlloc(&h_data[i],nr*sizeof(float),cudaHostAllocDefault);
	 
	 for(int gpu=0;gpu<NGPU;gpu++){
      cudaSetDevice(GPUs[gpu]);
	  
	  cudaMalloc(&d_recIndex[gpu],nr*sizeof(int));
	  cudaMemcpy(d_recIndex[gpu],recIndexBlock,nr*sizeof(int),cudaMemcpyHostToDevice);
	  
	  for(int i=0;i<2;i++) cudaMalloc(&d_data[gpu][i],nr*sizeof(float));
	  
	  for(int i=0;i<nd_Sigma;++i){
	   for(int j=0;j<nbuffSigma[i];++j){
	    cudaMemset(d_SigmaX[gpu][i][j],0,nByteBlock);
	    cudaMemset(d_SigmaZ[gpu][i][j],0,nByteBlock);
	   }
	  }
	
	  for(int i=0;i<nbuffVEpsDel;++i){
	   cudaMemset(d_v[gpu][i],0,nByteBlock);
	   cudaMemset(d_eps[gpu][i],0,nByteBlock);
	   cudaMemset(d_del[gpu][i],0,nByteBlock);
	  }
	 }
	 
	 int j=0,krecord=0;
 
	 for(int k=0;k<nk;k++){
	   if(k<ntb){
	    int ib=k%nb;
	    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
		threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
		threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
	   }
	   
	   if(k>0 && k<ntb+1){
        cudaSetDevice(GPUs[0]);
	    memcpyCpuToGpu3(d_v[0][k%nbuffVEpsDel],h_v[(k+1)%2],d_eps[0][k%nbuffVEpsDel],h_eps[(k+1)%2],d_del[0][k%nbuffVEpsDel],h_del[(k+1)%2],nByteBlock,transfInStream);
	    memcpyCpuToGpu2(d_SigmaX[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k+1)%2],d_SigmaX[0][1][k%nbuffSigma[1]],h_curSigmaX[(k+1)%2],nByteBlock,transfInStream);
	    memcpyCpuToGpu2(d_SigmaZ[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k+1)%2],d_SigmaZ[0][1][k%nbuffSigma[1]],h_curSigmaZ[(k+1)%2],nByteBlock,transfInStream);
	   }
	  
	   for(int gpu=0;gpu<NGPU;gpu++){
	    int kgpu=k-gpu*(NUPDATE+3);
	
        cudaSetDevice(GPUs[gpu]);
	
	    if(kgpu>2 && kgpu<ntb+NUPDATE+2){
	     bool record=forwardAbc(max(0,kgpu-ntb-2),min(kgpu-2,NUPDATE),kgpu,d_SigmaX[gpu],d_SigmaZ[gpu],nbuffSigma,d_damping[gpu],d_v[gpu],d_eps[gpu],d_del[gpu],nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nr,d_recIndex[gpu],recBlock,samplingTimeStep,d_data[gpu][jgpu[gpu]%2],nx,ny,nz,nt,npad,dx2,dy2,dz2,dt2,computeStream+gpu,gpu,NGPU);
	     if(record){
          krecord=kgpu;
//     	  fprintf(stderr,"record at j %d k %d gpu %d kgpu %d jgpu %d d_data %p\n",j,k,gpu,kgpu,jgpu[gpu],d_data[gpu][jgpu[gpu]%2]);
         }
		}
	    
	    if(kgpu>NUPDATE+3 && kgpu<ntb+NUPDATE+4){
	     if(NGPU>1 && gpu<NGPU-1){
	      memcpyGpuToGpu3(d_v[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_v[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],nByteBlock,transfOutStream+gpu);
	      memcpyGpuToGpu2(d_SigmaX[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaX[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
	      memcpyGpuToGpu2(d_SigmaZ[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaZ[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
	     }
	     else{
	      memcpyGpuToCpu2(h_SigmaX4[kgpu%2],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[kgpu%2],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
	      memcpyGpuToCpu2(h_SigmaZ4[kgpu%2],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[kgpu%2],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
	     }
	    }
	    
		if(kgpu-1==krecord && kgpu>1 && j<nnt-1){
//     	 fprintf(stderr,"transfer data to host at j %d k %d gpu %d kgpu %d krecord %d jgpu %d d_data %p h_data %p\n",j,k,gpu,kgpu,krecord,jgpu[gpu],d_data[gpu][jgpu[gpu]%2],h_data[jgpu[gpu]%2]);
	     cudaMemcpyAsync(h_data[jgpu[gpu]%2],d_data[gpu][jgpu[gpu]%2],nr*sizeof(float),cudaMemcpyDeviceToHost,transfOutStream[gpu]);
	    }
	 
	    if(kgpu-2==krecord && kgpu>2 && j<nnt-1){
//     	 fprintf(stderr,"move data to place at j %d k %d gpu %d kgpu %d krecord %d jgpu %d h_data %p\n\n",j,k,gpu,kgpu,krecord,jgpu[gpu],h_data[jgpu[gpu]%2]);
		 j++;
         #pragma omp parallel for num_threads(16)
         for(int ir=0;ir<nr;ir++) data[j+ir*nnt]=h_data[jgpu[gpu]%2][ir];
//	     memcpy(data+irbegin+j*nrtotal,h_data[(kgpu+1)%2],nr*sizeof(float));
		 jgpu[gpu]++;
         krecord=0;
	    }
	   }
	   
	   if(k>NGPU*(NUPDATE+3)+1){
	    int kgpu=k-(NGPU-1)*(NUPDATE+3);
		int ib=(kgpu-NUPDATE-5)%nb;
	    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(kgpu+1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(kgpu+1)%2],nByteBlock);
	    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(kgpu+1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(kgpu+1)%2],nByteBlock);
	   }
	  
	   for(int gpu=0;gpu<NGPU;gpu++){
        cudaSetDevice(GPUs[gpu]);
	    cudaDeviceSynchronize();
	   }
	   
	   for(int i=0;i<threads.size();++i) threads[i].join();
	   threads.erase(threads.begin(),threads.end());
	 }
    
     write("data",data,nnt*nr,ios_base::app);
	 
	 for(int i=0;i<2;i++) cudaFreeHost(h_data[i]);
	
	 delete []recIndexBlock;
     delete []data;

	 for(int gpu=0;gpu<NGPU;gpu++){
      cudaSetDevice(GPUs[gpu]);
	  cudaFree(d_recIndex[gpu]);
	  for(int i=0;i<2;i++) cudaFree(d_data[gpu][i]);
	 }
 }

 int nrtotal=souloc[5*(ns-1)+3]+souloc[5*(ns-1)+4];
 to_header("data","n1",nnt,"o1",ot,"d1",samplingRate);
 to_header("data","n2",nrtotal,"o2",0.,"d2",1);

 delete []prevSigmaX;delete []curSigmaX;
 delete []prevSigmaZ;delete []curSigmaZ;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_v[i]);
  cudaFreeHost(h_eps[i]);
  cudaFreeHost(h_del[i]);
  cudaFreeHost(h_prevSigmaX[i]);
  cudaFreeHost(h_curSigmaX[i]);
  cudaFreeHost(h_SigmaX4[i]);
  cudaFreeHost(h_SigmaX5[i]);
  cudaFreeHost(h_prevSigmaZ[i]);
  cudaFreeHost(h_curSigmaZ[i]);
  cudaFreeHost(h_SigmaZ4[i]);
  cudaFreeHost(h_SigmaZ5[i]);
 }
 
 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(GPUs[gpu]);

  cudaFree(d_damping[gpu]);
  delete []d_data[gpu];
  
  for(int i=0;i<nd_Sigma;++i){
   for(int j=0;j<nbuffSigma[i];++j){
    cudaFree(d_SigmaX[gpu][i][j]); 
    cudaFree(d_SigmaZ[gpu][i][j]); 
   }
   delete []d_SigmaX[gpu][i];
   delete []d_SigmaZ[gpu][i];
  }
  delete []d_SigmaX[gpu];
  delete []d_SigmaZ[gpu];

  for(int i=0;i<nbuffVEpsDel;++i){
   cudaFree(d_v[gpu][i]);
   cudaFree(d_eps[gpu][i]);
   cudaFree(d_del[gpu][i]);
  }
  delete []d_v[gpu];
  delete []d_eps[gpu];
  delete []d_del[gpu];
  
  if(gpu==0) cudaStreamDestroy(transfInStream[gpu]);
  cudaStreamDestroy(computeStream[gpu]);
  cudaStreamDestroy(transfOutStream[gpu]);
 
  cudaError_t e=cudaGetLastError();
  if(e!=cudaSuccess) fprintf(stderr,"0 error %s\n",cudaGetErrorString(e));
 }

 delete []d_recIndex;
 delete []d_data;
 delete []d_SigmaX;
 delete []d_SigmaZ;
 delete []d_v;
 delete []d_eps;
 delete []d_del;
 delete []transfInStream;
 delete []computeStream;
 delete []transfOutStream;
 delete []jgpu; 
 delete []damping;
 delete []d_damping;
 
 return;
}

void modelData3d_f(float *data,float *souloc,int ns,float *recloc,const float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float ox,float oy,float oz,float ot,float dx,float dy,float dz,float dt,float samplingRate){
 vector<int> GPUs;
 get_array("gpu",GPUs);
 int NGPU=GPUs.size();
 fprintf(stderr,"Total # GPUs = %d\n",NGPU);
 fprintf(stderr,"GPUs used are:\n");
 for(int i=0;i<NGPU;i++) fprintf(stderr,"%d",GPUs[i]);
 fprintf(stderr,"\n");

 float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;
 long long nxyz=nx*ny*nz;
 int nxy=nx*ny;
 int samplingTimeStep=std::round(samplingRate/dt);
 int nnt=(nt-1)/samplingTimeStep+1;
 
 float *dt2wavelet=new float[nt]();
 scale(dt2wavelet,wavelet,1.f/dt2,nt); //this effectively means input wavelet is scaled by dt2 already

// float *damping=new float[nxy];
// init_abc(damping,nx,ny,npad);
 float *damping=new float[nxy+nz];
 init_abc(damping,nx,ny,nz,npad);
 float **d_damping=new float*[NGPU]();

 float *prevSigmaX=new float[nxyz];
 float *curSigmaX=new float[nxyz];
 float *prevSigmaZ=new float[nxyz];
 float *curSigmaZ=new float[nxyz];

 size_t nElemBlock=HALF_STENCIL*nxy;
 size_t nByteBlock=nElemBlock*sizeof(float);
 int nb=nz/HALF_STENCIL;

 float *h_v[2],*h_eps[2],*h_del[2];
 float *h_prevSigmaX[2],*h_curSigmaX[2],*h_SigmaX4[2],*h_SigmaX5[2];
 float *h_prevSigmaZ[2],*h_curSigmaZ[2],*h_SigmaZ4[2],*h_SigmaZ5[2];
 float *h_data[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_v[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_eps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_del[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ5[i],nByteBlock,cudaHostAllocDefault);
 }

 const int nd_Sigma=NUPDATE+2;
 int nbuffSigma[nd_Sigma];
 
 int **d_recIndex=new int*[NGPU]();
 float ***d_data=new float**[NGPU]();
 
 for(int i=0;i<nd_Sigma;++i) nbuffSigma[i]=3;
 nbuffSigma[1]=4;nbuffSigma[nd_Sigma-2]=4;

 float ****d_SigmaX=new float ***[NGPU]();
 float ****d_SigmaZ=new float ***[NGPU]();
 
 const int nbuffVEpsDel=NUPDATE+4;
 float ***d_v=new float**[NGPU]();
 float ***d_eps=new float**[NGPU]();
 float ***d_del=new float**[NGPU]();
 
 cudaStream_t *transfInStream=new cudaStream_t[1]();
 cudaStream_t *transfOutStream=new cudaStream_t[NGPU]();
 cudaStream_t *computeStream=new cudaStream_t[NGPU]();
 
 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(GPUs[gpu]);
  
//  cudaMalloc(&d_damping[gpu],nxy*sizeof(float));
//  cudaMemcpy(d_damping[gpu],damping,nxy*sizeof(float),cudaMemcpyHostToDevice);
  cudaMalloc(&d_damping[gpu],(nxy+nz)*sizeof(float));
  cudaMemcpy(d_damping[gpu],damping,(nxy+nz)*sizeof(float),cudaMemcpyHostToDevice);

  d_data[gpu]=new float*[2]();
  
  d_SigmaX[gpu]=new float**[nd_Sigma]();
  d_SigmaZ[gpu]=new float**[nd_Sigma]();
  for(int i=0;i<nd_Sigma;++i){
   d_SigmaX[gpu][i]=new float*[nbuffSigma[i]]();
   d_SigmaZ[gpu][i]=new float*[nbuffSigma[i]]();
   for(int j=0;j<nbuffSigma[i];++j){
    cudaMalloc(&d_SigmaX[gpu][i][j],nByteBlock); 
    cudaMalloc(&d_SigmaZ[gpu][i][j],nByteBlock); 
   }
  }

  d_v[gpu]=new float*[nbuffVEpsDel]();
  d_eps[gpu]=new float*[nbuffVEpsDel]();
  d_del[gpu]=new float*[nbuffVEpsDel]();
  for(int i=0;i<nbuffVEpsDel;++i){
   cudaMalloc(&d_v[gpu][i],nByteBlock);
   cudaMalloc(&d_eps[gpu][i],nByteBlock);
   cudaMalloc(&d_del[gpu][i],nByteBlock);
  }
 
  if(gpu==0) cudaStreamCreate(&transfInStream[gpu]);
  cudaStreamCreate(&computeStream[gpu]);
  cudaStreamCreate(&transfOutStream[gpu]);
 }

 vector<thread> threads;

 int ntb=(nt-2)/NUPDATE/NGPU*nb;
 
 int recBlock=(recloc[2]-oz)/dz/HALF_STENCIL; //assume all receivers are at same depth
 
 for(int is=0;is<ns;is++){
	 fprintf(stderr,"shot # %d\n",is);

	 int nr=souloc[5*is+3];
	 int irbegin=souloc[5*is+4];

	 int *recIndexBlock=new int[nr];
	
	 #pragma omp parallel for num_threads(16)
	 for(int ir=0;ir<nr;ir++){
	  int ir1=ir+irbegin;
	  int ix=(recloc[3*ir1]-ox)/dx;
	  int iy=(recloc[3*ir1+1]-oy)/dy;
	  int iz=(recloc[3*ir1+2]-oz)/dz;
	  int ixy=ix+iy*nx;
	  recIndexBlock[ir]=ixy+(iz%HALF_STENCIL)*nxy;
	 }
	 
	 int souIndexX=(souloc[5*is]-ox)/dx;
	 int souIndexY=(souloc[5*is+1]-oy)/dy;
	 int souIndexZ=(souloc[5*is+2]-oz)/dz;
	 int souIndex=souIndexX+souIndexY*nx+souIndexZ*nxy;
	 int souIndexBlock=souIndexX+souIndexY*nx+(souIndexZ%HALF_STENCIL)*nxy;
	 int souBlock=souIndexZ/HALF_STENCIL;
	
	 memset(prevSigmaX,0,nxyz*sizeof(float));
	 memset(curSigmaX,0,nxyz*sizeof(float));
	 memset(prevSigmaZ,0,nxyz*sizeof(float));
	 memset(curSigmaZ,0,nxyz*sizeof(float));
	 
	 //injecting source at time 0 to wavefields at time 1
     float temp=dt2*dt2wavelet[0];
	 curSigmaX[souIndex]=temp;
	 curSigmaZ[souIndex]=temp;
     
	 for(int i=0;i<2;i++) cudaHostAlloc(&h_data[i],nr*sizeof(float),cudaHostAllocDefault);
	 
	 for(int gpu=0;gpu<NGPU;gpu++){
      cudaSetDevice(GPUs[gpu]);
	  
	  cudaMalloc(&d_recIndex[gpu],nr*sizeof(int));
	  cudaMemcpy(d_recIndex[gpu],recIndexBlock,nr*sizeof(int),cudaMemcpyHostToDevice);
	  
	  for(int i=0;i<2;i++) cudaMalloc(&d_data[gpu][i],nr*sizeof(float));
	  
	  for(int i=0;i<nd_Sigma;++i){
	   for(int j=0;j<nbuffSigma[i];++j){
	    cudaMemset(d_SigmaX[gpu][i][j],0,nByteBlock);
	    cudaMemset(d_SigmaZ[gpu][i][j],0,nByteBlock);
	   }
	  }
	
	  for(int i=0;i<nbuffVEpsDel;++i){
	   cudaMemset(d_v[gpu][i],0,nByteBlock);
	   cudaMemset(d_eps[gpu][i],0,nByteBlock);
	   cudaMemset(d_del[gpu][i],0,nByteBlock);
	  }
	 }
	 
	 int j=0,krecord=0;
 
	 for(int k=0;k<ntb+NGPU*(NUPDATE+3)+2;k++){
	   if(k<ntb){
	    int ib=k%nb;
	    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
		threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
		threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
	   }
	   
	   if(k>0 && k<ntb+1){
        cudaSetDevice(GPUs[0]);
	    memcpyCpuToGpu3(d_v[0][k%nbuffVEpsDel],h_v[(k+1)%2],d_eps[0][k%nbuffVEpsDel],h_eps[(k+1)%2],d_del[0][k%nbuffVEpsDel],h_del[(k+1)%2],nByteBlock,transfInStream);
	    memcpyCpuToGpu2(d_SigmaX[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k+1)%2],d_SigmaX[0][1][k%nbuffSigma[1]],h_curSigmaX[(k+1)%2],nByteBlock,transfInStream);
	    memcpyCpuToGpu2(d_SigmaZ[0][0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k+1)%2],d_SigmaZ[0][1][k%nbuffSigma[1]],h_curSigmaZ[(k+1)%2],nByteBlock,transfInStream);
	   }
	  
	   for(int gpu=0;gpu<NGPU;gpu++){
	    int kgpu=k-gpu*(NUPDATE+3);
	
        cudaSetDevice(GPUs[gpu]);
	
	    if(kgpu>2 && kgpu<ntb+NUPDATE+2){
	     bool record=forwardAbc(max(0,kgpu-ntb-2),min(kgpu-2,NUPDATE),kgpu,d_SigmaX[gpu],d_SigmaZ[gpu],nbuffSigma,d_damping[gpu],d_v[gpu],d_eps[gpu],d_del[gpu],nbuffVEpsDel,dt2wavelet,souIndexBlock,souBlock,nr,d_recIndex[gpu],recBlock,samplingTimeStep,d_data[gpu][kgpu%2],nx,ny,nz,nt,npad,dx2,dy2,dz2,dt2,computeStream+gpu,gpu,NGPU);
	     if(record){
		  krecord=kgpu;
		  j++;
		 }
		}
	    
	    if(kgpu>NUPDATE+3 && kgpu<ntb+NUPDATE+4){
	     if(NGPU>1 && gpu<NGPU-1){
	      memcpyGpuToGpu3(d_v[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_v[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_eps[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu+1][(kgpu-NUPDATE-3)%nbuffVEpsDel],d_del[gpu][(kgpu-NUPDATE-3)%nbuffVEpsDel],nByteBlock,transfOutStream+gpu);
	      memcpyGpuToGpu2(d_SigmaX[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaX[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
	      memcpyGpuToGpu2(d_SigmaZ[gpu+1][0][(kgpu-NUPDATE-4)%nbuffSigma[0]],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],d_SigmaZ[gpu+1][1][(kgpu-NUPDATE-3)%nbuffSigma[1]],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
	     }
	     else{
	      memcpyGpuToCpu2(h_SigmaX4[kgpu%2],d_SigmaX[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[kgpu%2],d_SigmaX[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
	      memcpyGpuToCpu2(h_SigmaZ4[kgpu%2],d_SigmaZ[gpu][nd_Sigma-2][(kgpu-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[kgpu%2],d_SigmaZ[gpu][nd_Sigma-1][(kgpu-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,transfOutStream+gpu);
	     }
	    }
	    
		if(kgpu-1==krecord){
	     cudaMemcpyAsync(h_data[kgpu%2],d_data[gpu][(kgpu+1)%2],nr*sizeof(float),cudaMemcpyDeviceToHost,transfOutStream[gpu]);
	    }
	 
	    if(kgpu-2==krecord){
         size_t isnntnr=nnt*irbegin;
         #pragma omp parallel for num_threads(16)
         for(int ir=0;ir<nr;ir++) data[j+ir*nnt+isnntnr]=h_data[(kgpu+1)%2][ir];
//	     memcpy(data+irbegin+j*nrtotal,h_data[(kgpu+1)%2],nr*sizeof(float));
	    }
	   }
	   
	   if(k>NGPU*(NUPDATE+3)+1){
	    int kgpu=k-(NGPU-1)*(NUPDATE+3);
		int ib=(kgpu-NUPDATE-5)%nb;
	    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(kgpu+1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(kgpu+1)%2],nByteBlock);
	    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(kgpu+1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(kgpu+1)%2],nByteBlock);
	   }
	  
	   for(int gpu=0;gpu<NGPU;gpu++){
        cudaSetDevice(GPUs[gpu]);
	    cudaDeviceSynchronize();
	   }
	   
	   for(int i=0;i<threads.size();++i) threads[i].join();
	   threads.erase(threads.begin(),threads.end());
	 }
    
	 for(int i=0;i<2;i++) cudaFreeHost(h_data[i]);
	
	 delete []recIndexBlock;

	 for(int gpu=0;gpu<NGPU;gpu++){
      cudaSetDevice(GPUs[gpu]);
	  cudaFree(d_recIndex[gpu]);
	  for(int i=0;i<2;i++) cudaFree(d_data[gpu][i]);
	 }
 }

 delete []prevSigmaX;delete []curSigmaX;
 delete []prevSigmaZ;delete []curSigmaZ;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_v[i]);
  cudaFreeHost(h_eps[i]);
  cudaFreeHost(h_del[i]);
  cudaFreeHost(h_prevSigmaX[i]);
  cudaFreeHost(h_curSigmaX[i]);
  cudaFreeHost(h_SigmaX4[i]);
  cudaFreeHost(h_SigmaX5[i]);
  cudaFreeHost(h_prevSigmaZ[i]);
  cudaFreeHost(h_curSigmaZ[i]);
  cudaFreeHost(h_SigmaZ4[i]);
  cudaFreeHost(h_SigmaZ5[i]);
 }
 
 for(int gpu=0;gpu<NGPU;gpu++){
  cudaSetDevice(GPUs[gpu]);

  cudaFree(d_damping[gpu]);
  delete []d_data[gpu];
  
  for(int i=0;i<nd_Sigma;++i){
   for(int j=0;j<nbuffSigma[i];++j){
    cudaFree(d_SigmaX[gpu][i][j]); 
    cudaFree(d_SigmaZ[gpu][i][j]); 
   }
   delete []d_SigmaX[gpu][i];
   delete []d_SigmaZ[gpu][i];
  }
  delete []d_SigmaX[gpu];
  delete []d_SigmaZ[gpu];

  for(int i=0;i<nbuffVEpsDel;++i){
   cudaFree(d_v[gpu][i]);
   cudaFree(d_eps[gpu][i]);
   cudaFree(d_del[gpu][i]);
  }
  delete []d_v[gpu];
  delete []d_eps[gpu];
  delete []d_del[gpu];
  
  if(gpu==0) cudaStreamDestroy(transfInStream[gpu]);
  cudaStreamDestroy(computeStream[gpu]);
  cudaStreamDestroy(transfOutStream[gpu]);
 
  cudaError_t e=cudaGetLastError();
  if(e!=cudaSuccess) fprintf(stderr,"0 error %s\n",cudaGetErrorString(e));
 }

 delete []d_recIndex;
 delete []d_data;
 delete []d_SigmaX;
 delete []d_SigmaZ;
 delete []d_v;
 delete []d_eps;
 delete []d_del;
 delete []transfInStream;
 delete []computeStream;
 delete []transfOutStream;
 
 delete []damping;
 delete []d_damping;
 delete []dt2wavelet;
 
 return;
}

