#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <iostream>
#include <vector>

#include "myio.h"
#include "mylib.h"
#include "wave3d.h"
#include "ModelingOp.h"

void ModelingOp::forward(bool wantadd,const float *model,float *data){
    if(!wantadd) modelData3d_f(data,_souloc,_ns,_recloc,model,_v,_eps,_del,_nx,_ny,_nz,_nt,_npad,_ox,_oy,_oz,_ot,_dx,_dy,_dz,_dt,_samplingRate);
    else{
        float *tdata=new float[_dataSize];
        modelData3d_f(tdata,_souloc,_ns,_recloc,model,_v,_eps,_del,_nx,_ny,_nz,_nt,_npad,_ox,_oy,_oz,_ot,_dx,_dy,_dz,_dt,_samplingRate);
        add(data,data,tdata,_dataSize);
        delete []tdata;
    }
    return;
}

void ModelingOp::adjoint(bool wantadd,float *model,const float *data){
    if(!wantadd) gradientWavelet_f(model,data,_souloc,_ns,_recloc,_v,_eps,_del,_nx,_ny,_nz,_nt,_npad,_ox,_oy,_oz,_ot,_dx,_dy,_dz,_dt,_samplingRate); 
    else{
        float *gw=new float[_nt];
        gradientWavelet_f(gw,data,_souloc,_ns,_recloc,_v,_eps,_del,_nx,_ny,_nz,_nt,_npad,_ox,_oy,_oz,_ot,_dx,_dy,_dz,_dt,_samplingRate);
        add(model,model,gw,_nt);
        delete []gw;
    } 
    return;
}

