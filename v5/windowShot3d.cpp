#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>

#include "myio.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);
    
    int ns,nr;
    from_header("souloc","n2",ns);
    float *souloc=new float[5*ns];
    read("souloc",souloc,5*ns);
    
    vector<int> shotid;
        if(!get_array("shotid",shotid)){
        vector<int> shotrange;
        if(!get_array("shotrange",shotrange)){
            shotrange.push_back(0);
            shotrange.push_back(ns);
        }
        vector<int> badshot;
        get_array("badshot",badshot);
        for(int i=shotrange[0];i<shotrange[1];i++){
            if(find(badshot.begin(),badshot.end(),i)==badshot.end()) shotid.push_back(i);
        }
    }
    
    int n1_recloc;
    from_header("recloc","n1",n1_recloc,"n2",nr);
    float *recloc=new float[n1_recloc*nr];
    read("recloc",recloc,n1_recloc*nr);

    int nt;
    float ot,dt;
    from_header("data","n1",nt,"o1",ot,"d1",dt);

    int ns1=shotid.size();
    float *souloc1=new float[5*ns1];
    int nrtotal=0;
    for(int is=0;is<ns1;is++){
        int id=shotid[is];
        int nr1=souloc[5*id+3];
        fprintf(stderr,"shot %d has %d traces\n",is,nr1);
        memcpy(souloc1+5*is,souloc+5*id,3*sizeof(float));
        souloc1[5*is+3]=nr1;
        souloc1[5*is+4]=nrtotal;
        nrtotal+=nr1;
    }

    float *recloc1=new float[n1_recloc*nrtotal];
    for(int is=0;is<ns1;is++){
        int id=shotid[is];
        int nr1=souloc1[5*is+3];
        memcpy(recloc1+n1_recloc*(int)souloc1[5*is+4],recloc+n1_recloc*(int)souloc[5*id+4],n1_recloc*nr1*sizeof(float));
        int ntnr1=nt*nr1;
        float *data=new float[ntnr1];
        size_t pos=(long long)nt*(long long)souloc[5*id+4];
        read("data",data,ntnr1,pos);
        write("dataout",data,ntnr1,ios_base::app);
        delete []data;
    }

    to_header("dataout","n1",nt,"o1",ot,"d1",dt);
    to_header("dataout","n2",nrtotal,"o2",0,"d2",1);
    
    write("soulocout",souloc1,5*ns1);
    to_header("soulocout","n1",5,"o1",0,"d1",1);
    to_header("soulocout","n2",ns1,"o2",0,"d2",1);

    write("reclocout",recloc1,n1_recloc*nrtotal);
    to_header("reclocout","n1",n1_recloc,"o1",0,"d1",1);
    to_header("reclocout","n2",nrtotal,"o2",0,"d2",1);

    delete []souloc;delete[]recloc;
    delete []souloc1;delete[]recloc1;

    myio_close();
    return 0;
}
