#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <iostream>
#include <vector>
#include <thread>
#include <chrono>

#include "myio.h"
#include "mylib.h"
#include "init.h"

#define PI 3.14159265359

#define C0 -2.84722222222
#define C1 1.6
#define C2 -0.2
#define C3 0.02539682539
#define C4 -0.00178571428

#define DAMPER 0.95

#define BLOCK_DIM 16
#define NUPDATE 9
#define HALF_STENCIL 4

using namespace std;

void memcpyCpuToCpu2(float *dest1,float *sou1,float *dest2,float *sou2,size_t nbytes){
 memcpy(dest1,sou1,nbytes);
 memcpy(dest2,sou2,nbytes);
}

void memcpyCpuToCpu3(float *dest1,float *sou1,float *dest2,float *sou2,float *dest3,float *sou3,size_t nbytes){
 memcpy(dest1,sou1,nbytes);
 memcpy(dest2,sou2,nbytes);
 memcpy(dest3,sou3,nbytes);
}

void memcpyCpuToGpu2(float *dest1,float *sou1,float *dest2,float *sou2,size_t nbytes,cudaStream_t *stream){
 cudaMemcpyAsync(dest1,sou1,nbytes,cudaMemcpyHostToDevice,*stream);
 cudaMemcpyAsync(dest2,sou2,nbytes,cudaMemcpyHostToDevice,*stream);
}

void memcpyCpuToGpu3(float *dest1,float *sou1,float *dest2,float *sou2,float *dest3,float *sou3,size_t nbytes,cudaStream_t *stream){
 cudaMemcpyAsync(dest1,sou1,nbytes,cudaMemcpyHostToDevice,*stream);
 cudaMemcpyAsync(dest2,sou2,nbytes,cudaMemcpyHostToDevice,*stream);
 cudaMemcpyAsync(dest3,sou3,nbytes,cudaMemcpyHostToDevice,*stream);
}

void memcpyGpuToCpu2(float *dest1,float *sou1,float *dest2,float *sou2,size_t nbytes,cudaStream_t *stream){
 cudaMemcpyAsync(dest1,sou1,nbytes,cudaMemcpyDeviceToHost,*stream);
 cudaMemcpyAsync(dest2,sou2,nbytes,cudaMemcpyDeviceToHost,*stream);
}

void memcpyGpuToCpu3(float *dest1,float *sou1,float *dest2,float *sou2,float *dest3,float *sou3,size_t nbytes,cudaStream_t *stream){
 cudaMemcpyAsync(dest1,sou1,nbytes,cudaMemcpyDeviceToHost,*stream);
 cudaMemcpyAsync(dest2,sou2,nbytes,cudaMemcpyDeviceToHost,*stream);
 cudaMemcpyAsync(dest3,sou3,nbytes,cudaMemcpyDeviceToHost,*stream);
}

__global__ void kernel(float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2){
 
 __shared__ float sSigmaX[BLOCK_DIM+2*HALF_STENCIL][BLOCK_DIM+2*HALF_STENCIL]; 
 
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx && iy<ny){
  int i=ix+iy*nx;
  
  int six=threadIdx.x+HALF_STENCIL;
  int siy=threadIdx.y+HALF_STENCIL;
  
  float zSigmaZ[2*HALF_STENCIL+1];
  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   zSigmaZ[iz+1]=curSigmaZ0[j];
   zSigmaZ[iz+1+HALF_STENCIL]=curSigmaZ1[j];
  }

  for(int iz=0;iz<HALF_STENCIL;++iz){
   int j=i+iz*nx*ny;
   
   zSigmaZ[0]=zSigmaZ[1];
   zSigmaZ[1]=zSigmaZ[2];
   zSigmaZ[2]=zSigmaZ[3];
   zSigmaZ[3]=zSigmaZ[4];
   zSigmaZ[4]=zSigmaZ[5];
   zSigmaZ[5]=zSigmaZ[6];
   zSigmaZ[6]=zSigmaZ[7];
   zSigmaZ[7]=zSigmaZ[8];
   zSigmaZ[8]=curSigmaZ2[j];
   
   __syncthreads();
   
   sSigmaX[six][siy]=curSigmaX1[j];
   
   if(threadIdx.x<HALF_STENCIL){
    int k=min(blockDim.x,nx-2*HALF_STENCIL-blockIdx.x*blockDim.x);
    sSigmaX[threadIdx.x][siy]=curSigmaX1[j-HALF_STENCIL];
	sSigmaX[six+k][siy]=curSigmaX1[j+k];
   }
   
   if(threadIdx.y<HALF_STENCIL){
    int k=min(blockDim.y,ny-2*HALF_STENCIL-blockIdx.y*blockDim.y);
    sSigmaX[six][threadIdx.y]=curSigmaX1[j-HALF_STENCIL*nx];
	sSigmaX[six][siy+k]=curSigmaX1[j+k*nx];
   }

   __syncthreads();

   float c33=v[j]*v[j];
   float c11=c33*(1.+2.*eps[j]);
   float c13=c33*sqrt(1.+2.*del[j]);
  
   float tx=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six-1][siy]+sSigmaX[six+1][siy])
                                 +C2*(sSigmaX[six-2][siy]+sSigmaX[six+2][siy])
                                 +C3*(sSigmaX[six-3][siy]+sSigmaX[six+3][siy])
                                 +C4*(sSigmaX[six-4][siy]+sSigmaX[six+4][siy]))/dx2;
   float ty=(C0*sSigmaX[six][siy]+C1*(sSigmaX[six][siy-1]+sSigmaX[six][siy+1])
                                 +C2*(sSigmaX[six][siy-2]+sSigmaX[six][siy+2])
                                 +C3*(sSigmaX[six][siy-3]+sSigmaX[six][siy+3])
                                 +C4*(sSigmaX[six][siy-4]+sSigmaX[six][siy+4]))/dy2;
   float tz=(C0*zSigmaZ[HALF_STENCIL]+C1*(zSigmaZ[HALF_STENCIL-1]+zSigmaZ[HALF_STENCIL+1])
                                     +C2*(zSigmaZ[HALF_STENCIL-2]+zSigmaZ[HALF_STENCIL+2])
                                     +C3*(zSigmaZ[HALF_STENCIL-3]+zSigmaZ[HALF_STENCIL+3])
                                     +C4*(zSigmaZ[HALF_STENCIL-4]+zSigmaZ[HALF_STENCIL+4]))/dz2;
   
   nextSigmaX[j]=dt2*(c11*(tx+ty)+c13*tz)+2.*sSigmaX[six][siy]-prevSigmaX[j];
   nextSigmaZ[j]=dt2*(c13*(tx+ty)+c33*tz)+2.*zSigmaZ[HALF_STENCIL]-prevSigmaZ[j];
  }
 } 

 return;
}

__global__ void injectSource(float *SigmaX,float *SigmaZ,float source,int souIndex){
 SigmaX[souIndex]+=source;
 SigmaZ[souIndex]+=source;
 return;
}

__global__ void recordData(float *data,float *SigmaX,float *SigmaZ,int nr,int *recIndex){
 int ir=threadIdx.x+blockIdx.x*blockDim.x;
 if(ir<nr){
  data[ir]=2./3.*SigmaX[recIndex[ir]]+1./3.*SigmaZ[recIndex[ir]];
 }
 return;
}

__global__ void abc(int iblock,int nx,int ny,int nz,int npad,float *nextSigmaX,float *curSigmaX1,float *nextSigmaZ,float *curSigmaZ1){
 int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
 int iy=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;

 if(ix<nx && iy<ny){
  int ixy=ix+iy*nx;

  for(int i=0;i<HALF_STENCIL;++i){
   int iz=iblock*HALF_STENCIL+i;
   float dist;
   
   if(ix<npad) dist=npad-ix;
   else if(ix>nx-npad) dist=ix-nx+npad;
   else dist=0.;
   float dampingX=DAMPER+(1.-DAMPER)*cos(PI*dist/npad);
   
   if(iy<npad) dist=npad-iy;
   else if(iy>ny-npad) dist=iy-ny+npad;
   else dist=0.;
   float dampingY=DAMPER+(1.-DAMPER)*cos(PI*dist/npad);
   
   if(iz<npad) dist=npad-iz;
   else if(iz>nz-npad) dist=iz-nz+npad;
   else dist=0.;
   float dampingZ=DAMPER+(1.-DAMPER)*cos(PI*dist/npad);
   
   float damping=dampingX*dampingY*dampingZ;

   int j=ixy+i*nx*ny;
   nextSigmaX[j]*=damping;
   curSigmaX1[j]*=damping;
   nextSigmaZ[j]*=damping;
   curSigmaZ1[j]*=damping;
  }
 }
	
 return;
}

void launchKernelStream(float *nextSigmaX,float *curSigmaX0,float *curSigmaX1,float *curSigmaX2,float *prevSigmaX,float *nextSigmaZ,float *curSigmaZ0,float *curSigmaZ1,float *curSigmaZ2,float *prevSigmaZ,float *v,float *eps,float *del,int nx,int ny,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream){
 dim3 block(BLOCK_DIM,BLOCK_DIM);
 dim3 grid((nx-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM,(ny-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM);
 kernel<<<grid,block,0,*stream>>>(nextSigmaX,curSigmaX0,curSigmaX1,curSigmaX2,prevSigmaX,nextSigmaZ,curSigmaZ0,curSigmaZ1,curSigmaZ2,prevSigmaZ,v,eps,del,nx,ny,dx2,dy2,dz2,dt2);
 return;
}

void update(int b,int e,int ib,float ***d_SigmaX,float ***d_SigmaZ,int *nbuffSigma,float **d_v,float **d_eps,float **d_del,const int nbuffVEpsDel,float *wavelet,int it,int souIndex,int souBlock,int nr,int *recIndex,int recBlock,int samplingTimeStep,float *data,int nx,int ny,int nz,int npad,float dx2,float dy2,float dz2,float dt2,cudaStream_t *stream){
 dim3 block(BLOCK_DIM,BLOCK_DIM);
 dim3 grid((nx-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM,(ny-2*HALF_STENCIL+BLOCK_DIM-1)/BLOCK_DIM);
 
 for(int i=b;i<e;++i){
  kernel<<<grid,block,0,*stream>>>(d_SigmaX[i+2][(ib-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(ib-3)%nbuffSigma[i+1]],d_SigmaX[i+1][(ib-2)%nbuffSigma[i+1]],d_SigmaX[i+1][(ib-1)%nbuffSigma[i+1]],d_SigmaX[i][(ib-3)%nbuffSigma[i]],d_SigmaZ[i+2][(ib-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(ib-3)%nbuffSigma[i+1]],d_SigmaZ[i+1][(ib-2)%nbuffSigma[i+1]],d_SigmaZ[i+1][(ib-1)%nbuffSigma[i+1]],d_SigmaZ[i][(ib-3)%nbuffSigma[i]],d_v[(ib-i-2)%nbuffVEpsDel],d_eps[(ib-i-2)%nbuffVEpsDel],d_del[(ib-i-2)%nbuffVEpsDel],nx,ny,dx2,dy2,dz2,dt2);
 
  int timeIndex=it*NUPDATE+2+i;

  if(ib<souBlock+3+NUPDATE && i==ib-souBlock-3){
   float source=dt2*wavelet[timeIndex-1];
   injectSource<<<1,1,0,*stream>>>(d_SigmaX[i+2][(ib-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(ib-1)%nbuffSigma[i+2]],source,souIndex);
  }
 
  int iblock=ib-3-i;
  abc<<<grid,block,0,*stream>>>(iblock,nx,ny,nz,npad,d_SigmaX[i+2][(ib-1)%nbuffSigma[i+2]],d_SigmaX[i+1][(ib-2)%nbuffSigma[i+1]],d_SigmaZ[i+2][(ib-1)%nbuffSigma[i+2]],d_SigmaZ[i+1][(ib-2)%nbuffSigma[i+1]]);
  
  if(ib<recBlock+3+NUPDATE && i==ib-recBlock-3 && timeIndex%samplingTimeStep==0){
   recordData<<<(nr+BLOCK_DIM-1)/BLOCK_DIM,BLOCK_DIM,0,*stream>>>(data+timeIndex/samplingTimeStep*nr,d_SigmaX[i+2][(ib-1)%nbuffSigma[i+2]],d_SigmaZ[i+2][(ib-1)%nbuffSigma[i+2]],nr,recIndex);
  }
 }
 
 return;
}

int main(int argc,char **argv){
 cudaSetDevice(0);
 
 myio_init(argc,argv);
 
 int nx,ny,nz,npad,nt;
 float ox,oy,oz,ot,dx,dy,dz,dt;
 
 get_param("nx",nx,"ox",ox,"dx",dx);
 get_param("ny",ny,"oy",oy,"dy",dy);
 get_param("nz",nz,"oz",oz,"dz",dz);
 get_param("npad",npad);
 get_param("nt",nt,"ot",ot,"dt",dt);
 
 float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;
 int nxy=nx*ny;
 long long nxyz=nx*ny*nz;
 
 float *wavelet=new float[nt]();
 float freq,scalefactor;
 
 get_param("freq",freq,"scalefactor",scalefactor);
 ricker(wavelet,freq,nt,dt,scalefactor);

 int souIndexX,souIndexY,souIndexZ;
 get_param("souIndexX",souIndexX,"souIndexY",souIndexY,"souIndexZ",souIndexZ);
 
 int souBlock=souIndexZ/HALF_STENCIL;
 int souIndex=souIndexX+souIndexY*nx+(souIndexZ%HALF_STENCIL)*nxy;

 int recIndexZ;
 get_param("recIndexZ",recIndexZ);
 int recBlock=recIndexZ/HALF_STENCIL;

 int nr=nxy;
 int *recIndex=new int[nr];
 
 for(int iy=0;iy<ny;iy++){
	 for(int ix=0;ix<nx;ix++){
		 int i=ix+iy*nx;
		 recIndex[i]=i+(recIndexZ%HALF_STENCIL)*nxy;
	 }
 }

 int *d_recIndex;
 cudaMalloc(&d_recIndex,nr*sizeof(int));
 cudaMemcpy(d_recIndex,recIndex,nr*sizeof(int),cudaMemcpyHostToDevice);

 float samplingRate;
 get_param("samplingRate",samplingRate);
 int samplingTimeStep=std::round(samplingRate/dt);
 int nnt=(nt-1)/samplingTimeStep+1;

 float *data=new float[nr*nnt]();
 float *d_data;
 cudaMalloc(&d_data,nr*nnt*sizeof(float));

 float *prevSigmaX=new float[nxyz]();
 float *curSigmaX=new float[nxyz]();
 float *prevSigmaZ=new float[nxyz]();
 float *curSigmaZ=new float[nxyz]();
 float *p=new float[nxyz]();

 curSigmaX[souIndex]=dt2*wavelet[0];
 curSigmaZ[souIndex]=dt2*wavelet[0];
 
 float *v=new float[nxyz];
 read("v",v,nxyz);
 float *eps=new float[nxyz];
 read("eps",eps,nxyz);
 float *del=new float[nxyz];
 read("del",del,nxyz);
 
 size_t nElemBlock=HALF_STENCIL*nx*ny;
 size_t nByteBlock=nElemBlock*sizeof(float);
 int nb=nz/HALF_STENCIL;

 float *h_v[2],*h_eps[2],*h_del[2];
 float *h_prevSigmaX[2],*h_curSigmaX[2],*h_SigmaX4[2],*h_SigmaX5[2];
 float *h_prevSigmaZ[2],*h_curSigmaZ[2],*h_SigmaZ4[2],*h_SigmaZ5[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_v[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_eps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_del[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ5[i],nByteBlock,cudaHostAllocDefault);
 }

 const int nd_Sigma=NUPDATE+2;
 float **d_SigmaX[nd_Sigma],**d_SigmaZ[nd_Sigma];
 int nbuffSigma[nd_Sigma];
 
 for(int i=0;i<nd_Sigma;++i) nbuffSigma[i]=3;
 nbuffSigma[1]=4;nbuffSigma[nd_Sigma-2]=4;
 
 size_t nByteAlloc=0;

 for(int i=0;i<nd_Sigma;++i){
  d_SigmaX[i]=new float*[nbuffSigma[i]]();
  d_SigmaZ[i]=new float*[nbuffSigma[i]]();
  for(int j=0;j<nbuffSigma[i];++j){
   cudaMalloc(&d_SigmaX[i][j],nByteBlock); 
   cudaMalloc(&d_SigmaZ[i][j],nByteBlock); 
   nByteAlloc+=2*nByteBlock;
   cudaMemset(d_SigmaX[i][j],0,nByteBlock);
   cudaMemset(d_SigmaZ[i][j],0,nByteBlock);
  }
 }

 const int nbuffVEpsDel=NUPDATE+4;
 float *d_v[nbuffVEpsDel],*d_eps[nbuffVEpsDel],*d_del[nbuffVEpsDel];
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaMalloc(&d_v[i],nByteBlock);
  cudaMalloc(&d_eps[i],nByteBlock);
  cudaMalloc(&d_del[i],nByteBlock);
  nByteAlloc+=3*nByteBlock;
  cudaMemset(d_v[i],0,nByteBlock);
  cudaMemset(d_eps[i],0,nByteBlock);
  cudaMemset(d_del[i],0,nByteBlock);
 }

 fprintf(stderr,"alloc %f MBs on GPU\n",nByteAlloc*1e-6);

 cudaStream_t computeStream,transfInStream,transfOutStream;
 cudaStreamCreate(&computeStream);
 cudaStreamCreate(&transfInStream);
 cudaStreamCreate(&transfOutStream);

 vector<thread> threads;
 
 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 for(int it=0;it<(nt-2)/NUPDATE;++it){
  for(int ib=0;ib<nb+NUPDATE+5;++ib){
   if(ib<nb){
    threads.push_back(thread(memcpyCpuToCpu3,h_v[ib%2],v+ib*nElemBlock,h_eps[ib%2],eps+ib*nElemBlock,h_del[ib%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[ib%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[ib%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[ib%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[ib%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(ib>0 && ib<nb+1){
    memcpyCpuToGpu3(d_v[ib%nbuffVEpsDel],h_v[(ib+1)%2],d_eps[ib%nbuffVEpsDel],h_eps[(ib+1)%2],d_del[ib%nbuffVEpsDel],h_del[(ib+1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(ib-1)%nbuffSigma[0]],h_prevSigmaX[(ib+1)%2],d_SigmaX[1][ib%nbuffSigma[1]],h_curSigmaX[(ib+1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(ib-1)%nbuffSigma[0]],h_prevSigmaZ[(ib+1)%2],d_SigmaZ[1][ib%nbuffSigma[1]],h_curSigmaZ[(ib+1)%2],nByteBlock,&transfInStream);
   }
   
   if(ib>2 && ib<nb+NUPDATE+2){
    if(ib<NUPDATE+3) cudaMemsetAsync(d_SigmaZ[ib-2][(ib-3)%nbuffSigma[ib-2]],0,nByteBlock,computeStream);
    if(ib>nb+1) cudaMemsetAsync(d_SigmaZ[ib-nb-1][(ib-1)%nbuffSigma[ib-nb-1]],0,nByteBlock,computeStream);
    update(max(0,ib-nb-2),min(ib-2,NUPDATE),ib,d_SigmaX,d_SigmaZ,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,it,souIndex,souBlock,nr,d_recIndex,recBlock,samplingTimeStep,d_data,nx,ny,nz,npad,dx2,dy2,dz2,dt2,&computeStream);
   }
   
   if(ib>NUPDATE+3 && ib<nb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[ib%2],d_SigmaX[nd_Sigma-2][(ib-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[ib%2],d_SigmaX[nd_Sigma-1][(ib-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[ib%2],d_SigmaZ[nd_Sigma-2][(ib-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[ib%2],d_SigmaZ[nd_Sigma-1][(ib-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
   }
   
   if(ib>NUPDATE+4){
    memcpyCpuToCpu2(prevSigmaX+(ib-NUPDATE-5)*nElemBlock,h_SigmaX4[(ib+1)%2],curSigmaX+(ib-NUPDATE-5)*nElemBlock,h_SigmaX5[(ib+1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+(ib-NUPDATE-5)*nElemBlock,h_SigmaZ4[(ib+1)%2],curSigmaZ+(ib-NUPDATE-5)*nElemBlock,h_SigmaZ5[(ib+1)%2],nByteBlock);
   }
   
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  }
 }

 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 long long n=nx*ny*nz;
 long long ncells=n*nt;
 cout<<"ncells "<<ncells<<endl;
 cout<<"speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl;
 
 cudaError_t e=cudaGetLastError();
 if(e!=cudaSuccess) fprintf(stderr,"error %s\n",cudaGetErrorString(e));

 lin_comb(p,2./3.,curSigmaX,1./3.,curSigmaZ,nxyz);

 write("pressure",p,nxyz);
 to_header("pressure","n1",nx,"o1",ox,"d1",dx);
 to_header("pressure","n2",ny,"o2",oy,"d2",dy);
 to_header("pressure","n3",nz,"o3",oz,"d3",dz);

 cudaMemcpy(data,d_data,nr*nnt*sizeof(float),cudaMemcpyDeviceToHost);

 write("data",data,nr*nnt);
 to_header("data","n1",nx,"o1",ox,"d1",dx);
 to_header("data","n2",ny,"o2",oy,"d2",dy);
 to_header("data","n3",nnt,"o3",ot,"d3",samplingRate);

 delete []data;
 cudaFree(d_data);
 
 delete []wavelet;
 delete []prevSigmaX;delete []curSigmaX;
 delete []prevSigmaZ;delete []curSigmaZ;
 delete []p;
 delete []v;delete []eps;delete []del;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_v[i]);
  cudaFreeHost(h_eps[i]);
  cudaFreeHost(h_del[i]);
  cudaFreeHost(h_prevSigmaX[i]);
  cudaFreeHost(h_curSigmaX[i]);
  cudaFreeHost(h_SigmaX4[i]);
  cudaFreeHost(h_SigmaX5[i]);
  cudaFreeHost(h_prevSigmaZ[i]);
  cudaFreeHost(h_curSigmaZ[i]);
  cudaFreeHost(h_SigmaZ4[i]);
  cudaFreeHost(h_SigmaZ5[i]);
 }
 
 for(int i=0;i<nd_Sigma;++i){
  for(int j=0;j<nbuffSigma[i];++j){
   cudaFree(d_SigmaX[i][j]); 
   cudaFree(d_SigmaZ[i][j]); 
  }
  delete []d_SigmaX[i];
  delete []d_SigmaZ[i];
 }
 
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaFree(d_v[i]);
  cudaFree(d_eps[i]);
  cudaFree(d_del[i]);
 }

 delete []recIndex;
 cudaFree(d_recIndex);
 
 cudaStreamDestroy(computeStream);
 cudaStreamDestroy(transfInStream);
 cudaStreamDestroy(transfOutStream);
 
 myio_close();
 return 0;
}

