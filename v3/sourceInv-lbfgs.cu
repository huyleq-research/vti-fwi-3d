#include <cstdio>
#include <chrono>
#include "myio.h"
#include "mylib.h"
#include "wave3d.h"

using namespace std;

extern "C"{
 void lbfgs_(int *N,int *M,float *X,float *F,float *G,int *DIAGCO,float *DIAG,int IPRINT[],float *EPS,float *XTOL,float *W,int *IFLAG);  
}

struct{
 int LP,MP;
 float BTOL,STPMIN,STPMAX;
} LB2;

int main(int argc,char **argv){
 myio_init(argc,argv);

 int nx,ny,nz,npad,nt;
 float ox,oy,oz,ot,dx,dy,dz,dt;
 
 from_header("v","n1",nx,"o1",ox,"d1",dx);
 from_header("v","n2",ny,"o2",oy,"d2",dy);
 from_header("v","n3",nz,"o3",oz,"d3",dz);
 get_param("npad",npad);
 get_param("nt",nt,"ot",ot,"dt",dt);
 
 long long nxy=nx*ny;
 long long nxyz=nxy*nz;
 long long nboundary=nxyz-(nx-2*npad)*(ny-2*npad)*(nz-2*npad);
 
 float *wavelet=new float[nt]();
 float *gwavelet=new float[nt]();

 float samplingRate;
 get_param("samplingRate",samplingRate);
 int samplingTimeStep=std::round(samplingRate/dt);
 
 float *vepsdel=new float[3*nxyz];
 float *v=vepsdel,*eps=vepsdel+nxyz,*del=vepsdel+2*nxyz;
 read("v",v,nxyz);
 read("eps",eps,nxyz);
 read("del",del,nxyz);

 float *padboundary=new float[nboundary];
 read("padboundary",padboundary,nboundary);
 
 int ns,nr;
 from_header("souloc","n2",ns);
 float *souloc=new float[5*ns];
 read("souloc",souloc,5*ns);

 from_header("recloc","n2",nr);
 float *recloc=new float[3*nr];
 read("recloc",recloc,3*nr);
 
 int nfg; get_param("nfg",nfg);

 int nn=nt,mm=5,IPRINT[2]={1,0},DIAGCO=0,ICALL=0,IFLAG=0;
 float EPS=1e-5,XTOL=1e-16,F;
 float *DIAG=new float[nn]();
 float *W=new float[nn*(2*mm+1)+2*mm]();
 
 to_header("iwavelet","n1",nt,"o1",ot,"d1",dt);
 
 to_header("gwavelet","n1",nt,"o1",ot,"d1",dt);
 
 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 while(true){
  ICALL++;
  
  F=objFuncGradientWavelet_f(gwavelet,souloc,ns,recloc,wavelet,v,eps,del,padboundary,nx,ny,nz,nt,npad,ox,oy,oz,ot,dx,dy,dz,dt,samplingRate);

  fprintf(stderr,"icall %d objfunc %10.16f\n",ICALL,F);

  write("objfunc",&F,1,std::ios_base::app);
  to_header("objfunc","n1",ICALL,"o1",0.,"d1",1.);
  
  write("iwavelet",wavelet,nt,std::ios_base::app);
  to_header("iwavelet","n2",ICALL,"o2",0.,"d2",1.);
  
  write("gwavelet",gwavelet,nt,std::ios_base::app);
  to_header("gwavelet","n2",ICALL,"o2",0.,"d2",1.);
  
  lbfgs_(&nn,&mm,wavelet,&F,gwavelet,&DIAGCO,DIAG,IPRINT,&EPS,&XTOL,W,&IFLAG);
  
  if(IFLAG<=0 || ICALL>nfg){
   fprintf(stderr,"IFLAG %d\n",IFLAG);
   break;
  }
 }
 
 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()/60.<<" minutes"<<endl;
 
 delete []vepsdel;delete []DIAG;delete []W;
 
 delete []gwavelet;delete []wavelet;delete []souloc;delete []recloc;
 delete []padboundary; 

 myio_close();
 return 0;
}
