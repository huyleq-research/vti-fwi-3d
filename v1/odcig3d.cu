#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <iostream>
#include <vector>
#include <thread>
#include <chrono>

#include "myio.h"
#include "mylib.h"
#include "wave3d.h"

using namespace std;

void ODCIG1Shot1GPU(float *image,float *observedData,float soulocX,float soulocY,float soulocZ,float *recloc,int nr,float *wavelet,float *v,float *eps,float *del,int nx,int ny,int nz,int nt,int npad,float dx,float dy,float dz,float dt,float samplingRate,int device){
 cudaSetDevice(device);

 fprintf(stderr,"NUPDATE %d NLAG %d\n",NUPDATE,NLAG);

 float dx2=dx*dx,dy2=dy*dy,dz2=dz*dz,dt2=dt*dt;
 int nxy=nx*ny;
 long long nxyz=nx*ny*nz;
 
 int *recIndex=new int[nr];
 
 #pragma omp parallel for num_threads(16)
 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  recIndex[ir]=ixy+(iz%HALF_STENCIL)*nxy;
 }
 
 int *d_recIndex;
 cudaMalloc(&d_recIndex,nr*sizeof(int));
 cudaMemcpy(d_recIndex,recIndex,nr*sizeof(int),cudaMemcpyHostToDevice);

 int recBlock=recloc[2]/dz/HALF_STENCIL; //assuming all receivers at same depth

 int souIndexX=soulocX/dx;
 int souIndexY=soulocY/dy;
 int souIndexZ=soulocZ/dz;
 int souIndex=souIndexX+souIndexY*nx+souIndexZ*nxy;
 int souIndexBlock=souIndexX+souIndexY*nx+(souIndexZ%HALF_STENCIL)*nxy;
 int souBlock=souIndexZ/HALF_STENCIL;

 int samplingTimeStep=std::round(samplingRate/dt);
 int nnt=(nt-1)/samplingTimeStep+1;

 size_t nElemBlock=HALF_STENCIL*nxy;
 size_t nByteBlock=nElemBlock*sizeof(float);
 int nb=nz/HALF_STENCIL;

 float *prevSigmaX=new float[nxyz]();
 float *curSigmaX=new float[nxyz]();
 float *prevSigmaZ=new float[nxyz]();
 float *curSigmaZ=new float[nxyz]();

 float *h_v[2],*h_eps[2],*h_del[2];
 float *h_prevSigmaX[2],*h_curSigmaX[2],*h_SigmaX4[2],*h_SigmaX5[2];
 float *h_prevSigmaZ[2],*h_curSigmaZ[2],*h_SigmaZ4[2],*h_SigmaZ5[2];
 float *h_data[2],*d_data[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_v[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_eps[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_del[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaX[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaX5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZ[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZ5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_data[i],nr*sizeof(float),cudaHostAllocDefault);
  cudaMalloc(&d_data[i],nr*sizeof(float));
 }

 const int nd_Sigma=NUPDATE+2;
 float **d_SigmaX[nd_Sigma],**d_SigmaZ[nd_Sigma];
 int nbuffSigma[nd_Sigma];
 
 for(int i=0;i<nd_Sigma;++i) nbuffSigma[i]=3;
 nbuffSigma[1]=4;nbuffSigma[nd_Sigma-2]=4;
 
 size_t nByteAlloc=0;

 for(int i=0;i<nd_Sigma;++i){
  d_SigmaX[i]=new float*[nbuffSigma[i]]();
  d_SigmaZ[i]=new float*[nbuffSigma[i]]();
  for(int j=0;j<nbuffSigma[i];++j){
   cudaMalloc(&d_SigmaX[i][j],nByteBlock); 
   cudaMalloc(&d_SigmaZ[i][j],nByteBlock); 
   nByteAlloc+=2*nByteBlock;
   cudaMemset(d_SigmaX[i][j],0,nByteBlock);
   cudaMemset(d_SigmaZ[i][j],0,nByteBlock);
  }
 }

 const int nbuffVEpsDel=NUPDATE+4;
 float *d_v[nbuffVEpsDel],*d_eps[nbuffVEpsDel],*d_del[nbuffVEpsDel];
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaMalloc(&d_v[i],nByteBlock);
  cudaMalloc(&d_eps[i],nByteBlock);
  cudaMalloc(&d_del[i],nByteBlock);
  nByteAlloc+=3*nByteBlock;
  cudaMemset(d_v[i],0,nByteBlock);
  cudaMemset(d_eps[i],0,nByteBlock);
  cudaMemset(d_del[i],0,nByteBlock);
 }

 fprintf(stderr,"for forward alloc %f MBs on GPU\n",nByteAlloc*1e-6);

 cudaStream_t computeStream,transfInStream,transfOutStream;
 cudaStreamCreate(&computeStream);
 cudaStreamCreate(&transfInStream);
 cudaStreamCreate(&transfOutStream);

 vector<thread> threads;

 int nsave=(nt-2)/NUPDATE+1;
 float *forwardSouWavefield=new float[nx*nz*nsave];

 //injecting source at time 0 to wavefields at time 1
 curSigmaX[souIndex]=dt2*wavelet[0];
 curSigmaZ[souIndex]=dt2*wavelet[0];
 
 int ntb=(nt-2)/NUPDATE*nb;

 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 for(int k=0;k<ntb+NUPDATE+5;k++){
   if(k<ntb){
	int ib=k%nb;
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
   }
   
   if(k>0 && k<ntb+1){
    memcpyCpuToGpu3(d_v[k%nbuffVEpsDel],h_v[(k-1)%2],d_eps[k%nbuffVEpsDel],h_eps[(k-1)%2],d_del[k%nbuffVEpsDel],h_del[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k-1)%2],d_SigmaX[1][k%nbuffSigma[1]],h_curSigmaX[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k-1)%2],d_SigmaZ[1][k%nbuffSigma[1]],h_curSigmaZ[(k-1)%2],nByteBlock,&transfInStream);
   }
   
   if(k>2 && k<ntb+NUPDATE+2){
    forwardRandom(max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,nx,ny,nz,npad,dx2,dy2,dz2,dt2,&computeStream);
   }
   
   if(k>NUPDATE+3 && k<ntb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[k%2],d_SigmaX[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[k%2],d_SigmaX[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[k%2],d_SigmaZ[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[k%2],d_SigmaZ[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
   }
    
   if(k>NUPDATE+4){
	int ib=(k-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(k-1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(k-1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(k-1)%2],nByteBlock);
   }
	
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(forwardSouWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,curSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 long long n=nx*ny*nz;
 long long ncells=n*(nt-2);
 cout<<"ncells "<<ncells<<endl;
 cout<<"speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl;
 
 cudaError_t e=cudaGetLastError();
 if(e!=cudaSuccess) fprintf(stderr,"error %s\n",cudaGetErrorString(e));

 write("forwardSouWavefield",forwardSouWavefield,nx*nz*nsave);
 to_header("forwardSouWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("forwardSouWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("forwardSouWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []forwardSouWavefield;

 //flip time direction
 float *pt;
 pt=curSigmaX;curSigmaX=prevSigmaX;prevSigmaX=pt;
 pt=curSigmaZ;curSigmaZ=prevSigmaZ;prevSigmaZ=pt;

 //receiver wavefields and cross-correlation imaging
 float *backwardSouWavefield=new float[nx*nz*nsave];

 float *recWavefield=new float[nx*nz*nsave];

 float *prevSigmaXa=new float[nxyz]();
 float *curSigmaXa=new float[nxyz]();
 float *prevSigmaZa=new float[nxyz]();
 float *curSigmaZa=new float[nxyz]();
 
 #pragma omp parallel for num_threads(16)
 for(int ir=0;ir<nr;ir++){
  int ix=recloc[3*ir]/dx;
  int iy=recloc[3*ir+1]/dy;
  int iz=recloc[3*ir+2]/dz;
  int ixy=ix+iy*nx;
  int i=ixy+iz*nxy;
  curSigmaXa[i]=2./3.*dt2*observedData[ir+(nnt-1)*nr];
  curSigmaZa[i]=1./3.*dt2*observedData[ir+(nnt-1)*nr];
 }

 float *h_prevSigmaXa[2],*h_curSigmaXa[2],*h_SigmaXa4[2],*h_SigmaXa5[2];
 float *h_prevSigmaZa[2],*h_curSigmaZa[2],*h_SigmaZa4[2],*h_SigmaZa5[2];
 float *h_image[2];

 for(int i=0;i<2;++i){
  cudaHostAlloc(&h_prevSigmaXa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaXa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaXa4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaXa5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_prevSigmaZa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_curSigmaZa[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZa4[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_SigmaZa5[i],nByteBlock,cudaHostAllocDefault);
  cudaHostAlloc(&h_image[i],nByteBlock*(2*NLAG+1),cudaHostAllocDefault);
 }
 
 float **d_SigmaXa[nd_Sigma],**d_SigmaZa[nd_Sigma];
 
 nByteAlloc=0;

 for(int i=0;i<nd_Sigma;++i){
  d_SigmaXa[i]=new float*[nbuffSigma[i]]();
  d_SigmaZa[i]=new float*[nbuffSigma[i]]();
  for(int j=0;j<nbuffSigma[i];++j){
   cudaMalloc(&d_SigmaXa[i][j],nByteBlock); 
   cudaMalloc(&d_SigmaZa[i][j],nByteBlock); 
   nByteAlloc+=2*nByteBlock;
   cudaMemset(d_SigmaXa[i][j],0,nByteBlock);
   cudaMemset(d_SigmaZa[i][j],0,nByteBlock);
  }
 }

 const int ndg=NUPDATE+2;
 float *d_image[ndg];
 for(int i=0;i<ndg;i++){
  cudaMalloc(&d_image[i],nByteBlock*(2*NLAG+1));
  nByteAlloc+=(2*NLAG+1)*nByteBlock;
 }

 fprintf(stderr,"for adjoint alloc %f MBs on GPU\n",nByteAlloc*1e-6);

 start=chrono::high_resolution_clock::now();
 
 for(int k=0;k<ntb+NUPDATE+5;k++){
   int ib=k%nb,it=k/nb;
   if(k<ntb){
    threads.push_back(thread(memcpyCpuToCpu3,h_v[k%2],v+ib*nElemBlock,h_eps[k%2],eps+ib*nElemBlock,h_del[k%2],del+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaX[k%2],prevSigmaX+ib*nElemBlock,h_curSigmaX[k%2],curSigmaX+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZ[k%2],prevSigmaZ+ib*nElemBlock,h_curSigmaZ[k%2],curSigmaZ+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaXa[k%2],prevSigmaXa+ib*nElemBlock,h_curSigmaXa[k%2],curSigmaXa+ib*nElemBlock,nByteBlock));
	threads.push_back(thread(memcpyCpuToCpu2,h_prevSigmaZa[k%2],prevSigmaZa+ib*nElemBlock,h_curSigmaZa[k%2],curSigmaZa+ib*nElemBlock,nByteBlock));
   }
   
   if(ib>recBlock && ib<recBlock+NUPDATE+1) threads.push_back(thread(interpolateResidual,h_data[k%2],observedData,nt-it*NUPDATE-ib-1+recBlock,nr,samplingTimeStep));
   
   if(k>0 && k<ntb+1){
    memcpyCpuToGpu3(d_v[k%nbuffVEpsDel],h_v[(k-1)%2],d_eps[k%nbuffVEpsDel],h_eps[(k-1)%2],d_del[k%nbuffVEpsDel],h_del[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaX[0][(k-1)%nbuffSigma[0]],h_prevSigmaX[(k-1)%2],d_SigmaX[1][k%nbuffSigma[1]],h_curSigmaX[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZ[0][(k-1)%nbuffSigma[0]],h_prevSigmaZ[(k-1)%2],d_SigmaZ[1][k%nbuffSigma[1]],h_curSigmaZ[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaXa[0][(k-1)%nbuffSigma[0]],h_prevSigmaXa[(k-1)%2],d_SigmaXa[1][k%nbuffSigma[1]],h_curSigmaXa[(k-1)%2],nByteBlock,&transfInStream);
    memcpyCpuToGpu2(d_SigmaZa[0][(k-1)%nbuffSigma[0]],h_prevSigmaZa[(k-1)%2],d_SigmaZa[1][k%nbuffSigma[1]],h_curSigmaZa[(k-1)%2],nByteBlock,&transfInStream);
   }
   
   if(ib>recBlock+1 && ib<recBlock+NUPDATE+2) cudaMemcpyAsync(d_data[k%2],h_data[(k+1)%2],nr*sizeof(float),cudaMemcpyHostToDevice,transfInStream);

   if(k>2 && k<ntb+NUPDATE+2){
    extendedImaging(d_image,ndg,max(0,k-ntb-2),min(k-2,NUPDATE),k,d_SigmaX,d_SigmaZ,d_SigmaXa,d_SigmaZa,nbuffSigma,d_v,d_eps,d_del,nbuffVEpsDel,wavelet,souIndexBlock,souBlock,d_data[(k+1)%2],nr,d_recIndex,recBlock,nx,ny,nz,npad,nt,dx2,dy2,dz2,dt2,&computeStream);
   }
   
   if(k>NUPDATE+3 && k<ntb+NUPDATE+4){
    memcpyGpuToCpu2(h_SigmaX4[k%2],d_SigmaX[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaX5[k%2],d_SigmaX[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZ4[k%2],d_SigmaZ[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZ5[k%2],d_SigmaZ[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaXa4[k%2],d_SigmaXa[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaXa5[k%2],d_SigmaXa[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    memcpyGpuToCpu2(h_SigmaZa4[k%2],d_SigmaZa[nd_Sigma-2][(k-4)%nbuffSigma[nd_Sigma-2]],h_SigmaZa5[k%2],d_SigmaZa[nd_Sigma-1][(k-3)%nbuffSigma[nd_Sigma-1]],nByteBlock,&transfOutStream);
    cudaMemcpyAsync(h_image[k%2],d_image[(k-NUPDATE-1)%ndg],nByteBlock*(2*NLAG+1),cudaMemcpyDeviceToHost,transfOutStream);
	cudaMemsetAsync(d_image[(k-NUPDATE-1)%ndg],0,nByteBlock*(2*NLAG+1),transfOutStream);
   }
    
   if(k>NUPDATE+4){
	ib=(k-NUPDATE-5)%nb;
    memcpyCpuToCpu2(prevSigmaX+ib*nElemBlock,h_SigmaX4[(k-1)%2],curSigmaX+ib*nElemBlock,h_SigmaX5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZ+ib*nElemBlock,h_SigmaZ4[(k-1)%2],curSigmaZ+ib*nElemBlock,h_SigmaZ5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaXa+ib*nElemBlock,h_SigmaXa4[(k-1)%2],curSigmaXa+ib*nElemBlock,h_SigmaXa5[(k-1)%2],nByteBlock);
    memcpyCpuToCpu2(prevSigmaZa+ib*nElemBlock,h_SigmaZa4[(k-1)%2],curSigmaZa+ib*nElemBlock,h_SigmaZa5[(k-1)%2],nByteBlock);
    for(int lag=0;lag<(2*NLAG+1);lag++) sumImageTime(image+ib*nElemBlock+lag*nx*ny*nz,h_image[(k+1)%2]+lag*nElemBlock,nElemBlock);
   }
	
   cudaDeviceSynchronize();
   
   for(int i=0;i<threads.size();++i) threads[i].join();
   threads.erase(threads.begin(),threads.end());
  
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(backwardSouWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,prevSigmaX+ny/2*nx+iz*nx*ny,nx*sizeof(float));
  if((k-NUPDATE-4)%nb==0) for(int iz=0;iz<nz;iz++) memcpy(recWavefield+iz*nx+(k-NUPDATE-4)/nb*nx*nz,prevSigmaXa+ny/2*nx+iz*nx*ny,nx*sizeof(float));
 }

 end=chrono::high_resolution_clock::now();
 time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 cout<<"for adjoint speed "<<static_cast<double>(ncells)/time.count()<<" cells per second"<<endl;
 
 e=cudaGetLastError();
 if(e!=cudaSuccess) fprintf(stderr,"error in adjoint %s\n",cudaGetErrorString(e));
 
 write("backwardSouWavefield",backwardSouWavefield,nx*nz*nsave);
 to_header("backwardSouWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("backwardSouWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("backwardSouWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []backwardSouWavefield;

 write("recWavefield",recWavefield,nx*nz*nsave);
 to_header("recWavefield","n1",nx,"o1",0.,"d1",dx);
 to_header("recWavefield","n2",nz,"o2",0.,"d2",dz);
 to_header("recWavefield","n3",nsave,"o3",0.,"d3",dt*NUPDATE);

 delete []recWavefield;

 //delete arrays used in forward
 delete []prevSigmaX;delete []curSigmaX;
 delete []prevSigmaZ;delete []curSigmaZ;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_v[i]);
  cudaFreeHost(h_eps[i]);
  cudaFreeHost(h_del[i]);
  cudaFreeHost(h_prevSigmaX[i]);
  cudaFreeHost(h_curSigmaX[i]);
  cudaFreeHost(h_SigmaX4[i]);
  cudaFreeHost(h_SigmaX5[i]);
  cudaFreeHost(h_prevSigmaZ[i]);
  cudaFreeHost(h_curSigmaZ[i]);
  cudaFreeHost(h_SigmaZ4[i]);
  cudaFreeHost(h_SigmaZ5[i]);
  cudaFreeHost(h_data[i]);
  cudaFree(d_data[i]);
 }
 
 for(int i=0;i<nd_Sigma;++i){
  for(int j=0;j<nbuffSigma[i];++j){
   cudaFree(d_SigmaX[i][j]); 
   cudaFree(d_SigmaZ[i][j]); 
  }
  delete []d_SigmaX[i];
  delete []d_SigmaZ[i];
 }
 
 for(int i=0;i<nbuffVEpsDel;++i){
  cudaFree(d_v[i]);
  cudaFree(d_eps[i]);
  cudaFree(d_del[i]);
 }
 
 delete []recIndex;
 cudaFree(d_recIndex);
 
 cudaStreamDestroy(computeStream);
 cudaStreamDestroy(transfInStream);
 cudaStreamDestroy(transfOutStream);
 
 //delete arrays used in adjoint
 delete []prevSigmaXa;delete []curSigmaXa;
 delete []prevSigmaZa;delete []curSigmaZa;

 for(int i=0;i<2;++i){
  cudaFreeHost(h_prevSigmaXa[i]);
  cudaFreeHost(h_curSigmaXa[i]);
  cudaFreeHost(h_SigmaXa4[i]);
  cudaFreeHost(h_SigmaXa5[i]);
  cudaFreeHost(h_prevSigmaZa[i]);
  cudaFreeHost(h_curSigmaZa[i]);
  cudaFreeHost(h_SigmaZa4[i]);
  cudaFreeHost(h_SigmaZa5[i]);
  cudaFreeHost(h_image[i]);
 }
 
 for(int i=0;i<nd_Sigma;++i){
  for(int j=0;j<nbuffSigma[i];++j){
   cudaFree(d_SigmaXa[i][j]); 
   cudaFree(d_SigmaZa[i][j]); 
  }
  delete []d_SigmaXa[i];
  delete []d_SigmaZa[i];
 }
 
 for(int i=0;i<ndg;i++){
  cudaFree(d_image[i]);
 }

 return;
}

