#include <cstdio>
#include <string>
#include <cstring>
#include <chrono>

#include "myio.h"
#include "mylib.h"
#include "wave3d.h"

using namespace std;

extern "C"{
 void setulb_(int *n,int *m,float *x,float *l,float *u,int *nbd,float *f,float *g,float *factr,float *pgtol,float *wa,int *iwa,char task[],int *iprint,char csave[],int lsave[],int isave[],float dsave[],unsigned int tasklen,unsigned int csavelen);
}

int main(int argc,char **argv){
 myio_init(argc,argv);

 int nx,ny,nz,npad,nt;
 float ox,oy,oz,ot,dx,dy,dz,dt;
 
 from_header("v","n1",nx,"o1",ox,"d1",dx);
 from_header("v","n2",ny,"o2",oy,"d2",dy);
 from_header("v","n3",nz,"o3",oz,"d3",dz);
 get_param("npad",npad);
 get_param("nt",nt,"ot",ot,"dt",dt);
 
 long long nxy=nx*ny;
 long long nxyz=nxy*nz;
 long long nboundary=nxyz-(nx-2*npad)*(ny-2*npad)*(nz-2*npad);
 
 float *wavelet=new float[nt]();
 read("wavelet",wavelet,nt);

 float samplingRate;
 get_param("samplingRate",samplingRate);
 int samplingTimeStep=std::round(samplingRate/dt);
 
 float *vepsdel=new float[3*nxyz];
 float *v=vepsdel,*eps=vepsdel+nxyz,*del=vepsdel+2*nxyz;
 read("v",v,nxyz);
 read("eps",eps,nxyz);
 read("del",del,nxyz);

 float *randomboundary=new float[nboundary]; 
 read("randomboundary",randomboundary,nboundary);
 float *padboundary=new float[nboundary];
 read("padboundary",padboundary,nboundary);

 int ns,nr;
 from_header("souloc","n2",ns);
 float *souloc=new float[5*ns];
 read("souloc",souloc,5*ns);

 from_header("recloc","n2",nr);
 float *recloc=new float[3*nr];
 read("recloc",recloc,3*nr);
 
 float wbottom; get_param("wbottom",wbottom);
 float *m=new float[nxyz];
 
 float v0,eps0;
 get_param("v0",v0,"eps0",eps0);

 scale(v,v,1./v0,nxyz);
 scale(eps,eps,1./eps0,nxyz);

 float *gvepsdel=new float[3*nxyz]();
 float *gv=gvepsdel,*geps=gvepsdel+nxyz,*gdel=gvepsdel+2*nxyz;

 int nfg; get_param("nfg",nfg);

 int nn=3*nxyz,mm=5,iprint=-1,lsave[4],isave[44];
 float factr=0.,pgtol=0.,f,dsave[29];
 char task[60],csave[60];
 int *nbd=new int[nn]();
 int *iwa=new int[3*nn]();
 float *l=new float[nn]();
 float *u=new float[nn]();
 float *wa=new float[2*mm*nn+5*nn+11*mm*mm+8*mm]();

// float *lv=l,*leps=l+nxyz,*ldel=l+2*nxyz;
// float *uv=u,*ueps=u+nxyz,*udel=u+2*nxyz;

 strcpy(task,"START");
 for(int i=5;i<60;++i) task[i]=' ';

 int nnew=0;

 to_header("iv","n1",nx,"o1",ox,"d1",dx);
 to_header("iv","n2",ny,"o2",oy,"d2",dy);
 to_header("iv","n3",nz,"o3",oz,"d3",dz);
 
 to_header("ieps","n1",nx,"o1",ox,"d1",dx);
 to_header("ieps","n2",ny,"o2",oy,"d2",dy);
 to_header("ieps","n3",nz,"o3",oz,"d3",dz);
 
 to_header("idel","n1",nx,"o1",ox,"d1",dx);
 to_header("idel","n2",ny,"o2",oy,"d2",dy);
 to_header("idel","n3",nz,"o3",oz,"d3",dz);
 
 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 while((task[0]=='F' && task[1]=='G') || 
       (task[0]=='N' && task[1]=='E' && task[2]=='W' && task[3]=='_' && task[4]=='X') || 
       (task[0]=='S' && task[1]=='T' && task[2]=='A' && task[3]=='R' && task[4]=='T')){
  
  setulb_(&nn,&mm,vepsdel,l,u,nbd,&f,gvepsdel,&factr,&pgtol,wa,iwa,task,&iprint,csave,lsave,isave,dsave,60,60);

  if(task[0]=='F' && task[1]=='G'){
   f=objFuncGradientVEpsDel(gv,geps,gdel,souloc,ns,recloc,wavelet,v,eps,del,padboundary,randomboundary,nx,ny,nz,nt,npad,ox,oy,oz,ot,dx,dy,dz,dt,samplingRate,v0,eps0,wbottom,m);
  }
  else{
   if(task[0]=='N' && task[1]=='E' && task[2]=='W' && task[3]=='_' && task[4]=='X'){
	nnew++;

    fprintf(stderr,"New x %d iterate %d nfg=%d f=%.10f |proj g|=%.10f\n",nnew,isave[29],isave[33],f,dsave[12]);
    
    write("objfunc",&f,1,std::ios_base::app);
    to_header("objfunc","n1",nnew,"o1",0.,"d1",1.);

    write("iv",v,nxyz,std::ios_base::app);
    to_header("iv","n4",nnew,"o4",0.,"d4",1.);

    write("ieps",eps,nxyz,std::ios_base::app);
    to_header("ieps","n4",nnew,"o4",0.,"d4",1.);
    
    write("idel",del,nxyz,std::ios_base::app);
    to_header("idel","n4",nnew,"o4",0.,"d4",1.);
 
	if(isave[33]>=nfg){
	 fprintf(stderr,"STOP: TOTAL NO. of f AND g EVALUATIONS EXCEEDS LIMIT");
	 break; 
	}
   
    if(dsave[12]<=1e-10*(1+fabs(f))){
	 fprintf(stderr,"STOP: THE PROJECTED GRADIENT IS SUFFICIENTLY SMALL");
	 break; 
	}
   }
  }
 }

 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()/60.<<" minutes"<<endl;
 
 delete []vepsdel;delete []gvepsdel;
 delete []nbd;delete []iwa;delete []l;delete []u;delete []wa;
 delete []wavelet;
 delete []souloc; delete []recloc;
 delete []padboundary; delete []randomboundary;

 myio_close();
 return 0;
}
